/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/*
 * pnrt-receive.c
 *
 *  Created on: 22.07.2012
 *      Author: cleitner
 */

#include "pnrt-transport.h"

static pcap_t *dev;

static bool recv_ipc_handler(int32_t id, void* buf, size_t len);
static bool recv_ipc_call_handler(int32_t id, void* in_buf, size_t in_len,
		void* out_buf, size_t* out_len);

static int run_receiver(const char* device, const char* bpf_filter);

static void term_receive(int signum);
static void handle_receive(u_char *user, const struct pcap_pkthdr *h,
		const u_char *bytes);

extern int cmd_receive(int argc, char **argv) {
	char filter_program[1024];

	uint8_t dst_mac[6];

	if (argc != 4) {
		usage(argv[0]);
		return EXIT_INVALID_ARGUMENTS;
	}

	parse_mac(argv[3], dst_mac);

	/* Filter out anything not PN-RT and not meant for us */
	snprintf(filter_program, sizeof(filter_program),
			"(ether proto 0x8892 or (vlan and ether proto 0x8892))"
			" and ether dst %02" PRIx8 ":%02" PRIx8 ":%02" PRIx8 ":%02" PRIx8 ":%02" PRIx8 ":%02" PRIx8,
			dst_mac[0], dst_mac[1], dst_mac[2], dst_mac[3], dst_mac[4], dst_mac[5]);

	return run_receiver(argv[2], filter_program);
}

extern int cmd_receive_lldp(int argc, char **argv) {
	char filter_program[1024];

	if (argc != 3) {
		usage(argv[0]);
		return EXIT_INVALID_ARGUMENTS;
	}

	/* Filter out anything not LLDP and not meant for LLDP listeners */
	snprintf(filter_program, sizeof(filter_program),
		"(ether proto 0x88cc or (vlan and ether proto 0x88cc))"
		" and ether dst 01:80:c2:00:00:0e");

	return run_receiver(argv[2], filter_program);
}

static int run_receiver(const char* device, const char* bpf_filter) {
	int result;

	{
		char errbuf[PCAP_ERRBUF_SIZE];

		dev = pcap_create(device, errbuf);
		if (dev == NULL) {
			fprintf(stderr, "error: %s\n", errbuf);
			return EXIT_PCAP_ERROR;
		}
	}

	/* TODO: recent libpcap adds support for pcap_set_immediate_mode */
	if ((pcap_set_snaplen(dev, MAX_PACKET_SIZE) != 0)
			|| (pcap_set_promisc(dev, 0) != 0)
			|| (pcap_set_timeout(dev, 0) != 0)
			|| (pcap_set_buffer_size(dev, 0) != 0)) {
		fprintf(stderr, "error: failed to setup device\n");
		goto error;
	}

#ifndef WIN32
	/* Set the direction to input only, but ignore the result as some
	 * implementations might not support setdirection */
	pcap_setdirection(dev, PCAP_D_IN);
#endif

	if (pcap_activate(dev) != 0) {
		fprintf(stderr, "error: couldn't activate capture device\n");
		goto error;
	}

#ifdef WIN32
	if (pcap_setmintocopy(dev, 0) != 0) {
		fprintf(stderr, "error: couldn't set minimum copy size for driver\n");
		goto error;
	}
#endif

	if (pcap_set_datalink(dev, LINKTYPE_ETHERNET) != 0) {
		fprintf(stderr, "error: couldn't set datalink type\n");
		goto error;
	}

#ifdef __APPLE__
	/* Force immediate mode */
	{
		int enable = 1;
		int fd = pcap_fileno(dev);

		if (fd == -1) {
			fprintf(stderr, "error: couldn't get capture file handle\n");
			goto error;
		}

		if (ioctl(fd, BIOCIMMEDIATE, &enable) == -1) {
			fprintf(stderr, "error: couldn't set immediate capture mode\n");
			goto error;
		}
	}
#endif

	if (bpf_filter != NULL) {
		struct bpf_program filter;

		if ((pcap_compile(dev, &filter, bpf_filter, 1, 0) != 0)
				|| (pcap_setfilter(dev, &filter) != 0)) {
			fprintf(stderr, "error: %s\n", pcap_geterr(dev));
			goto error;
		}
	}

	/* we must break the receive loop with pcap_breakloop */
	signal(SIGTERM, term_receive);
	signal(SIGINT, term_receive);
#ifndef WIN32
	signal(SIGHUP, term_receive);
#endif

	start_ipc_receiver(recv_ipc_handler, recv_ipc_call_handler);

	result = pcap_loop(dev, -1, handle_receive, NULL);

	if ((result != 0) && (result != -2)) {
		fprintf(stderr, "error: %s\n", pcap_geterr(dev));
		goto error;
	}

	pcap_close(dev);

	return EXIT_SUCCESS;

error:
	pcap_close(dev);

	return EXIT_PCAP_ERROR;
}


static void term_receive(int signum) {
	pcap_breakloop(dev);
}

static void handle_receive(u_char *user, const struct pcap_pkthdr *hdr,
		const u_char *packet) {

	assert(hdr->caplen <= MAX_PACKET_SIZE);
	assert(hdr->caplen >= 14);

	ipc_send(IPC_ID_RECEIVE, (void*) packet, hdr->caplen);
}

static bool recv_ipc_handler(int32_t id, void* buf, size_t len) {
	switch (id) {
	default:
		fprintf(stderr, "warning: received unknown IPC message %d\n", id);
		break;
	}

	return true;
}

static bool recv_ipc_call_handler(int32_t id, void* in_buf, size_t in_len,
		void* out_buf, size_t* out_len) {

	switch (id) {
	case IPC_ID_QUIT:
		return false;
	default:
		fprintf(stderr, "warning: received unknown IPC message %d\n", id);
		break;
	}

	return true;
}
