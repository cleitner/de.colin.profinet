/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/*
 * pnrt-send.c
 *
 *  Created on: 22.07.2012
 *      Author: cleitner
 */

#include "pnrt-transport.h"

static pcap_t *dev;
static uint8_t packet[MAX_PACKET_SIZE];

static bool send_ipc_handler(int32_t id, void* buf, size_t len);
static bool send_ipc_call_handler(int32_t id, void* in_buf, size_t in_len,
		void* out_buf, size_t* out_len);

extern int cmd_send(int argc, char **argv) {
	uint16_t len;

	if (argc != 4) {
		usage(argv[0]);
		return EXIT_INVALID_ARGUMENTS;
	}

	parse_mac(argv[3], &packet[6]);
	packet[12] = 0x88;
	packet[13] = 0x92;

	{
		char errbuf[PCAP_ERRBUF_SIZE];

		dev = pcap_open_live(argv[2], 0, 0, 0, errbuf);
		if (dev == NULL) {
			fprintf(stderr, "error: %s\n", errbuf);
			return EXIT_PCAP_ERROR;
		}
	}

	if (pcap_datalink(dev) != LINKTYPE_ETHERNET) {
		fprintf(stderr, "error: device uses unsupported link type %d\n", pcap_datalink(dev));
		pcap_close(dev);
		return EXIT_PCAP_ERROR;
	}

#ifndef WIN32
	/* Set the direction to input only, but ignore the result as some
	 * implementations might not support setdirection */
	pcap_setdirection(dev, PCAP_D_OUT);
#else
	pcap_setmintocopy(dev, 0);
#endif

	if (!ipc_receive(send_ipc_handler, send_ipc_call_handler)) {
		fprintf(stderr, "error: IPC reception failed\n");
		pcap_close(dev);
		return EXIT_FAILURE;
	}

	pcap_close(dev);

	return EXIT_SUCCESS;
}

static bool send_ipc_handler(int32_t id, void* buf, size_t len) {
	uint8_t* u8buf = (uint8_t*) buf;

	switch (id) {
	case IPC_ID_SEND:
		if (len < 14) {
			fprintf(stderr, "error: invalid length for Ethernet packet (too short)\n");
			pcap_close(dev);
			exit(EXIT_INVALID_DATA);
		}

		if (len > 1514) {
			fprintf(stderr, "error: invalid length for Ethernet packet\n");
			pcap_close(dev);
			exit(EXIT_INVALID_DATA);
		}

		/* Exclude source address and Ethernet type */
		memcpy(&packet[0], &u8buf[0], 6);
		memcpy(&packet[14], &u8buf[14], len - 14);

		/* Fill Ethernet frame to minimum size */
		if (len < MIN_PACKET_SIZE) {
			memset(&packet[len], 0, MIN_PACKET_SIZE - len);
			len = MIN_PACKET_SIZE;
		}

		if (pcap_sendpacket(dev, packet, len) != 0) {
			fprintf(stderr, "error: %s\n", pcap_geterr(dev));
			pcap_close(dev);

			exit(EXIT_PCAP_ERROR);
		}

		break;
	default:
		fprintf(stderr, "warning: received unknown IPC message %d\n", id);
		break;
	}

	return true;
}

static bool send_ipc_call_handler(int32_t id, void* in_buf, size_t in_len,
		void* out_buf, size_t* out_len) {

	switch (id) {
	case IPC_ID_QUIT:
		return false;
	case IPC_ID_HELLO:
		memcpy(out_buf, "hello", 5);
		*out_len = 5;
		break;
	default:
		fprintf(stderr, "warning: received unknown IPC message %d\n", id);
		break;
	}

	return true;
}
