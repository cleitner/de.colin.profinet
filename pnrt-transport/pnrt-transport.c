/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

#include "pnrt-transport.h"

static void init(void);
static void print_ll(pcap_if_t *dev);

extern int main(int argc, char *argv[]) {

	const char *command;

	init();

	if (argc < 2) {
		usage(argv[0]);
		return EXIT_INVALID_ARGUMENTS;
	}

	command = argv[1];

	if (!strcmp(command, "list")) {
		return cmd_list(argc, argv);
	} else if (!strcmp(command, "send")) {
		return cmd_send(argc, argv);
	} else if (!strcmp(command, "receive")) {
		return cmd_receive(argc, argv);
	} else if (!strcmp(command, "rlldp")) {
		return cmd_receive_lldp(argc, argv);
	}

	usage(argv[0]);
	return EXIT_INVALID_ARGUMENTS;
}

extern void usage(const char *prog) {
	fprintf(stderr, "usage: %s <command>\n", prog);
	fprintf(stderr, "\n");
	fprintf(stderr, "Commands:\n");
	fprintf(stderr, "   list: prints all devices that we can access\n");
	fprintf(stderr, "   send <device> <src mac>:\n");
	fprintf(stderr, "      sends PN-RT packets through IPCs\n");
	fprintf(stderr, "   receive <device> <dst mac>:\n");
	fprintf(stderr, "      receives PN-RT packets throug IPCs\n");
	fprintf(stderr, "   rlldp <device>:\n");
	fprintf(stderr, "      receives LLDP packets throug IPCs\n");
}

extern int cmd_list(int argc, char **argv) {
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_if_t *devs, *d;

	if (argc != 2) {
		usage(argv[0]);
		return EXIT_INVALID_ARGUMENTS;
	}

	if (pcap_findalldevs(&devs, errbuf) != 0) {
		fprintf(stderr, "error: %s\n", errbuf);
		return EXIT_PCAP_ERROR;
	}

	for (d = devs; d != NULL; d = d->next) {
		pcap_addr_t *a;

		fprintf(stdout, "%s\n", d->name);

		print_ll(d);

		for (a = d->addresses; a != NULL; a = a->next) {
			struct sockaddr *sa;

			if (a->addr->sa_family == AF_INET) {
				struct sockaddr_in *ina;
				ina = (struct sockaddr_in *)a->addr;
				printf("   ipv4: %s\n", inet_ntoa(ina->sin_addr));
#ifdef __APPLE__
			} else if (a->addr->sa_family == AF_LINK) {
				struct sockaddr_dl *dla;

				dla = (struct sockaddr_dl *)a->addr;
				if ((dla->sdl_type != IFT_ETHER) || (dla->sdl_alen != 6)) {
					printf("   slt: %d\n", dla->sdl_type);
				} else {
					caddr_t mac = LLADDR(dla);

					printf("   ll: %02X:%02X:%02X:%02X:%02X:%02X\n",
								(uint8_t) mac[0],
								(uint8_t) mac[1],
								(uint8_t) mac[2],
								(uint8_t) mac[3],
								(uint8_t) mac[4],
								(uint8_t) mac[5]);
				}
#endif
			} else {
				printf("   unknown: %d\n", a->addr->sa_family);
			}
		}
	}

	pcap_freealldevs(devs);

	return EXIT_SUCCESS;
}

extern void parse_mac(const char *str, uint8_t *mac) {
	memset(mac, 0, 6);

	/* TODO: SCNx8 operating on mac directly resulted in a memory corruption */
	int temp[6];

	sscanf(str,
		"%02x"
		":%02x"
		":%02x"
		":%02x"
		":%02x"
		":%02x",
		&temp[0],
		&temp[1],
		&temp[2],
		&temp[3],
		&temp[4],
		&temp[5]);

	mac[0] = (uint8_t) temp[0];
	mac[1] = (uint8_t) temp[1];
	mac[2] = (uint8_t) temp[2];
	mac[3] = (uint8_t) temp[3];
	mac[4] = (uint8_t) temp[4];
	mac[5] = (uint8_t) temp[5];
}

#ifdef WIN32

#include <iphlpapi.h>

static CRITICAL_SECTION ipc_cs;
static ipc_handler_t ipc_recv_handler;
static ipc_call_handler_t ipc_recv_call_handler;

static void __cdecl receive_ipc_task(void* args);

static void init(void) {
	/* Disable MinGW stdout buffering */
	setvbuf(stdout, NULL, _IONBF, 0);

	/* Set stdin/stdout to binary mode */
	_setmode(_fileno(stdin), _O_BINARY);
	_setmode(_fileno(stdout), _O_BINARY);

	/* The spin count is chossen as 4000, a value found in the description on
	 * MSDN. Probably a value that would have to be evaluated to be correct. */
	if (!InitializeCriticalSectionAndSpinCount(&ipc_cs, 4000)) {
		fprintf(stderr, "error: couldn't initialize IPC critical section\n");
		exit(EXIT_FAILURE);
	}
}

extern void start_ipc_receiver(ipc_handler_t handler,
		ipc_call_handler_t call_handler) {

	ipc_recv_handler = handler;
	ipc_recv_call_handler = call_handler;

	_beginthread(receive_ipc_task, 0, NULL);
}

extern void ipc_critical_begin(void) {
	EnterCriticalSection(&ipc_cs);
}

extern void ipc_critical_end(void) {

	LeaveCriticalSection(&ipc_cs);
}

static void __cdecl receive_ipc_task(void* args) {
	if (!ipc_receive(ipc_recv_handler, ipc_recv_call_handler)) {
		fprintf(stderr, "error: IPC reception failed\n");
		exit(EXIT_FAILURE);
	} else {
		exit(EXIT_SUCCESS);
	}
}

#else

#include <pthread.h>

static pthread_mutex_t ipc_mutex = PTHREAD_MUTEX_INITIALIZER;
static ipc_handler_t ipc_recv_handler;
static ipc_call_handler_t ipc_recv_call_handler;

static void* receive_ipc_task(void* args);

static void init(void) {
	/* We're fine already */
}

extern void start_ipc_receiver(ipc_handler_t handler,
		ipc_call_handler_t call_handler) {

	pthread_t thread;

	ipc_recv_handler = handler;
	ipc_recv_call_handler = call_handler;

	pthread_create(&thread, NULL, receive_ipc_task, NULL);
}

extern void ipc_critical_begin(void) {
	pthread_mutex_lock(&ipc_mutex);
}

extern void ipc_critical_end(void) {
	pthread_mutex_unlock(&ipc_mutex);
}

static void* receive_ipc_task(void* args) {
	if (!ipc_receive(ipc_recv_handler, ipc_recv_call_handler)) {
		fprintf(stderr, "error: IPC reception failed\n");
		exit(EXIT_FAILURE);
	} else {
		exit(EXIT_SUCCESS);
	}

	return NULL;
}

#endif

#ifdef WIN32

static void print_ll(pcap_if_t *dev) {
	DWORD size = 0;
	IP_ADAPTER_ADDRESSES *addresses, *a;

	if (GetAdaptersAddresses(AF_INET, 0, NULL, NULL, &size) != ERROR_BUFFER_OVERFLOW) {
		return;
	}

	/* Don't accept unreasonable amounts of memory */
	if (size > 1024 * 1024) {
		return;
	}

	addresses = (IP_ADAPTER_ADDRESSES *)malloc(size);
	if (addresses == NULL) {
		return;
	}

	if (GetAdaptersAddresses(AF_INET, 0, NULL, addresses, &size) != ERROR_SUCCESS) {
		goto out;
	}

	for (a = addresses; a != NULL; a = a->Next) {
		char npf_name[256];

		snprintf(npf_name, sizeof(npf_name), "\\Device\\NPF_%s", a->AdapterName);

		if (strcmp(npf_name, dev->name) == 0) {
			if (a->PhysicalAddressLength != 6) {
				continue;
			}

			printf("   ift: %d\n", a->IfType);

			printf("   ll: %02X:%02X:%02X:%02X:%02X:%02X\n",
					a->PhysicalAddress[0], a->PhysicalAddress[1], a->PhysicalAddress[2],
					a->PhysicalAddress[3], a->PhysicalAddress[4], a->PhysicalAddress[5]);

			break;
		}
	}

out:
	free(addresses);
}

#elif defined(__APPLE__)

static void print_ll(pcap_if_t *dev) {
	/* Mac OS X returns the LL address as part of the pcap device address list.
	 * Nice, no special handling required. */
}

#else

static void print_ll(pcap_if_t *dev) {
	int fd;
	struct ifreq ifr;

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd == -1) {
		return;
	}

	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, dev->name, IFNAMSIZ - 1);

	if (ioctl(fd, SIOCGIFHWADDR, &ifr) != 0) {
		goto out;
	}

	printf("   llt: %04X\n", ifr.ifr_hwaddr.sa_family);

	if (ifr.ifr_hwaddr.sa_family != ARPHRD_ETHER) {
		goto out;
	}

	printf("   ll: %02X:%02X:%02X:%02X:%02X:%02X\n",
			(uint8_t) ifr.ifr_hwaddr.sa_data[0],
			(uint8_t) ifr.ifr_hwaddr.sa_data[1],
			(uint8_t) ifr.ifr_hwaddr.sa_data[2],
			(uint8_t) ifr.ifr_hwaddr.sa_data[3],
			(uint8_t) ifr.ifr_hwaddr.sa_data[4],
			(uint8_t) ifr.ifr_hwaddr.sa_data[5]);

out:
	close(fd);
}

#endif
