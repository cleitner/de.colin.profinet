/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/*
 * pnrt-transport.h
 *
 *  Created on: 22.07.2012
 *      Author: cleitner
 */

#ifndef PNRT_TRANSPORT_H_
#define PNRT_TRANSPORT_H_

#ifdef WIN32

#define WINVER 0x0502

#include <winsock2.h>
#include <process.h>
#else
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <net/if_arp.h>
#endif

#ifdef __APPLE__
#include <net/if_dl.h>
#include <net/if_types.h>
#include <net/bpf.h>
#endif

#ifdef __ANDROID__
/* In Android we use the platform/external/libpcap version, which is built as
 * a static library and is not installed in a `pcap' subdirectory */
#include <pcap.h>
#else
#include <pcap/pcap.h>
#endif

#include <assert.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "ipc.h"

#ifndef LINKTYPE_ETHERNET
#define LINKTYPE_ETHERNET 1
#endif

#define EXIT_INVALID_ARGUMENTS (EXIT_FAILURE + 0)
#define EXIT_PCAP_ERROR (EXIT_FAILURE + 1)
#define EXIT_INVALID_DATA (EXIT_FAILURE + 2)

#define MIN_PACKET_SIZE (6 + 6 + 4 + 2 + 42)
/* destination and source address, type, payload */
#define MAX_PACKET_SIZE (6 + 6 + 4 + 2 + 1500)


#define IPC_ID_QUIT 0
#define IPC_ID_SEND 1
#define IPC_ID_RECEIVE 2
#define IPC_ID_HELLO 3

#ifdef __cplusplus
extern "C" {
#endif

extern void usage(const char *prog);

extern int cmd_list(int argc, char **argv);
extern int cmd_send(int argc, char **argv);
extern int cmd_receive(int argc, char **argv);
extern int cmd_receive_lldp(int argc, char **argv);

extern void parse_mac(const char *str, uint8_t *mac);

extern void start_ipc_receiver(ipc_handler_t handler,
		ipc_call_handler_t call_handler);

#ifdef __cplusplus
}
#endif

#endif /* PNRT_TRANSPORT_H_ */
