/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/*
 * ipc.c
 *
 *  Created on: 21.07.2012
 *      Author: cleitner
 */

#include "ipc.h"

#include <assert.h>

#define FLAGS_CALL 0x0001
#define FLAGS_RESPONSE 0x0002

static bool send_internal(int32_t id, void* buf, int16_t len, int16_t flags);

extern bool ipc_send(int32_t id, void* buf, size_t len) {
	return send_internal(id, buf, (int16_t) len, 0);
}

extern bool ipc_receive(ipc_handler_t handler, ipc_call_handler_t call_handler) {

	int32_t id;
	int16_t flags;
	int16_t len;

	uint8_t buf[IPC_MAX_LEN];

	while (true) {
		if (fread(&id, sizeof(id), 1, stdin) != 1) {
			return false;
		}

		if (fread(&flags, sizeof(flags), 1, stdin) != 1) {
			return false;
		}

		if (fread(&len, sizeof(len), 1, stdin) != 1) {
			return false;
		}

		if (len < 0 || len > IPC_MAX_LEN) {
			return false;
		}

		if (len > 0) {
			if (len > 0 && fread(&buf[0], len, 1, stdin) != 1) {
				return false;
			}
		}

		if ((flags & FLAGS_CALL) != 0) {
			uint8_t response[IPC_MAX_LEN];
			size_t response_len = 0;

			bool result = call_handler(id, buf, (size_t) len, response, &response_len);

			send_internal(id, response, (int16_t) response_len, FLAGS_RESPONSE);

			if (!result) {
				break;
			}
		} else {
			if (!handler(id, buf, (size_t) len)) {
				break;
			}
		}
	}

	return true;
}

static bool send_internal(int32_t id, void* buf, int16_t len, int16_t flags) {

	assert((len >= 0) && (len <= IPC_MAX_LEN));

	ipc_critical_begin();

	if (fwrite(&id, sizeof(id), 1, stdout) != 1) {
		return false;
	}

	if (fwrite(&flags, sizeof(flags), 1, stdout) != 1) {
		return false;
	}

	if (fwrite(&len, sizeof(len), 1, stdout) != 1) {
		return false;
	}

	if (fwrite(buf, len, 1, stdout) != 1) {
		return false;
	}

	fflush(stdout);

	ipc_critical_end();

	return true;
}
