/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/*
 * ipc.h
 *
 *  Created on: 21.07.2012
 *      Author: cleitner
 *
 * A very simple IPC mechanism to communicate over stdin/stdout with a parent
 * process. The encoding is simple (all in native byte order):
 *
 * <pre>
 * +--------+-----------+---------+-------------+
 * | i32 id | i16 flags | i16 len | u8 data ... |
 * +--------+-----------+---------+-------------+
 * </pre>
 *
 * Flags is a Bit-Field:
 *
 * <pre>
 * Bit 0: Call - the sender expects a response with the same id
 * Bit 1: Response - a response
 * </pre>
 *
 * At most one call can be pending at the same time.
 */

#ifndef IPC_H_
#define IPC_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

#define IPC_MAX_LEN 4096

/**
 * Handler for incoming IPC messages.
 *
 * @return @c true if the receiver should continue handling IPC calls
 */
typedef bool (*ipc_handler_t)(int32_t id, void* buf, size_t len);

/**
 * Handler for incoming IPC calls. An IPC call returns a result synchronously.
 *
 * @return @c true if the receiver should continue handling IPC calls
 */
typedef bool (*ipc_call_handler_t)(int32_t id, void* in_buf, size_t in_len,
		void* out_buf, size_t* out_len);

/* Have to be provided by the threading environment */
extern void ipc_critical_begin(void);
extern void ipc_critical_end(void);

/**
 * Sends an IPC call to the given output.
 *
 * @return @c true if sending succeeded
 */
extern bool ipc_send(int32_t id, void* buf, size_t len);

/**
 * Reads incoming IPC calls and dispatches them to the handler until the
 * handler returns @c false or the stream is closed.
 *
 * @return @c true if the handler returned @c false, or @c false if receiving
 *         failed
 */
extern bool ipc_receive(ipc_handler_t handler, ipc_call_handler_t call_handler);

#ifdef __cplusplus
}
#endif

#endif /* IPC_H_ */
