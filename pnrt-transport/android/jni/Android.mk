# Calling my-dir twice fails somehow
MY_DIR := $(call my-dir)
LOCAL_PATH := $(MY_DIR)

include $(call all-subdir-makefiles)

LOCAL_PATH := $(MY_DIR)
include $(CLEAR_VARS)

LOCAL_MODULE    := pnrt-transport
LOCAL_SRC_FILES := ../../ipc.c ../../pnrt-send.c ../../pnrt-receive.c ../../pnrt-transport.c
LOCAL_C_INCLUDES := $(LOCAL_PATH)/libpcap
LOCAL_STATIC_LIBRARIES := libpcap

include $(BUILD_EXECUTABLE)


