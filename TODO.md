TODO
====

 * Fix all the TODOs :)
 * Use Javolution as much as possible, especially the Struct class
 * Rethink all the synchronized methods and maybe change the MT architecture to
   something that a sane person can still fully understand
 * Write a Device class. All the building blocks are there
