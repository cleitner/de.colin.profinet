package de.colin.profinet.io;

import static org.junit.Assert.*;

import org.junit.Test;

public class FrameTest {

	public static Frame buildFrameA() {
		FrameBuilder b = FrameBuilder.create();
		
		b.iocs(0);
		b.iocs(0, 0x8000);
		b.iocs(0, 0x8001);
		b.iocs(0, 0x8002);
		b.iocs(1);
		b.iocs(2);
		
		b.dataObject(2, 6);
		for (int s = 3; s < 16; s++) {
			b.dataObject(s, 1);
		}
		
		return b.build();
	}

	public static Frame buildFrameB() {
		FrameBuilder b = FrameBuilder.create();
		
		b.iocs(2);
		for (int s = 3; s < 16; s++) {
			b.iocs(s);
		}
		
		b.dataObject(0, 0);
		b.dataObject(0, 0x8000, 0);
		b.dataObject(0, 0x8001, 0);
		b.dataObject(0, 0x8002, 0);
		b.dataObject(1, 0);
		b.dataObject(2, 6);
		
		return b.build();
	}
	
	@Test
	public void testBuilder() {
		Frame frame = buildFrameA();
		
		assertEquals(40, frame.getLength());
		
		assertEquals(5, frame.getIOConsumerStatus()[5].getFrameOffset());
		assertEquals(35, frame.getIODataObjects()[12].getFrameOffset());
	}

}
