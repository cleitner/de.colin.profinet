package de.colin.profinet.io;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

import org.junit.Test;

public class IODataServiceTest {

	@Test
	public void testIO() throws IOException, InterruptedException {
/*
		IODataService serviceA = new IODataService();
		IODataService serviceB = new IODataService();

		try {

			serviceA.setUp(FrameTest.buildFrameA(), FrameTest.buildFrameB(),
					TransportFactory.RT_CLASS_UDP, new byte[6], new byte[6],
					new InetSocketAddress("127.0.0.1", 0x8892),
					new InetSocketAddress("127.0.0.1", 63001), 10,
					(short) 0xc000, (short) 0xc001);

			serviceB.setUp(FrameTest.buildFrameB(), FrameTest.buildFrameA(),
					TransportFactory.RT_CLASS_UDP, new byte[6], new byte[6],
					new InetSocketAddress("127.0.0.1", 63001),
					new InetSocketAddress("127.0.0.1", 0x8892), 10,
					(short) 0xc001, (short) 0xc000);

			Frame rxA = serviceA.getIncomingFrame();
			ByteBuffer sliceA = rxA.dataSlice(0);
			
			Frame txB = serviceB.getOutgoingFrame();
			ByteBuffer sliceB = txB.dataSlice(0);

			txB.setProviderState(true);
			txB.setPrimary(true);
			txB.setDataValid(true);
			txB.setProblemIndicator(false);
			
			serviceA.start();
			serviceB.start();

			for (int n = 0; n < 500; n++) {
				synchronized (rxA) {
					rxA.wait(50);
				
					System.out.println(rxA.isDataGood(0));
					System.out.println(sliceA.get(0) & 0xff);
				}
				
				synchronized (txB) {
					txB.setDataGood(0, true);
					sliceB.put(0, (byte)n);
				}
			}

		} finally {
			serviceB.stop();
			serviceA.stop();
		}
		*/
		throw new AssertionError("Testcase hasn't been updated to the saner API using onSend/onReceive");
	}

}
