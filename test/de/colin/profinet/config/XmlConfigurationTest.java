package de.colin.profinet.config;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XmlConfigurationTest {

	@Test
	public void testA() throws SAXException, IOException {
		DeviceConfiguration config = XmlConfigurationParser.parse(new InputSource(XmlConfigurationTest.class.getResourceAsStream("testA.xml")));
		
		assertEquals(1234, config.getVendorId());
		assertEquals(0x123, config.getDeviceId());
		
		assertEquals(2, config.getModules().size());

		assertEquals(1, config.getModules().get(0).getSlot());
		assertEquals(123, config.getModules().get(0).getIdentNumber());
		assertEquals(1, config.getModules().get(0).getSubmodules().size());
		assertEquals(6, config.getModules().get(0).getSubmodules().get(0).getInputLength());
		assertEquals(3, config.getModules().get(0).getSubmodules().get(0).getOutputLength());

		assertEquals(2, config.getModules().get(1).getSlot());
		assertEquals(456, config.getModules().get(1).getIdentNumber());
		assertEquals(1, config.getModules().get(1).getSubmodules().size());
		assertEquals(1, config.getModules().get(1).getSubmodules().get(0).getInputLength());
		assertEquals(2, config.getModules().get(1).getSubmodules().get(0).getOutputLength());
	}

}
