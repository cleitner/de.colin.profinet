package de.colin.profinet;

import static org.junit.Assert.*;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.UUID;

import org.junit.Test;


public class BufferAdapterTest {

    @Test
    public void testAlignment() {
        ByteBuffer buf = ByteBuffer.allocate(5);
        buf.order(ByteOrder.LITTLE_ENDIAN);

        buf.put((byte) 1);
        buf.putShort((short) 0x1234);
        buf.put((byte) 0x56);
        buf.flip();

        BufferAdapter a = new BufferAdapter(buf);

        a.get();
        assertEquals(0x5612, a.getAlignedShort());
    }

    @Test
    public void testUUID() {
        UUID uuid1 = UUID.fromString("DEA00002-6C97-11D1-8271-00A02442DF7D");
        UUID uuid2 = UUID.randomUUID();

        BufferAdapter a = new BufferAdapter(50);
        a.order(ByteOrder.LITTLE_ENDIAN);

        a.putUUID(uuid1);
        a.flip();
        assertEquals(uuid1, a.getUUID());

        a.order(ByteOrder.BIG_ENDIAN);

        a.clear();
        a.putInt(103298098);
        a.putAlignedUUID(uuid2);

        a.position(2);
        assertEquals(uuid2, a.getAlignedUUID());
    }
    
    @Test
    public void testUUID2() {
        UUID failedUUID = UUID.fromString("a39266fe-f96d-47f7-b44d-9c4602b539b1");
        
        BufferAdapter a = new BufferAdapter(16);
        a.putUUID(failedUUID);
        a.flip();
        UUID readUUID = a.getUUID();
        
        assertEquals(failedUUID, readUUID);
    }
}
