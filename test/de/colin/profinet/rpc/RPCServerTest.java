package de.colin.profinet.rpc;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.Test;

import de.colin.profinet.rpc.RPCClient;
import de.colin.profinet.rpc.RPCServer;

public class RPCServerTest {

    @Test
    public void testStartupAndShutdown() throws IOException {
        RPCServer s = new RPCServer() {
            @Override
            protected int onCall(SocketAddress address, UUID activity,
                    UUID object, UUID interface_, int operation,
                    ByteBuffer args, ByteBuffer result) throws RPCReject {
                return 0;
            }
        };

        s.start();
        s.stop();
    }

    /*
     * @Test public void testConnect() throws IOException { InetSocketAddress
     * address = new InetSocketAddress(InetAddress.getLocalHost(), 63011);
     * 
     * RPCServer s = new RPCServer();
     * 
     * s.start(address);
     * 
     * DatagramChannel ch = DatagramChannel.open();
     * ch.socket().setSoTimeout(1000);
     * 
     * ByteBuffer req = ByteBuffer.wrap(new byte[] { 0, 1, 2, 3, 4 });
     * ch.send(req, address); req.flip();
     * 
     * ByteBuffer rsp = ByteBuffer.allocate(100); ch.receive(rsp); rsp.flip();
     * 
     * assertTrue(req.equals(rsp));
     * 
     * ch.close();
     * 
     * s.stop(); }
     */

    @Test
    public void testCall() throws IOException {
        final AtomicReference<UUID> calledObj = new AtomicReference<UUID>();
        final AtomicReference<UUID> calledIf = new AtomicReference<UUID>();
        final AtomicInteger calledMethod = new AtomicInteger();
        final AtomicReference<ByteBuffer> calledArgs = new AtomicReference<ByteBuffer>();

        InetSocketAddress address = new InetSocketAddress(
                InetAddress.getLocalHost(), 34964 /*63011*/);

        RPCServer s = new RPCServer() {

            @Override
            protected synchronized int onCall(SocketAddress address,
                    UUID activity, UUID object, UUID interface_, int method,
                    ByteBuffer args, ByteBuffer result) throws RPCReject {

                calledObj.set(object);
                calledIf.set(interface_);
                calledMethod.set(method);
                
                ByteBuffer argsCopy = ByteBuffer.allocate(args.remaining());
                argsCopy.put(args);
                argsCopy.flip();
                calledArgs.set(argsCopy);

                result.putInt(123456);
                result.flip();
                
                return 0;
            }

        };

        s.start(address);

        RPCClient c = new RPCClient();

        try {
            UUID originalObj = PNIORPC.UUID_IO_ObjectInstance((short)1, (short)1, (short)0) /*UUID.randomUUID()*/;
            UUID originalInterface = PNIORPC.UUID_IO_DeviceInterface /*UUID.randomUUID()*/;
            short originalMethod = 0;

            ByteBuffer originalArgs = ByteBuffer.allocate(10);
            ByteBuffer result = ByteBuffer.allocate(10);

            originalArgs.putInt(7890);
            originalArgs.flip();
            
            c.call(address, originalObj, originalInterface, originalMethod,
                    originalArgs, result);

            originalArgs.flip();
            
            assertEquals(originalObj, calledObj.get());
            assertEquals(originalInterface, calledIf.get());
            assertEquals(originalMethod, calledMethod.get());
            assertEquals(originalArgs, calledArgs.get());
            assertEquals(123456, result.getInt());
            
        } finally {
            c.close();
            s.stop();
        }
    }
}
