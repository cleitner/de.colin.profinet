/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package de.colin.timer;

/**
 * A clock with millisecond accuracy.
 * 
 * @author Colin Leitner
 */
public interface Clock {

    /**
     * Returns the current time in milliseconds.
     * 
     * @return the time in milliseconds
     */
    int getTime();

    /**
     * Starts a timer to be triggered after the given timeout. If the timer has
     * already been started, the trigger time will be changed.
     * 
     * Implementing clock classes have to call {@link Timer#timeout(int)
     * timeout} if the timeout expires.
     * 
     * @param timer
     *            the timer to be triggered after the timeout
     * @param triggerTime
     *            the trigger time in milliseconds
     */
    void startTimer(Timer timer, int timeout);

    /**
     * Stops the given timer. Has no effect if the timer hasn't been started.
     * 
     * @param timer
     *            the timer to be stopped
     */
    void stopTimer(Timer timer);

}
