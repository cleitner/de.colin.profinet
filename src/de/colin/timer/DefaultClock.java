/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package de.colin.timer;

import java.util.PriorityQueue;

/**
 * Default clock implementation. The clock must be updated in regular intervals
 * (the clock resolution).
 * 
 * @author Colin Leitner
 */
public class DefaultClock implements Clock {

    private static class TimerEntry implements Comparable<TimerEntry> {
        int triggerTime;
        Timer timer;

        public TimerEntry(int triggerTime, Timer timer) {
            this.triggerTime = triggerTime;
            this.timer = timer;
        }

        @Override
        public int compareTo(TimerEntry o) {
            if (triggerTime < o.triggerTime) {
                return -1;
            } else if (triggerTime > o.triggerTime) {
                return 1;
            }

            return 0;
        }

        // We abuse hashCode/equals to only include the timer and not the
        // timeout. This allows us to use remove(e) and add(e). Changing the
        // priority would be a problem otherwise
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((timer == null) ? 0 : timer.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            TimerEntry other = (TimerEntry) obj;
            if (timer == null) {
                if (other.timer != null)
                    return false;
            } else if (!timer.equals(other.timer))
                return false;
            return true;
        }
    }

    private int time;
    private final PriorityQueue<TimerEntry> timers;

    public DefaultClock() {
        timers = new PriorityQueue<DefaultClock.TimerEntry>();
    }

    /**
     * Resets the clock. Any pending timers will <em>not</em> be triggered.
     */
    public synchronized void reset() {
        time = 0;
        timers.clear();
    }

    /**
     * Updates the clock through the {@code System.currentTimeMillis()}.
     */
    public void update() {
        update((int) (System.currentTimeMillis() & 0x7FFFFFFF));
    }

    /**
     * Updates the clock and triggers any outstanding timers.
     * 
     * @param time
     *            current time in milliseconds
     */
    public synchronized void update(int time) {
        if (time < this.time) {
            throw new IllegalArgumentException();
        }

        this.time = time;

        TimerEntry e;
        do {
            e = timers.peek();
            if (e != null) {
                if (e.triggerTime < time) {
                    timers.remove();

                    e.timer.timeout(time);
                } else {
                    break;
                }
            }
        } while (e != null);
    }

    @Override
    public int getTime() {
        return time;
    }

    @Override
    public synchronized void startTimer(Timer timer, int triggerTime) {
        if (timer.getClock() != this) {
            throw new NullPointerException();
        }

        if (triggerTime < time) {
            throw new IllegalArgumentException();
        }

        TimerEntry e = new TimerEntry(triggerTime, timer);
        this.timers.remove(e);
        this.timers.add(e);
    }

    @Override
    public synchronized void stopTimer(Timer timer) {
        if (timer.getClock() != this) {
            throw new NullPointerException();
        }

        TimerEntry e = new TimerEntry(0, timer);
        this.timers.remove(e);
    }
}
