/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.timer;

/**
 * Simple timer for non-platform specific clocks.
 * 
 * @author Colin Leitner
 */
public class Timer {

    private final Clock clock;
    private final TimerListener listener;

    private boolean triggered;

    /**
     * Creates a timer with no listener.
     * 
     * @param clock
     *            the time providing clock
     */
    public Timer(Clock clock) {
        this(clock, null);
    }

    /**
     * Creates a timer with a listener.
     * 
     * @param clock
     *            the time providing clock
     * @param listener
     *            the listener that is called when this timer is triggered
     */
    public Timer(Clock clock, TimerListener listener) {
        if (clock == null) {
            throw new NullPointerException();
        }

        this.clock = clock;
        this.listener = listener;
    }

    /**
     * Returns the clock that provides the time to this timer.
     * 
     * @return a clock instance
     */
    public Clock getClock() {
        return clock;
    }

    /**
     * Starts the timer to be triggered after the given timeout
     * 
     * @param timeout
     *            the timeout after which the timer should be triggered
     */
    public void start(int timeout) {
        triggered = false;

        clock.startTimer(this, clock.getTime() + timeout);
    }

    /**
     * Starts the timer to be triggered after the next interval elapsed. This
     * makes the timer stable, although it's still possible to miss triggers if
     * the clock accuracy is too coarse.
     * 
     * @param interval
     *            the timer interval
     */
    public void startStable(int interval) {
        triggered = false;

        int nextTriggerTime = clock.getTime();

        nextTriggerTime = nextTriggerTime + interval - nextTriggerTime
                % interval;

        clock.startTimer(this, nextTriggerTime);
    }

    public void stop() {
        clock.stopTimer(this);
    }

    /**
     * Triggers the timer. Should only be called by the clock.
     */
    public final void timeout(int time) {
        triggered = true;

        if (listener != null) {
            listener.onTimeout(this, time);
        }
    }

    /**
     * Returns whether this timer is triggered or not. After restarting the
     * timer, this method returns {@code false}.
     * 
     * @return {@code true} if the timer has timed out
     */
    public boolean isTriggered() {
        return triggered;
    }
}
