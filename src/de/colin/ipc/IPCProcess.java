/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.ipc;

import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * IPC controller to communicate with a child process through its standard input
 * and standard output streams.
 * 
 * @author Colin Leitner
 */
public abstract class IPCProcess implements Runnable, Closeable {

    private static final Logger LOG = Logger.getLogger(IPCProcess.class
            .getName());

    private static enum State {
        STOPPED, STARTED, RUNNING
    }

    public static final int MAX_LEN = 4096;

    private static final int FLAGS_CALL = 0x0001;
    private static final int FLAGS_RESPONSE = 0x0002;

    private static class CallResponse {
        int id;
        ByteBuffer result;

        public CallResponse(int id, ByteBuffer result) {
            this.id = id;
            this.result = result;
        }
    }

    private AtomicReference<State> state = new AtomicReference<State>(
            State.STOPPED);

    private final Object sendLock = new Object();

    private Process process;
    private WritableByteChannel sink;
    private ReadableByteChannel source;
    private Thread receiverThread;

    /**
     * Used to exchange the call result between receive() and call(). Uses null
     * to signal an error to call()
     */
    private final Exchanger<CallResponse> responseExchange = new Exchanger<CallResponse>();

    // TODO: I removed the synchronized keyword here because it causes a
    // dead-lock if close is called from an external application and the
    // recevier thread
    private void cleanup() {
        if (sink != null) {
            try {
                sink.close();
            } catch (IOException e) {
                // Ignore
            }
        }

        if (source != null) {
            try {
                source.close();
            } catch (IOException e) {
                // Ignore
            }
        }

        if (process != null) {
            process.destroy();
        }

        try {
            responseExchange.exchange(null, 0, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (TimeoutException e) {
            // Ignore
        }

        if (receiverThread != null) {
            receiverThread.interrupt();

            try {
                receiverThread.join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        synchronized (sendLock) {
            sink = null;
            source = null;
            process = null;
        }

        receiverThread = null;
    }

    /**
     * Creates the child process.
     * 
     * @return the process instance
     * @throws IOException
     *             if spawning the process failed
     */
    protected abstract Process createProcess() throws IOException;

    /**
     * Called when the IPC received received a regular message
     * 
     * @param id
     *            message id
     * @param buf
     *            message
     */
    protected void onReceive(int id, ByteBuffer buf) {
        // Ignore incoming IPC calls. Subclasses will handle these
    }

    /**
     * Called when the IPC receiver received a call request from the child
     * process.
     * 
     * @param id
     *            message id
     * @param buf
     *            message
     * @return response for the child process
     */
    protected ByteBuffer onCall(int id, ByteBuffer buf) {
        // Ignore incoming IPC calls. Subclasses will handle these
        return ByteBuffer.allocate(0);
    }

    /**
     * Called when the IPC receiver fails to read a message from the child
     * process.
     * 
     * The child process has been killed when this handler is called.
     * 
     * @param e
     *            an exception that describes the problem
     */
    protected void onError(IOException e) {
        // Child process died
        LOG.log(Level.INFO, "IPC process died", e);
    }

    /**
     * Starts the child process. IPC messages can be sent to the process, but
     * the receiver has to be run by calling {@link #run()} to handle incoming
     * messages.
     * 
     * @throws IOException
     *             if spawning the process fails
     */
    public synchronized void start() throws IOException {
        if (state.get() != State.STOPPED) {
            throw new IllegalStateException();
        }

        try {
            process = createProcess();
            process.getErrorStream().close();

            sink = Channels.newChannel(process.getOutputStream());
            source = Channels.newChannel(process.getInputStream());
        } catch (IOException e) {
            cleanup();

            throw e;
        }

        state.set(State.STARTED);
    }

    /**
     * Starts the process and executes the IPC receiver in a new daemon thread.
     * 
     * @throws IOException
     *             if spawning the process fails
     */
    public synchronized void startAndRun() throws IOException {
        start();

        receiverThread = new Thread(this, "IPC-receiver");
        receiverThread.setDaemon(true);
        receiverThread.start();
    }

    /**
     * Runs the IPC receiver. This method will call the {@code onXXX} handlers
     * and return after the child process has exited.
     */
    @Override
    public void run() {
        synchronized (this) {
            if (state.get() != State.STARTED) {
                throw new IllegalStateException();
            }

            state.set(State.RUNNING);
        }

        try {
            receive();
        } catch (IOException ioe) {
            onError(ioe);
        }

        // TODO: There is still a window for deadlock here (I think) 
        if (state.get() == State.RUNNING) {
            close();
        }
    }

    private void receive() throws IOException {
        ByteBuffer header = ByteBuffer.allocate(8);
        header.order(ByteOrder.nativeOrder());

        while (true) {
            header.clear();
            while (header.hasRemaining()) {
                if (source.read(header) == -1) {
                    throw new EOFException();
                }
            }
            header.flip();

            int id = header.getInt();
            int flags = header.getShort() & 0xFFFF;
            int len = header.getShort() & 0xFFFF;

            if (len > MAX_LEN) {
                throw new IOException("Invalid message length");
            }

            ByteBuffer buf = ByteBuffer.allocate(len);
            while (buf.hasRemaining()) {
                if (source.read(buf) == -1) {
                    throw new EOFException();
                }
            }
            buf.flip();

            if ((flags & FLAGS_CALL) != 0) {
                ByteBuffer response = onCall(id, buf);

                synchronized (sendLock) {
                    sendInternal(id, FLAGS_RESPONSE, response);
                }

            } else if ((flags & FLAGS_RESPONSE) != 0) {
                try {
                    responseExchange.exchange(new CallResponse(id, buf));
                } catch (InterruptedException e) {
                    throw new IOException(e);
                }
            } else {
                onReceive(id, buf);
            }
        }
    }

    /**
     * Sends a message to the child process.
     * 
     * @param id
     *            the message id
     * @param buf
     *            the message content
     */
    public void send(int id, ByteBuffer buf) throws IOException {
        if (state.get() == State.STOPPED) {
            throw new IllegalStateException();
        }

        try {
            synchronized (sendLock) {
                sendInternal(id, 0, buf);
            }
        } catch (IOException e) {
            close();
            throw e;
        }
    }

    private void sendInternal(int id, int flags, ByteBuffer buf)
            throws IOException {
        if (buf.remaining() > MAX_LEN) {
            throw new IllegalArgumentException();
        }

        if (sink == null || process == null) {
            // We're closing down
            return;
        }

        ByteBuffer header = ByteBuffer.allocate(8);

        header.order(ByteOrder.nativeOrder());
        header.putInt(id);
        header.putShort((short) flags);
        header.putShort((short) buf.remaining());
        header.flip();

        sink.write(header);
        if (buf.hasRemaining()) {
            sink.write(buf);
        }

        process.getOutputStream().flush();
    }

    public ByteBuffer call(int id, ByteBuffer buf) throws IOException {
        if (state.get() == State.STOPPED) {
            throw new IllegalStateException();
        }

        CallResponse response;

        synchronized (sendLock) {
            sendInternal(id, FLAGS_CALL, buf);

            // Wait for the result while the sendLock is held. This ensures that
            // only one call can be active at a time
            try {
                // TODO: Handle broken receiver/close/etc.
                response = responseExchange.exchange(null);
                if (response == null) {
                    throw new IOException(
                            "Process couldn't provide a response anymore");
                }
            } catch (InterruptedException e) {
                close();

                throw new IOException(e);
            }

            if (response.id != id) {
                close();

                throw new IOException("Invalid IPC response id (got "
                        + response.id + ", expected " + id + ")");
            }
        }

        return response.result;
    }

    @Override
    public void close() {
        synchronized (this) {
            if (state.get() == State.STOPPED) {
                return;
            }

            state.set(State.STOPPED);
        }

        cleanup();
    }
}
