/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.io;

import java.util.ArrayList;
import java.util.List;

public class FrameBuilder {

    private final List<IOConsumerStatus> iocs = new ArrayList<IOConsumerStatus>();
    private final List<IODataObject> dataObjects = new ArrayList<IODataObject>();

    private int length = -1;
    private int offset;

    public static FrameBuilder create() {
        return new FrameBuilder();
    }

    public Frame build() {
        if (length == -1) {
            length = 1 + offset;
        }

        return new Frame(length, iocs.toArray(new IOConsumerStatus[0]),
                dataObjects.toArray(new IODataObject[0]));
    }

    public FrameBuilder length(int length) {
        this.length = length;

        return this;
    }

    public FrameBuilder iocs(int slot, int subslot, int offset) {
        iocs.add(new IOConsumerStatus((short) slot, (short) subslot, offset));

        this.offset = offset + 1;

        return this;
    }

    public FrameBuilder iocs(int slot, int subslot) {
        return iocs(slot, subslot, offset);
    }

    public FrameBuilder iocs(int slot) {
        return iocs(slot, 1, offset);
    }

    public FrameBuilder dataObject(int slot, int subslot, int offset, int length) {
        dataObjects.add(new IODataObject((short) slot, (short) subslot, offset,
                length));

        this.offset = offset + length + 1;

        return this;
    }

    public FrameBuilder dataObject(int slot, int length) {
        return dataObject(slot, 1, offset, length);
    }

    public FrameBuilder dataObject(int slot, int subslot, int length) {
        return dataObject(slot, subslot, offset, length);
    }
}
