/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.io;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.colin.profinet.config.DeviceConfiguration;
import de.colin.profinet.config.SubmoduleConfiguration;
import de.colin.profinet.rpc.blocks.IOCRBlockReq;

/**
 * An I/O frame representation.
 */
public class Frame {

    public static final int MAX_FRAME_LENGTH = 1440;

    public static Frame createFrameFromIOCR(DeviceConfiguration config,
            boolean input, IOCRBlockReq cr) {

        List<IOConsumerStatus> iocs = new ArrayList<IOConsumerStatus>();
        List<IODataObject> dataObjects = new ArrayList<IODataObject>();

        for (IOCRBlockReq.API api : cr.APIs) {
            for (IOCRBlockReq.IOCS i : api.IOCSs) {
                iocs.add(new IOConsumerStatus((short) i.SlotNumber,
                        (short) i.SubslotNumber, (short) i.IOCSFrameOffset));
            }

            for (IOCRBlockReq.IODataObject d : api.IODataObjects) {
                SubmoduleConfiguration submodule = config.getModule(
                        d.SlotNumber).getSubmodule(d.SubslotNumber);
                int length;
                if (input) {
                    length = submodule.getInputLength();
                } else {
                    length = submodule.getOutputLength();
                }

                dataObjects.add(new IODataObject((short) d.SlotNumber,
                        (short) d.SubslotNumber,
                        (short) d.IODataObjectFrameOffset, (short) length));
            }
        }

        Frame frame = new Frame(cr.DataLength,
                iocs.toArray(new IOConsumerStatus[0]),
                dataObjects.toArray(new IODataObject[0]));
        frame.setBytes(new byte[cr.DataLength + 6]);
        frame.setFrameID(cr.FrameID);
        return frame;
    }

    private final int length;
    private final IOConsumerStatus[] ioConsumerStatus;
    private final IODataObject[] ioDataObjects;

    private ByteBuffer bytes;
    private ByteBuffer data;

    public Frame(int length, IOConsumerStatus[] iocs, IODataObject[] dataObjects) {
        if ((length < 0) || (length > MAX_FRAME_LENGTH)) {
            throw new IllegalArgumentException("Invalid frame length");
        }

        this.length = length;

        ioConsumerStatus = Arrays.copyOf(iocs, iocs.length);

        for (IOConsumerStatus cs : ioConsumerStatus) {
            if ((cs.getFrameOffset() < 0) || (cs.getFrameOffset() >= length)) {
                throw new IllegalArgumentException(
                        "IOCS offset doesn't fit into frame");
            }
        }

        ioDataObjects = Arrays.copyOf(dataObjects, dataObjects.length);

        for (IODataObject iod : ioDataObjects) {
            if ((iod.getFrameOffset() < 0)
                    || ((iod.getFrameOffset() + iod.getLength()) >= length)) {
                throw new IllegalArgumentException(
                        "IO data object doesn't fit into frame");
            }
        }
    }

    public int getLength() {
        return length;
    }

    public void setBytes(byte[] data) {
        if (data.length < (length + 6)) {
            this.bytes = null;
            this.data = null;

            throw new IllegalArgumentException("The frame length is too short");
        }

        setBytes(ByteBuffer.wrap(data, 0, length + 6));
    }

    public void setBytes(ByteBuffer data) {
        this.bytes = data.slice();

        if (this.bytes.remaining() < (length + 6)) {
            this.bytes = null;
            this.data = null;

            throw new IllegalArgumentException("The frame length is too short");
        }

        this.bytes.order(ByteOrder.BIG_ENDIAN);

        this.bytes.position(2).limit(length);
        this.data = this.bytes.slice();

        this.bytes.clear();
    }

    public ByteBuffer getBytes() {
        return bytes;
    }

    public void setFrameID(int frameID) {
        if (frameID < 0 || frameID > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        bytes.position(0);
        bytes.putShort((short) frameID);
    }

    public int getFrameID() {
        bytes.position(0);
        return bytes.getShort() & 0xFFFF;
    }

    public void setCycleCounter(short counter) {
        bytes.position(bytes.capacity() - 4);
        bytes.putShort(counter);
    }

    public short getCycleCounter() {
        bytes.position(bytes.capacity() - 4);
        return bytes.getShort();
    }

    // -- Data Status --

    public byte getDataStatus() {
        return bytes.get(bytes.capacity() - 2);
    }

    public void setDataStatus(byte status) {
        bytes.put(bytes.capacity() - 2, status);
    }

    public void setProblemIndicator(boolean problem) {
        byte dataStatus = getDataStatus();

        if (problem) {
            dataStatus &= 0xdf;
        } else {
            dataStatus |= 0x20;
        }

        setDataStatus(dataStatus);
    }

    public boolean getProblemIndicator() {
        return (getDataStatus() & 0x20) == 0;
    }

    public void setProviderState(boolean run) {
        byte dataStatus = getDataStatus();

        if (run) {
            dataStatus |= 0x10;
        } else {
            dataStatus &= 0xef;
        }

        setDataStatus(dataStatus);
    }

    public boolean getProviderState() {
        return (getDataStatus() & 0x10) != 0;
    }

    public void setDataValid(boolean valid) {
        byte dataStatus = getDataStatus();

        if (valid) {
            dataStatus |= 0x04;
        } else {
            dataStatus &= 0xfb;
        }

        setDataStatus(dataStatus);
    }

    public boolean isDataValid() {
        return (getDataStatus() & 0x04) != 0;
    }

    public void setPrimary(boolean primary) {
        byte dataStatus = getDataStatus();

        if (primary) {
            dataStatus |= 0x01;
        } else {
            dataStatus &= 0xfe;
        }

        setDataStatus(dataStatus);
    }

    public boolean isPrimary() {
        return (getDataStatus() & 0x01) != 0;
    }

    // -- I/O CS --

    public IOConsumerStatus[] getIOConsumerStatus() {
        return ioConsumerStatus;
    }

    public void setConsumerDataGood(int iocsIndex, boolean good) {
        ioConsumerStatus[iocsIndex].setDataGood(data, good);
    }

    public boolean isConsumerDataGood(int iocsIndex) {
        return ioConsumerStatus[iocsIndex].isDataGood(data);
    }

    // -- I/O Data --

    /**
     * Returns the IOD index for the given slot/subslot. This is a linear
     * search.
     * 
     * @return the IOD index or {@literal -1}
     */
    public int findIODIndex(int slot, int subslot) {
        int index = 0;
        for (IODataObject iod : ioDataObjects) {
            if (iod.getSlot() == slot && iod.getSubslot() == subslot) {
                return index;
            }
            index += 1;
        }

        return -1;
    }

    public IODataObject[] getIODataObjects() {
        return ioDataObjects;
    }

    public void setDataGood(int iodIndex, boolean good) {
        ioDataObjects[iodIndex].setDataGood(data, good);
    }

    public boolean isDataGood(int iodIndex) {
        return ioDataObjects[iodIndex].isDataGood(data);
    }

    public ByteBuffer dataSlice(int iodIndex) {
        return ioDataObjects[iodIndex].dataSlice(data);
    }
}
