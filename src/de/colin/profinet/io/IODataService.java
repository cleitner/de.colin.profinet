/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.io;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.colin.profinet.pnrt.PNRTTransport;

/**
 * This class handles network I/O for RT data.
 * 
 * Every {@code IOService} instance handles I/O traffic on a specific Ethernet
 * device, regardless of the transport type (raw Ethernet or UDP).
 * 
 * @see "8.3.2 IO Data ASE"
 */
public class IODataService {

    private static final Logger LOG = Logger.getLogger(IODataService.class
            .getName());

    private final IODataHandler handler;

    private final AtomicBoolean running = new AtomicBoolean();

    private final int sendInterval;

    private final Frame rxFrame;
    private final Frame txFrame;

    private final int rxFrameID;
    private final int txFrameID;

    private final SocketAddress remoteAddress;
    private final PNRTTransport transport;

    private long startTimestamp;

    private final AtomicLong timeout = new AtomicLong();
    private final AtomicLong lastReceived = new AtomicLong();

    private CyclicBarrier barrier = new CyclicBarrier(2);

    public IODataService(IODataHandler handler, PNRTTransport transport,
            int sendInterval, Frame txFrame, Frame rxFrame,
            SocketAddress remoteAddress) {
        if (handler == null) {
            throw new NullPointerException();
        }

        if (transport == null) {
            throw new IllegalArgumentException();
        }

        if (sendInterval < 0) {
            throw new IllegalArgumentException();
        }

        if (rxFrame == null || txFrame == null) {
            throw new NullPointerException();
        }

        this.handler = handler;

        this.transport = transport;

        this.sendInterval = sendInterval;

        this.txFrame = txFrame;
        this.txFrameID = txFrame.getFrameID();
        this.rxFrame = rxFrame;
        this.rxFrameID = rxFrame.getFrameID();

        this.remoteAddress = remoteAddress;
    }

    public synchronized void start(Executor executor) throws IOException {
        if (running.getAndSet(true)) {
            throw new IllegalStateException();
        }

        executor.execute(new Runnable() {
            @Override
            public void run() {
                Thread.currentThread().setName("PROFINET IODataService Sender");
                sendFrames();
            }
        });

        startTimestamp = System.nanoTime();
        lastReceived.set(startTimestamp);

        // This barrier ensures that both tasks have copied the transport
        // instances
        try {
            barrier.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (BrokenBarrierException e) {
            // Ignore
            LOG.log(Level.WARNING, "Broken barrier", e);
        }
    }

    public synchronized void stop() {
        running.set(false);
    }

    /**
     * Sets the timeout for this IO data service.
     * 
     * @param timeout
     *            the timeout in nanoseconds. 0 or less disables the timeout
     */
    public void setTimeout(long timeout) {
        this.timeout.set(timeout);
    }

    private void checkTimeout() {
        long timeoutValue = timeout.get();
        if (timeoutValue <= 0) {
            return;
        }

        long lastReceivedValue = lastReceived.get();
        long now = System.nanoTime();

        if (now > (lastReceivedValue + timeoutValue)) {
            // Timeout
            handler.onTimeout(this, now - startTimestamp);
        }
    }

    private void sendFrames() {
        try {
            barrier.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return;
        } catch (BrokenBarrierException e) {
            LOG.log(Level.WARNING, "Broken barrier", e);
        }

        final ByteBuffer sendBuffer = ByteBuffer
                .allocate(Frame.MAX_FRAME_LENGTH + 6);

        sendBuffer.order(ByteOrder.BIG_ENDIAN);

        final long cycleStart = System.nanoTime();

        while (running.get()) {

            checkTimeout();

            final long frameStart = System.nanoTime();

            short cycle = (short) (((frameStart - cycleStart) / 32500) & 0xFFFF);

            sendBuffer.clear();
            txFrame.setCycleCounter(cycle);

            handler.onSend(this, frameStart - startTimestamp, txFrame);

            ByteBuffer buf = txFrame.getBytes();
            buf.clear();

            sendBuffer.put(buf);
            sendBuffer.flip();

            // Skip frame ID
            sendBuffer.position(2);

            try {
                transport.send(remoteAddress, txFrameID, sendBuffer);
            } catch (IOException ioe) {
                LOG.log(Level.FINE, "I/O error while sending RT-IO frame", ioe);
                break;
            }

            try {
                Thread.sleep(sendInterval);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
                break;
            }
        }
    }

    public void handleFrame(int frameID, ByteBuffer buf) {
        long now = System.nanoTime();

        int len = buf.remaining();

        buf.order(ByteOrder.BIG_ENDIAN);

        if (frameID == rxFrameID) {
            if (len != rxFrame.getLength() + 4) {
                LOG.warning("Received packet with wrong size (is " + len
                        + ", expecting " + (rxFrame.getLength() + 4) + ")");
            }

            lastReceived.set(now);

            buf.position(0);

            rxFrame.getBytes().clear();
            rxFrame.getBytes().position(2);
            rxFrame.getBytes().put(buf);

            handler.onReceive(this, now - startTimestamp, rxFrame);
        } else {
            // Unknown ID. Ignore it
        }
    }
}
