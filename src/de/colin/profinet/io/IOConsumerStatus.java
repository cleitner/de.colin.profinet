/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.io;

import java.nio.ByteBuffer;

public class IOConsumerStatus {
    private final int slot;
    private final int subslot;
    private final int frameOffset;

    public IOConsumerStatus(short slot, short subslot, int frameOffset) {
        if (slot < 0 || slot > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (subslot < 0 || subslot > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (frameOffset < 0) {
            throw new IllegalArgumentException();
        }

        this.slot = slot;
        this.subslot = subslot;
        this.frameOffset = frameOffset;
    }

    public int getFrameOffset() {
        return frameOffset;
    }

    public int getSlot() {
        return slot;
    }

    public int getSubslot() {
        return subslot;
    }

    public void setDataGood(ByteBuffer frame, boolean good) {
        int index = frameOffset;

        if (good) {
            frame.put(index, (byte) (frame.get(index) | 0x80));
        } else {
            frame.put(index, (byte) (frame.get(index) & 0x7f));
        }
    }

    public boolean isDataGood(ByteBuffer frame) {
        int index = frameOffset;
        return (frame.get(index) & 0x80) != 0;
    }
}