/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.io;

/**
 * Handler for incoming and outgoing data. As the {@code IODataService} is
 * identical in controllers and devices we avoid the terms {@literal input} and
 * {@literal output} as these are by convention tied to the controller view.
 */
public interface IODataHandler {

    /**
     * Called to update a frame before sending it. The data service instance
     * will always use the same frame for sending, so it will always contain the
     * state from the last call.
     * 
     * @param service
     *            the service that called this handler
     * @param timestamp
     *            the timestamp since sending began in nanoseconds. this value
     *            is used for the outgoing cycle counter in PN-IO and can be
     *            used for timing
     * @param frame
     *            the outgoing frame
     */
    void onSend(IODataService service, long timestamp, Frame frame);

    /**
     * Called with the most recent incoming frame, right after reception.
     * 
     * @param service
     *            the service that called this handler
     * @param timestamp
     *            the timestamp since reception began in nanoseconds. This
     *            timestamp reflects the same timer used for the {@code onSend}
     *            timestamp.
     * @param frame
     *            the updated frame
     */
    void onReceive(IODataService service, long timestamp, Frame frame);

    /**
     * Called when the receiver hasn't received a frame in the timeout interval.
     * The handler will be called on every send cycle until the timeout is
     * disabled.
     * 
     * @param service
     *            the service that called this handler
     * @param timestamp
     *            the timestamp since reception began in nanoseconds
     */
    void onTimeout(IODataService service, long timestamp);
}
