/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.colin.profinet.config.DeviceConfiguration;
import de.colin.profinet.io.Frame;
import de.colin.profinet.io.IODataHandler;
import de.colin.profinet.io.IODataService;
import de.colin.profinet.pnrt.DCP;
import de.colin.profinet.pnrt.DCPInfo;
import de.colin.profinet.pnrt.DCPStationHandler;
import de.colin.profinet.pnrt.EthernetUtils;
import de.colin.profinet.pnrt.MACAddress;
import de.colin.profinet.pnrt.PNRTHandler;
import de.colin.profinet.pnrt.PNRTTransport;
import de.colin.profinet.rpc.PNIOError;
import de.colin.profinet.rpc.PNIORPC;
import de.colin.profinet.rpc.RPCClient;
import de.colin.profinet.rpc.RPCReject;
import de.colin.profinet.rpc.RPCServer;
import de.colin.profinet.rpc.blocks.AlarmAck;
import de.colin.profinet.rpc.blocks.AlarmNotification;
import de.colin.profinet.rpc.blocks.BlockBuilder;
import de.colin.profinet.rpc.blocks.BlockHandler;
import de.colin.profinet.rpc.blocks.BlockReader;
import de.colin.profinet.rpc.blocks.ConnectReq;
import de.colin.profinet.rpc.blocks.ConnectRes;
import de.colin.profinet.rpc.blocks.IOCRBlockReq;
import de.colin.profinet.rpc.blocks.IOCRBlockRes;
import de.colin.profinet.rpc.blocks.IODBlockReq;
import de.colin.profinet.rpc.blocks.IODBlockRes;
import de.colin.profinet.rpc.blocks.IODReadReqHeader;
import de.colin.profinet.rpc.blocks.IODWriteReqHeader;

/**
 * A PROFINET capable controller.
 * 
 * <p>
 * By design every {@code Controller} instance can handle exactly one connected
 * device. To synchronize execution between multiple controllers the executor
 * can be shared.
 * 
 * <p>
 * The controller has three states, which limit the methods that can be called
 * at a certain point in time.
 * 
 * <p>
 * If the controller has been created and not
 * {@link #start(InetSocketAddress, boolean) started} yet, no communication of
 * any kind is established. You can't issue DCP commands and won't be notified
 * of available stations.
 * 
 * <p>
 * In the started state the controller will listen to DCP notifications and
 * allow to issue DCP commands to arbitrary stations until {@link #stop()
 * stopped}. The controller starts two IPC processes to handle the PN-RT
 * communication.
 * 
 * <p>
 * The controller can then
 * {@link #connect(InetSocketAddress, int, int, int, DeviceConfiguration)
 * connect} to a station. This enables I/O exchange until the station is
 * {@link #release() released}.
 * 
 * <p>
 * Stations require the controller to complete the {@link #parameterEnd()
 * parametrization} before sending "application ready", after which I/O exchange
 * is going live (meaning that the watchdog begins to matter).
 * 
 * <p>
 * A full startup sequence would look like this:
 * 
 * <pre>
 * 
 * InetAddress address = InetAddress.getByAddress(new byte[] { 10, 1, 2, 3 })
 * c.start(localAddress)
 * dev = c.findStationByName("my-device")
 * c.setStationIPAddress(dev.getPNRTAddress(), address)
 * c.connect(address, config)
 * ...
 * c.write(...)
 * ...
 * c.parameterEnd()
 * </pre>
 * 
 * <p>
 * After application ready has been received the controller might decide to
 * release the device after it's done:
 * 
 * <pre>
 * c.release()
 * c.stop()
 * </pre>
 * 
 * @author Colin Leitner
 */
public class Controller {

    /**
     * <pre>
     * The controller has a number of states that control which objects are
     * valid and running:
     * 
     * STOPPED:
     * 
     * no communication, all IDs etc. are invalid. This is the initial and
     * "closed" state. Errors in the PNRT transport result in this state.
     * 
     * STARTED:
     * 
     * PNRT transport started, if not in UDP mode the DCP identity cache is
     * updated every second. I/O and CM timeout result in this state. 
     * 
     * CONNECTED:
     * 
     * CM connection succeeded, I/O frame and alarm IDs are known. I/O is
     * updated. 
     * 
     * RUNNING:
     * 
     * Application ready received.
     * 
     * We provide three guarantees:
     * 
     * 1. all asynchronous tasks are executed in the provided executor, giving
     * full thread control to the user.
     * 
     * 2. stop can always be called
     * 
     * 3. All state changing calls synchronize on the controller instance
     * </pre>
     */

    private enum State {
        STOPPED, STARTED, CONNECTED, RUNNING;
    }

    private static final Logger LOG = Logger.getLogger(Controller.class
            .getName());

    private static class StationInfo {
        long lastUpdate;
        DCPInfo dcpInfo;
    }

    private final PNRTHandler pnrtHandler = new PNRTHandler() {
        @Override
        public void onFrame(PNRTTransport transport, SocketAddress source,
                int frameID, ByteBuffer data) {
            onPNRTFrame(source, frameID, data);
        }

        @Override
        public void onClose(PNRTTransport transport, Exception e) {
            onPNRTClose(e);
        }
    };

    private final DCPStationHandler stationHandler = new DCPStationHandler() {
        @Override
        public void onStationDetected(DCPInfo info) {
            onDCPStationDetected(info);
        }
    };

    private final Runnable identifyTask = new Runnable() {
        @Override
        public void run() {
            Thread.currentThread().setName("PROFINET DCP identifier");
            runIdentify();
        }
    };

    private final IODataHandler ioDataHandler = new IODataHandler() {
        @Override
        public void onSend(IODataService service, long timestamp, Frame frame) {
            onIOSend(timestamp);
        }

        @Override
        public void onReceive(IODataService service, long timestamp, Frame frame) {
            onIOReceive(timestamp);
        }

        @Override
        public void onTimeout(IODataService service, long timestamp) {
            onIOTimeout(timestamp);
        }
    };

    private final AtomicReference<State> state = new AtomicReference<State>(
            State.STOPPED);
    private final AtomicBoolean started = new AtomicBoolean();

    private final String name;
    private final ControllerHandler handler;
    private final ExecutorService executor;

    /* Valid in STARTED */
    private boolean udp;
    private MACAddress localMACAddress;
    private PNRTTransport pnrt;
    private RPCClient cmClient;
    private RPCServer cmServer;
    private DCP dcp;
    private Map<SocketAddress, StationInfo> stationCache;

    /* Valid in CONNECTED */
    private UUID controllerCMObject;
    private InetSocketAddress deviceCMAddress;
    private UUID deviceCMObject;

    private UUID arUUID;
    private int sessionKey;

    private int writeSequenceCounter;

    private DeviceConfiguration deviceConfiguration;
    private boolean applicationReady;

    private MACAddress remotePNRTAddress;

    private int inputFrameID = -1;
    private int outputFrameID = -1;
    private Frame inputFrame;
    private Frame outputFrame;

    private IODataService ioDataService;
    private int localAlarmRef;
    private int remoteAlarmRef;
    private int alarmSequenceCounter = 0xFFFF;

    /**
     * Closes all resources and sets the members to {@literal null}.
     */
    private synchronized void cleanup() {
        udp = false;
        localMACAddress = null;

        if (pnrt != null) {
            pnrt.close();
            pnrt = null;
        }

        if (cmClient != null) {
            cmClient.close();
            cmClient = null;
        }

        if (cmServer != null) {
            cmServer.stop();
            cmServer = null;
        }

        if (dcp != null) {
            dcp.abortTransactions();
        }
        dcp = null;

        if (stationCache != null) {
            stationCache.clear();
        }
        stationCache = null;

        cleanupConnect();
    }

    private synchronized void cleanupConnect() {
        controllerCMObject = null;

        deviceCMAddress = null;
        deviceCMObject = null;

        arUUID = null;
        // TODO: Check session key handling
        // sessionKey += 1;

        writeSequenceCounter = 0;

        deviceConfiguration = null;

        remotePNRTAddress = null;

        inputFrame = null;
        outputFrame = null;

        inputFrameID = -1;
        outputFrameID = -1;

        if (ioDataService != null) {
            ioDataService.stop();
            ioDataService = null;
        }

        localAlarmRef = -1;
        remoteAlarmRef = -1;
        alarmSequenceCounter = 0xFFFF;
    }

    private void onPNRTClose(Exception e) {
        if (!isStopped()) {
            LOG.log(Level.WARNING, "PN-RT transport closed unexpectedly", e);
        }

        stop();
    }

    private void onPNRTFrame(SocketAddress source, int frameID, ByteBuffer data) {
        if (isStopped()) {
            return;
        }

        if (frameID == inputFrameID) {
            onInput(frameID, data);
        } else {
            switch (frameID) {
            // DCP Hello
            // DCP Get or Set
            // DCP Ident Request
            // DCP Ident Response
            case 0xFEFC:
            case 0xFEFD:
            case 0xFEFE:
            case 0xFEFF:
                onDCP(source, frameID, data);
                break;

            // Alarm (high prio)
            // Alarm (low prio)
            case 0xFC01:
            case 0xFE01:
                onAlarm(frameID, data);
                break;

            default:
                // Unknown ID. Ignore it
                break;
            }
        }
    }

    private void onInput(int frameID, ByteBuffer data) {
        if (!isConnected()) {
            return;
        }

        ioDataService.handleFrame(frameID, data);
    }

    private void onIOSend(long timestamp) {
        if (!isConnected()) {
            return;
        }

        handler.onIOOutput(this, timestamp, outputFrame);
    }

    private void onIOReceive(long timestamp) {
        if (!isConnected()) {
            return;
        }

        handler.onIOInput(this, timestamp, inputFrame);
    }

    private void onIOTimeout(long timestamp) {
        if (!isConnected()) {
            return;
        }

        // Disable timeout
        ioDataService.setTimeout(0);

        handler.onIOTimeout(this, timestamp);

        disconnect();
    }

    private void onAlarm(int frameID, ByteBuffer data) {
        if (!isConnected()) {
            return;
        }

        // TODO: Could be refactored in a class

        try {
            BufferAdapter buf = new BufferAdapter(data);
            buf.order(ByteOrder.BIG_ENDIAN);

            int dstEndpoint = buf.getUnsignedShort();
            int srcEndpoint = buf.getUnsignedShort();

            if (dstEndpoint != localAlarmRef || srcEndpoint != remoteAlarmRef) {
                // Not meant for us
                return;
            }

            int typeAndFlags = buf.getUnsignedShort();
            int sendSeqNum = buf.getUnsignedShort();
            int ackSeqNum = buf.getUnsignedShort();

            int varPartLen = buf.getUnsignedShort();
            if (buf.remaining() < varPartLen) {
                // Invalid alarm packet
                LOG.info("Received invalid alarm PDU (var part length)");
                return;
            }
            buf.limit(buf.position() + varPartLen);

            if ((typeAndFlags & 0x0F00) != 0x0100) {
                // Currently we only respond to Data PDUs. ACK PDUs are silently
                // discarded
                return;
            }

            // Send ACK to Data-PDU
            BufferAdapter ackPdu = new BufferAdapter(12);
            ackPdu.putUnsignedShort(srcEndpoint);
            ackPdu.putUnsignedShort(dstEndpoint);
            ackPdu.putUnsignedShort(0x1301);
            ackPdu.putUnsignedShort(ackSeqNum);
            ackPdu.putUnsignedShort(sendSeqNum);
            ackPdu.putUnsignedShort(0);
            ackPdu.flip();
            try {
                pnrt.send(remotePNRTAddress, frameID, ackPdu.buffer());
            } catch (IOException e) {
                // TODO: How to handle this

                LOG.log(Level.WARNING, "Couldn't send alarm ACK PDU", e);
            }

            final BufferAdapter alarmAck = new BufferAdapter(1500);
            alarmAck.putUnsignedShort(srcEndpoint);
            alarmAck.putUnsignedShort(dstEndpoint);
            alarmAck.putUnsignedShort(0x1111);
            alarmAck.putUnsignedShort(alarmSequenceCounter);
            alarmAck.putUnsignedShort(alarmSequenceCounter);
            alarmSequenceCounter += 1;
            alarmSequenceCounter &= 0xFFFF;
            // Size. Set after we added all blocks
            alarmAck.putUnsignedShort(0);

            final BlockBuilder abb = new BlockBuilder(alarmAck);

            BlockReader.readBlocks(data, new BlockHandler() {
                @Override
                public void onBlock(int type, int version, ByteBuffer content) {
                    if ((type != 0x0001) && (type != 0x0002)) {
                        // Unknown block
                        return;
                    }

                    AlarmNotification notification = new AlarmNotification(type);
                    notification.read(new BufferAdapter(content));

                    if (((notification.ChannelProperties >> 11) & 0x0003) == 1) {
                        // Alarm appears
                        handler.onAlarmAppears(Controller.this,
                                type == 0x0001 ? AlarmPriority.HIGH
                                        : AlarmPriority.LOW,
                                notification.SlotNumber,
                                notification.SubslotNumber,
                                notification.ChannelNumber,
                                notification.ChannelErrorType);
                    } else {
                        // Alarm disappears
                        handler.onAlarmDisappears(Controller.this,
                                type == 0x0001 ? AlarmPriority.HIGH
                                        : AlarmPriority.LOW,
                                notification.SlotNumber,
                                notification.SubslotNumber,
                                notification.ChannelNumber);
                    }

                    AlarmAck ack = new AlarmAck(type | 0x8000);
                    ack.AlarmType = notification.AlarmType;
                    ack.API = notification.API;
                    ack.SlotNumber = notification.SlotNumber;
                    ack.SubslotNumber = notification.SubslotNumber;
                    ack.AlarmSpecifier = notification.AlarmSpecifier;
                    ack.Status = 0;

                    abb.addBlock(ack.getType());
                    ack.write(alarmAck);
                }
            });

            abb.complete();
            int size = alarmAck.position();
            alarmAck.position(10);
            alarmAck.putUnsignedShort(size - 12);
            alarmAck.position(size);
            alarmAck.flip();

            try {
                pnrt.send(remotePNRTAddress, frameID, alarmAck.buffer());
            } catch (IOException e) {
                // TODO: How to handle this

                LOG.log(Level.WARNING, "Couldn't send alarm ACK", e);
            }

        } catch (BufferUnderflowException bue) {
            LOG.info("Received invalid alarm PDU");
        }
    }

    private void onDCP(SocketAddress source, int frameID, ByteBuffer data) {
        dcp.handleFrame(source, frameID, data);
    }

    /**
     * Called to handle an incoming RPC request.
     * 
     * @return 0 on success, otherwise a PNIO error code
     * @throws RPCReject
     *             if the RPC request is invalid altogether
     */
    private int onCMCall(SocketAddress address, UUID activity, UUID object,
            UUID interface_, int operation, ByteBuffer args, ByteBuffer result)
            throws RPCReject {

        if (isStopped()) {
            throw new RPCReject();
        }

        LOG.info("CM RPC request");

        if (!interface_.equals(PNIORPC.UUID_IO_ControllerInterface)) {
            throw new RPCReject(RPCReject.nca_unk_if);
        }

        switch (operation) {
        case PNIORPC.Control:
            return onCMControl(args, result);
        default:
            throw new RPCReject(RPCReject.nca_op_rng_error);
        }
    }

    private int onCMControl(ByteBuffer args, ByteBuffer result) {
        // TODO: Cleanup >>>

        final AtomicReference<IODBlockReq> reqRef = new AtomicReference<IODBlockReq>();

        BlockReader.readBlocks(args, new BlockHandler() {
            @Override
            public void onBlock(int type, int version, ByteBuffer content) {
                switch (type) {
                case IODBlockReq.BLOCK_TYPE_APPLICATION_READY:
                    IODBlockReq req = new IODBlockReq(type);
                    reqRef.set(req);
                    req.read(new BufferAdapter(content));
                    break;
                }
            }
        });

        IODBlockReq req = reqRef.get();
        if (req == null) {
            return PNIOError.CONTROL_RPC_REJECTED;
        }

        if (req.ControlCommand != IODBlockReq.CONTROL_COMMAND_APPLICATION_READY) {
            return PNIOError.CONTROL_RPC_REJECTED;
        }

        if (!req.ARUUID.equals(arUUID)) {
            LOG.info("Received ApplicationReady from wrong AR (got "
                    + req.ARUUID + ", expected " + arUUID + ")");
            return PNIOError.CONTROL_RPC_REJECTED;
        }

        if (req.SessionKey != sessionKey) {
            LOG.info("Received ApplicationReady from wrong session (got "
                    + req.SessionKey + ",expected " + sessionKey + ")");
            return PNIOError.CONTROL_RPC_REJECTED;
        }

        applicationReady = true;
        handler.onApplicationReady(this);

        IODBlockRes res = new IODBlockRes(
                IODBlockRes.BLOCK_TYPE_APPLICATION_READY);
        res.ARUUID = arUUID;
        res.SessionKey = sessionKey;
        res.ControlCommand = IODBlockReq.CONTROL_COMMAND_DONE;

        try {
            BufferAdapter buf = new BufferAdapter(result);
            BlockBuilder bb = new BlockBuilder(buf);

            bb.addBlock(res.getType());
            res.write(buf);
            bb.complete();
        } catch (BufferOverflowException boe) {
            // Ignore
        }

        result.flip();

        return 0;

        // TODO: <<<
    }

    private synchronized void cmCall(short operation, ByteBuffer args,
            ByteBuffer result) throws IOException {
        if (isStopped()) {
            throw new IllegalStateException();
        }

        try {
            cmClient.call(deviceCMAddress, deviceCMObject,
                    PNIORPC.UUID_IO_DeviceInterface, operation, args, result);
        } catch (PNIOError error) {
            throw error;
        } catch (ProfinetException e) {
            if (isConnected()) {
                disconnect();
            }
            throw e;
        } catch (IOException e) {
            stop();
            throw e;
        }
    }

    private void onDCPStationDetected(DCPInfo info) {
        long now = System.currentTimeMillis();

        StationInfo station = stationCache.get(info.getPNRTAddress());

        if (station != null) {
            // Update

            station.lastUpdate = now;
            if (!station.dcpInfo.equals(info)) {
                station.dcpInfo = info;

                handler.onStationChanged(this, info.getPNRTAddress(), info);
            }

        } else {
            // New station

            station = new StationInfo();
            station.lastUpdate = now;
            station.dcpInfo = info;

            stationCache.put(info.getPNRTAddress(), station);

            handler.onStationDetected(this, info.getPNRTAddress(), info);
        }
    }

    /**
     * Runs the DCP identify task and updates the cache
     */
    private void runIdentify() {
        while (!isStopped()) {
            try {
                dcp.findStations(stationHandler, 1000);
            } catch (IOException e) {
                if (!isStopped()) {
                    LOG.log(Level.WARNING,
                            "Failed to identify stations via DCP", e);

                    stop();
                }

                break;
            }

            // Remove stale entries
            synchronized (this) {
                if (isStopped()) {
                    break;
                }

                long deadline = System.currentTimeMillis() - 3000;

                Iterator<Map.Entry<SocketAddress, StationInfo>> s = stationCache
                        .entrySet().iterator();

                while (s.hasNext()) {
                    Map.Entry<SocketAddress, StationInfo> station = s.next();

                    if (station.getValue().lastUpdate < deadline) {
                        s.remove();

                        handler.onStationLost(this, station.getKey());
                    }
                }
            }
        }
    }

    public Controller(String name, ControllerHandler handler) {
        this(name, handler, null);
    }

    public Controller(String name, ControllerHandler handler,
            ExecutorService executor) {
        // TODO: Name regexp check
        if ((name == null) || name.isEmpty()) {
            throw new IllegalArgumentException("Invalid name");
        }

        if (handler == null) {
            throw new IllegalArgumentException("Invalid handler");
        }

        if (executor == null) {
            executor = Executors.newCachedThreadPool(new ThreadFactory() {
                @Override
                public Thread newThread(Runnable task) {
                    Thread thread = new Thread(task, "PROFINET task");
                    thread.setDaemon(true);
                    return thread;
                }
            });
        }

        this.name = name;
        this.handler = handler;
        this.executor = executor;
    }

    public String getName() {
        return name;
    }

    /**
     * Returns the currently active device configuration. This is the
     * configuration as returned from the device after <tt>connect</tt>
     * succeeded.
     * 
     * @return the device configuration or {@literal null}
     */
    public DeviceConfiguration getDeviceConfiguration() {
        return deviceConfiguration;
    }

    public boolean isStopped() {
        return !started.get();
    }

    public boolean isConnected() {
        State s = state.get();
        return started.get()
                && ((s == State.CONNECTED) || (s == State.RUNNING));
    }

    public boolean isRunning() {
        State s = state.get();
        return started.get() && (s == State.RUNNING);
    }

    /**
     * Starts the PN-RT transport, CM RPC and, if not using UDP, the DCP
     * identity cache.
     * 
     * @param cmAddress
     *            local CM address
     * @param udp
     *            if {@literal true} PN-RT will use UDP instead of raw Ethernet
     *            frames
     */
    public synchronized void start(InetSocketAddress cmAddress, boolean udp)
            throws IOException {
        if (!isStopped()) {
            throw new IllegalStateException();
        }

        try {
            this.udp = udp;

            byte[] mac = EthernetUtils
                    .getMACForInetAddress((Inet4Address) cmAddress.getAddress());
            if (mac == null) {
                throw new IOException("Couldn't find MAC address for "
                        + cmAddress.getAddress());
            }
            localMACAddress = new MACAddress(mac);

            SocketAddress localPNRTAddress;
            if (udp) {
                localPNRTAddress = new InetSocketAddress(
                        cmAddress.getAddress(), 0x8892);
            } else {
                localPNRTAddress = localMACAddress;
            }
            pnrt = PNRTTransport.open(localPNRTAddress, pnrtHandler);
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    Thread.currentThread().setName("PROFINET Receiver");
                    pnrt.run();
                }
            });

            cmClient = new RPCClient();
            cmServer = new RPCServer(executor) {
                @Override
                protected synchronized int onCall(SocketAddress address,
                        UUID activity, UUID object, UUID interface_,
                        int operation, ByteBuffer args, ByteBuffer result)
                        throws RPCReject {

                    return onCMCall(address, activity, object, interface_,
                            operation, args, result);
                }
            };
            cmServer.start(cmAddress);

            dcp = new DCP(pnrt);
            stationCache = new HashMap<SocketAddress, StationInfo>();

            if (!udp) {
                executor.submit(identifyTask);
            }

        } catch (IOException e) {
            cleanup();
            throw e;
        }

        state.set(State.STARTED);
        started.set(true);
    }

    /**
     * Stops all communication and transports.
     */
    public void stop() {
        // Only the call that really stops is synchronized
        if (!started.getAndSet(false)) {
            return;
        }

        pnrt.close();
        cmClient.close();
        cmServer.stop();

        synchronized (this) {
            cleanup();

            state.set(State.STOPPED);
        }
    }

    /**
     * Finds a station by its station name using DCP. Can only be used if this
     * controller has not been started in UDP mode. This method will block for a
     * short time.
     * 
     * @param name
     *            station name
     * @return the DCP information returned by the station with the given name
     *         or {@literal null}
     */
    public synchronized DCPInfo findStationByName(String name)
            throws IOException {
        if (isStopped()) {
            throw new IllegalStateException();
        }

        if (udp) {
            throw new IllegalStateException(
                    "DCP operations are not available in UDP mode");
        }

        return dcp.findStationByName(name);
    }

    /**
     * Copies all stations that have been found in the last 3 seconds.
     * 
     * @param stations
     *            list that'll receive the stations, in no particular order
     */
    public synchronized void getCachedStations(List<DCPInfo> stations) {
        if (isStopped()) {
            throw new IllegalStateException();
        }

        for (Map.Entry<SocketAddress, StationInfo> e : stationCache.entrySet()) {
            stations.add(e.getValue().dcpInfo);
        }
    }

    /**
     * Enables the blink signal on the given station.
     * 
     * @param address
     *            station address
     */
    public synchronized void signalStation(SocketAddress address)
            throws IOException {
        if (isStopped()) {
            throw new IllegalStateException();
        }

        if (udp) {
            throw new IllegalStateException(
                    "DCP operations are not available in UDP mode");
        }

        dcp.signalStation(address);
    }

    /**
     * Resets the name and network settings to factory defaults.
     * 
     * @param address
     *            station address
     */
    public synchronized void resetStationToFactoryDefaults(SocketAddress address)
            throws IOException {
        if (isStopped()) {
            throw new IllegalStateException();
        }

        if (udp) {
            throw new IllegalStateException(
                    "DCP operations are not available in UDP mode");
        }

        dcp.resetStationToFactoryDefaults(address);
    }

    /**
     * Sets the IP address of the station with the given DCP (PN-RT) address.
     * 
     * @param address
     *            the station DCP address
     * @param ip
     *            the IP to set
     * @param netmask
     *            the netmask to set
     * @param gateway
     *            the gateway to set
     * @param permanent
     *            if {@literal true}, the IP address will be stored permanently
     * 
     * @throws IOException
     *             if the station didn't respond
     */
    public synchronized void setStationIPAddress(SocketAddress address,
            InetAddress ip, InetAddress netmask, InetAddress gateway,
            boolean permanent) throws IOException {
        if (isStopped()) {
            throw new IllegalStateException();
        }

        if (udp) {
            throw new IllegalStateException(
                    "DCP operations are not available in UDP mode");
        }

        dcp.setStationIPAddress(address, ip, netmask, gateway, permanent);
    }

    /**
     * Resets the IP address of the station with the given DCP (PN-RT) address.
     * 
     * @param address
     *            the station DCP address
     * @param permanent
     *            if {@literal true}, the IP address will be stored permanently
     * 
     * @throws IOException
     *             if the station didn't respond
     */
    public synchronized void resetStationIPAddress(SocketAddress address,
            boolean permanent) throws IOException {
        if (isStopped()) {
            throw new IllegalStateException();
        }

        if (udp) {
            throw new IllegalStateException(
                    "DCP operations are not available in UDP mode");
        }

        dcp.resetStationIPAddress(address, permanent);
    }

    /**
     * Sets the station name of the device with the given DCP (PN-RT) address.
     * 
     * @param address
     *            the station DCP address
     * @param name
     *            the station name to set
     * @param permanent
     *            if {@literal true}, the name will be stored permanently
     * 
     * @throws IOException
     *             if the station didn't respond
     */
    public synchronized void setStationName(SocketAddress address, String name,
            boolean permanent) throws IOException {
        if (isStopped()) {
            throw new IllegalStateException();
        }

        if (udp) {
            throw new IllegalStateException(
                    "DCP operations are not available in UDP mode");
        }

        dcp.setStationName(address, name, permanent);
    }

    /**
     * Connects to a PROFINET device. The connection alone is not enough to
     * start I/O exchange, which is initiated by calling {@link #parameterEnd()}
     * and receiving {@code onApplicationReady} from the device.
     * 
     * @param address
     *            the device CM address. Usually the port is {@literal 0x8892}
     * @param outputFrameID
     *            the output frame ID to use for this connection. Must be unique
     *            for this session on the local MAC address
     * @param alarmRef
     *            the local alarm reference for this session. Must be unique for
     *            this session on the local MAC address
     * @param sendClock
     *            the send clock for I/O. Must a power of 2
     * @param config
     *            the device configuration to use
     * @throws IOException
     *             if connection fails
     */
    public synchronized void connect(InetSocketAddress address,
            int outputFrameID, int alarmRef, int sendClock,
            DeviceConfiguration config) throws IOException {

        if ((outputFrameID < 0) || (outputFrameID > 0xFFFF)) {
            throw new IllegalArgumentException("Invalid frame ID");
        }

        if ((alarmRef < 0) || (alarmRef > 0xFFFF)) {
            throw new IllegalArgumentException("Invalid alarm reference");
        }

        if (sendClock <= 0) {
            throw new IllegalArgumentException("Invalid send clock");
        }

        if (isStopped()) {
            throw new IllegalStateException(
                    "You must start the controller first");
        }

        if (isConnected()) {
            throw new IllegalStateException("Already connected");
        }

        // Choose an Id that is unlikely to be used by the vendor
        controllerCMObject = PNIORPC.UUID_IO_ObjectInstance((short) 1,
                (short) 0xFFFF, (short) config.getVendorId());

        deviceCMAddress = address;
        deviceCMObject = PNIORPC.UUID_IO_ObjectInstance((short) 1,
                (short) config.getDeviceId(), (short) config.getVendorId());

        arUUID = UUID.randomUUID();
        sessionKey += 1;

        deviceConfiguration = null;
        applicationReady = false;

        localAlarmRef = alarmRef;

        // TODO: Cleanup >>>

        try {
            BufferAdapter args = new BufferAdapter(1500);
            BufferAdapter result = new BufferAdapter(1500);

            ConnectReq req = new ConnectReq();

            req.ar.ARUUID = arUUID;
            req.ar.SessionKey = sessionKey;
            req.ar.CMInitiatorMacAdd.fromAddress(localMACAddress
                    .getMACAddress());
            req.ar.CMInitiatorObjectUUID = controllerCMObject;
            req.ar.CMInitiatorStationName = name;

            req.alarmCR.LocalAlarmReference = localAlarmRef;

            req.setIOCRs(config, outputFrameID, sendClock);
            req.setExpectedSubmodules(config);

            req.write(args);
            args.flip();

            cmCall(PNIORPC.Connect, args.buffer(), result.buffer());

            ConnectRes res = new ConnectRes();
            res.read(result);

            remoteAlarmRef = res.alarmCR.LocalAlarmReference;
            deviceConfiguration = res.getDeviceConfiguration(config);

            setupIO(sendClock, config, req, res);
        } catch (IOException e) {
            cleanupConnect();

            throw e;
        }
        // TODO: <<<

        state.set(State.CONNECTED);

        handler.onConnect(this);
    }

    /**
     * Disconnects from the device. This will only clear connection related
     * state on the controller side. The device should be released instead.
     */
    public synchronized void disconnect() {
        if (!isConnected()) {
            throw new IllegalStateException();
        }

        cleanupConnect();

        state.set(State.STARTED);

        handler.onDisconnect(this);
    }

    /**
     * Releases the connection from the device and stops I/O etc.
     */
    public synchronized void release() throws IOException {
        if (!isConnected()) {
            throw new IllegalStateException();
        }

        try {
            // TODO: Cleanup >>>

            BufferAdapter args = new BufferAdapter(1500);
            BufferAdapter result = new BufferAdapter(1500);

            IODBlockReq req = new IODBlockReq(IODBlockReq.BLOCK_TYPE_RELEASE);

            req.ARUUID = arUUID;
            req.SessionKey = sessionKey;
            req.ControlCommand = IODBlockReq.CONTROL_COMMAND_RELEASE;

            BlockBuilder bb = new BlockBuilder(args);
            bb.addBlock(req.getType());

            req.write(args);

            bb.complete();
            args.flip();

            cmCall(PNIORPC.Release, args.buffer(), result.buffer());

            // TODO: <<<

            disconnect();
        } catch (SocketTimeoutException ste) {
            disconnect();

            throw ste;
        } catch (ProfinetException ste) {
            disconnect();

            throw ste;
        } catch (IOException e) {
            stop();
            throw e;
        }
    }

    /**
     * Send a parameter end indication to the device. This is the final step the
     * controller takes to enable I/O exchange and must be called after the
     * parameters have been sent.
     * 
     * The device will answer with an application ready indication
     * 
     * @throws IOException
     *             if the request failed
     * 
     * @see {@link ControllerHandler#onApplicationReady(Controller)
     *      onApplicationReady}
     */
    public synchronized void parameterEnd() throws IOException {
        // parameter end is actually only valid once after connect, but who
        // cares :)
        if (!isConnected()) {
            throw new IllegalStateException();
        }

        // TODO: Cleanup >>>

        BufferAdapter args = new BufferAdapter(1500);
        BufferAdapter result = new BufferAdapter(1500);

        IODBlockReq req = new IODBlockReq(IODBlockReq.BLOCK_TYPE_PARAMETER_END);

        req.ARUUID = arUUID;
        req.SessionKey = sessionKey;
        req.ControlCommand = IODBlockReq.CONTROL_COMMAND_PARAMETER_END;

        BlockBuilder bb = new BlockBuilder(args);
        bb.addBlock(req.getType());

        req.write(args);

        bb.complete();
        args.flip();

        cmCall(PNIORPC.Control, args.buffer(), result.buffer());

        // TODO: Parse the result?

        // TODO: <<<
    }

    /**
     * Returns {@literal true} if the device has sent application ready.
     */
    public synchronized boolean isApplicationReady() throws IOException {
        if (!isConnected()) {
            throw new IllegalStateException();
        }
        
        return applicationReady;
    }
    
    /**
     * Writes a record to a given subslot. This can be used to write parameters
     * etc.
     * 
     * @param api
     *            the api, which is always 0
     * @param index
     *            the record index
     * @param data
     *            the record data
     * @throws IOException
     *             if writing failed
     */
    public void write(int api, int slot, int subslot, int index, ByteBuffer data)
            throws IOException {
        if (!isConnected()) {
            throw new IllegalStateException();
        }

        // TODO: cleanup >>>

        BufferAdapter args = new BufferAdapter(1500);
        BufferAdapter result = new BufferAdapter(1500);

        IODWriteReqHeader req = new IODWriteReqHeader();

        req.SeqNumber = writeSequenceCounter;
        req.ARUUID = arUUID;
        req.API = api;
        req.SlotNumber = slot;
        req.SubslotNumber = subslot;
        req.Index = index;
        req.RecordDataLength = data.remaining();

        BlockBuilder bb = new BlockBuilder(args);
        bb.addBlock(req.getType());

        req.write(args);

        bb.complete();

        args.put(data);
        args.flip();

        cmCall(PNIORPC.Write, args.buffer(), result.buffer());

        // TODO: Parse response
        // TODO: Increment only if good
        writeSequenceCounter += 1;

        // TODO: <<<
    }

    /**
     * Reads a record from a given subslot. This can be used to read parameters
     * etc.
     * 
     * @param api
     *            the api, which is always 0
     * @param index
     *            the record index
     * @param data
     *            the record data
     * 
     * @return the record length (which can be bigger than the returned amount)
     * @throws IOException
     *             if reading failed
     */
    public int read(int api, int slot, int subslot, int index, ByteBuffer data)
            throws IOException {
        if (!isConnected()) {
            throw new IllegalStateException();
        }

        BufferAdapter args = new BufferAdapter(1500);
        BufferAdapter result = new BufferAdapter(1500);

        IODReadReqHeader req = new IODReadReqHeader();

        req.SeqNumber = writeSequenceCounter;
        req.ARUUID = arUUID;
        req.API = api;
        req.SlotNumber = slot;
        req.SubslotNumber = subslot;
        req.Index = index;
        req.RecordDataLength = data.remaining();

        BlockBuilder bb = new BlockBuilder(args);
        bb.addBlock(req.getType());

        req.write(args);

        bb.complete();

        args.flip();

        cmCall(PNIORPC.Read, args.buffer(), result.buffer());

        // TODO: Parse response properly
        result.skip(36);
        int recordLength = result.getInt();

        result.skip(24);
        if (recordLength < result.remaining()) {
            result.limit(result.position() + recordLength);
        }
        if (result.remaining() > data.remaining()) {
            result.limit(result.position() + data.remaining());
        }

        data.put(result.buffer());

        // TODO: Increment only if good
        writeSequenceCounter += 1;

        return recordLength;
    }

    // -- I/O --

    /**
     * Starts the I/O transport for the connection.
     * 
     * @param config
     *            the device configuration used during the connection
     * @param req
     *            the connect request
     * @param res
     *            the connect response
     * @throws IOException
     *             if starting the transport fails
     */
    private void setupIO(int sendClock, DeviceConfiguration config,
            ConnectReq req, ConnectRes res) throws IOException {

        IOCRBlockReq inputCR = req.iocrs.get(0);
        IOCRBlockReq outputCR = req.iocrs.get(1);

        // IOCRBlockRes inputCRRes = res.iocrs.get(0);
        IOCRBlockRes outputCRRes = res.iocrs.get(1);

        inputFrame = Frame.createFrameFromIOCR(config, true, inputCR);
        inputFrameID = inputCR.FrameID;

        // TODO: Could check for matching input frame ID
        outputFrame = Frame.createFrameFromIOCR(config, false, outputCR);
        outputFrameID = outputCRRes.FrameID;
        outputFrame.setFrameID(outputFrameID);

        outputFrame.setPrimary(true);
        outputFrame.setDataValid(true);
        outputFrame.setProviderState(true);
        outputFrame.setProblemIndicator(false);

        // TODO: We use RT_CLASS_2 atm.
        remotePNRTAddress = new MACAddress(res.ar.CMResponderMacAdd.address);

        ioDataService = new IODataService(ioDataHandler, pnrt, sendClock,
                outputFrame, inputFrame, remotePNRTAddress);

        ioDataService.start(executor);
        ioDataService.setTimeout(Math.min(300, inputCR.WatchdogFactor
                * sendClock) * 1000000);
    }

    /**
     * Returns the input frame, which is used for I/O reception. Beware that
     * this frame should only be modified in the
     * {@link ControllerHandler#onIOInput(Controller, long, Frame) onIOInput}
     * handler.
     * 
     * @return the input frame
     */
    public Frame getInputFrame() {
        return inputFrame;
    }

    /**
     * Returns the output frame, which is used for I/O sending. Beware that this
     * frame should only be modified in the
     * {@link ControllerHandler#onIOOutput(Controller, long, Frame) onIOOutput}
     * handler.
     * 
     * @return the output frame
     */
    public Frame getOutputFrame() {
        return outputFrame;
    }
}
