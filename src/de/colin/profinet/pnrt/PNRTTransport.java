/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.pnrt;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * PN-RT transport class, that enables you to send and receive PN-RT frames.
 * 
 * <p>
 * The transport handler will be called in the thread that executes the
 * {@link #run() receiver loop}.
 * 
 * @author Colin Leitner
 */
public abstract class PNRTTransport implements Closeable, Runnable {

    /**
     * Opens a new PN-RT transport. Depending on the type of local address,
     * you'll either receive an Ethernet or UDP transport.
     * 
     * @param localAddress
     *            local PN-RT address
     * @param handler
     *            handler for incoming frames
     * @return a new transport instance
     * @throws IOException
     *             if creation of the transport failed
     */
    public static PNRTTransport open(SocketAddress localAddress,
            PNRTHandler handler) throws IOException {

        if (localAddress instanceof InetSocketAddress) {
            return new UDPPNRTTransport((InetSocketAddress) localAddress,
                    handler);
        } else if (localAddress instanceof MACAddress) {
            return new EthernetPNRTTransport((MACAddress) localAddress, handler);
        } else {
            throw new IllegalArgumentException();
        }
    }

    private final AtomicBoolean closed = new AtomicBoolean();

    private final SocketAddress localAddress;
    private final PNRTHandler handler;

    PNRTTransport(SocketAddress localAddress, PNRTHandler handler) {
        if (localAddress == null) {
            throw new NullPointerException();
        }

        this.localAddress = localAddress;
        this.handler = handler;
    }

    protected final void close(Exception e) {
        if (closed.getAndSet(true)) {
            return;
        }

        implClose();

        if (handler != null) {
            handler.onClose(this, e);
        }
    }

    protected abstract void implClose();

    public final PNRTHandler getHandler() {
        return handler;
    }

    // Not final to allow the subclasses to change the return type
    public SocketAddress getLocalAddress() {
        return localAddress;
    }

    public final boolean isOpen() {
        return !closed.get();
    }

    @Override
    public final void close() {
        close(null);
    }

    /**
     * Runs the receiver loop. Returns as soon as the transport is closed or if
     * no handler has been specified.
     * 
     * @see {@link #close()}
     */
    @Override
    public abstract void run();

    /**
     * Sends a frame to the given destination address.
     * 
     * @param destination
     *            destination address
     * @param frameID
     *            PN-RT frame ID
     * @param buf
     *            the frame data
     * @throws IOException
     *             if sending failed for some reason
     */
    public abstract void send(SocketAddress destination, int frameID,
            ByteBuffer buf) throws IOException;

}
