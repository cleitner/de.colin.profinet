package de.colin.profinet.pnrt;

public interface LLDPListener {

    void onStationDiscovered(LLDPCache cache, LLDPInfo info);

    void onStationTimeout(LLDPCache cache, LLDPInfo info);

}
