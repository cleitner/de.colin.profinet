/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.pnrt;

import java.net.SocketAddress;
import java.nio.ByteBuffer;

/**
 * Handler for PN-RT frames received by {@code PNRTTransport}.
 * 
 * @author Colin Leitner
 */
public interface PNRTHandler {

    /**
     * Called on reception of a PN-RT frame.
     * 
     * @param transport
     *            the transport that received the frame
     * @param source
     *            the source address of the frame
     * @param frameID
     *            the frame ID
     * @param data
     *            the frame content excluding the frame ID
     */
    void onFrame(PNRTTransport transport, SocketAddress source, int frameID,
            ByteBuffer data);

    /**
     * Called if the transport has been closed. If the transport has been closed
     * by calling {@code close()}, the exception will be {@literal null}.
     * 
     * @param transport
     *            the closed transport
     * @param e
     *            the exception that caused the transport to close
     */
    void onClose(PNRTTransport transport, Exception e);

}
