package de.colin.profinet.pnrt;

public class LLDPInfo {

    private final String chassisId;
    private final String portId;

    public LLDPInfo(String chassisId, String portId) {
        this.chassisId = chassisId;
        this.portId = portId;
    }

    public String getChassisId() {
        return chassisId;
    }

    public String getPortId() {
        return portId;
    }

    @Override
    public String toString() {
        return "LLDPInfo [chassisId=" + chassisId + ", portId=" + portId + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((chassisId == null) ? 0 : chassisId.hashCode());
        result = prime * result + ((portId == null) ? 0 : portId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LLDPInfo other = (LLDPInfo) obj;
        if (chassisId == null) {
            if (other.chassisId != null)
                return false;
        } else if (!chassisId.equals(other.chassisId))
            return false;
        if (portId == null) {
            if (other.portId != null)
                return false;
        } else if (!portId.equals(other.portId))
            return false;
        return true;
    }

}
