package de.colin.profinet.pnrt;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.atomic.AtomicBoolean;

import de.colin.ipc.IPCProcess;
import de.colin.profinet.BufferAdapter;

public class LLDPCache {

    private static final Charset UCS = Charset.forName("UTF-8");

    private class LLDPReceiver extends IPCProcess {
        private static final int IPC_ID_RECEIVE = 2;

        private final String device;

        public LLDPReceiver(String device) {
            this.device = device;
        }

        @Override
        protected Process createProcess() throws IOException {
            return EthernetUtils.spawnTransportHelper("rlldp", device);
        }

        @Override
        protected void onReceive(int id, ByteBuffer buf) {
            if (id == IPC_ID_RECEIVE) {
                LLDPCache.this.onReceive(buf);
            } else {
                super.onReceive(id, buf);
            }
        }

        @Override
        protected void onError(IOException e) {
            // Ignore the dying process
        }
    }

    private static class ReceivedInfo {
        long timeout;
        LLDPInfo info;

        public ReceivedInfo(LLDPInfo info, int ttl) {
            this.info = info;
            this.timeout = System.nanoTime() + ttl * 1000000000L;
        }
    }

    private final AtomicBoolean running = new AtomicBoolean();

    private LLDPListener listener;

    private final List<LLDPReceiver> receivers = new ArrayList<LLDPReceiver>();
    private final List<ReceivedInfo> info = new ArrayList<ReceivedInfo>();

    private Thread timeoutThread;

    public LLDPCache() {
    }

    private void cleanup() {
        if (!running.getAndSet(false)) {
            return;
        }

        for (LLDPReceiver r : receivers) {
            r.close();
        }

        synchronized (this) {
            receivers.clear();
            info.clear();
        }

        if (timeoutThread != null) {
            timeoutThread.interrupt();
            timeoutThread = null;
        }
    }

    private void onReceive(ByteBuffer packet) {
        if (!running.get()) {
            return;
        }

        int offset = 0;

        // Strip VLAN header if present
        if (packet.remaining() >= 18) {
            if (((packet.get(12) & 0xFF) == 0x81)
                    && ((packet.get(13) & 0xFF) == 0x00)) {
                offset += 4;
            }
        }

        packet.position(14 + offset);

        ReceivedInfo info = parseLLDP(packet);
        if (info == null) {
            return;
        }

        synchronized (this) {
            if (!running.get()) {
                return;
            }

            boolean found = false;
            for (ReceivedInfo i : this.info) {
                if (i.info.equals(info.info)) {
                    if (i.timeout < info.timeout) {
                        i.timeout = info.timeout;
                    }

                    found = true;
                    break;
                }
            }

            if (!found) {
                this.info.add(info);

                if (listener != null) {
                    listener.onStationDiscovered(this, info.info);
                }
            }
        }
    }

    private void timeoutTask() {
        while (running.get()) {
            try {
                Thread.sleep(997);
            } catch (InterruptedException e) {
                break;
            }

            long now = System.nanoTime();

            synchronized (this) {
                ListIterator<ReceivedInfo> i = info.listIterator();
                while (i.hasNext()) {
                    ReceivedInfo ri = i.next();

                    if (ri.timeout < now) {
                        i.remove();

                        if (running.get() && (listener != null)) {
                            listener.onStationTimeout(this, ri.info);
                        }
                    }
                }
            }
        }
    }

    private ReceivedInfo parseLLDP(ByteBuffer buf) {
        String chassisId = null;
        String portId = null;
        int ttl = -1;

        BufferAdapter b = new BufferAdapter(buf);

        try {
            while (true) {
                int tlv = b.getUnsignedShort();
                if (tlv == 0) {
                    break;
                }

                int type = tlv >> 9;
                int len = tlv & 0x1FF;

                switch (type) {
                case 1: {
                    // Chassis Id

                    if (len < 2) {
                        // Invalid TLV
                        return null;
                    }

                    if (chassisId != null) {
                        // Duplicated chassis id?
                        return null;
                    }

                    byte idType = b.get();
                    if (idType == 4) {
                        // MAC
                        if (len != 7) {
                            // Invalid length
                            return null;
                        }

                        byte[] mac = new byte[6];
                        b.get(mac);

                        // TODO: Check what PN equipment actually uses
                        // as unassigned names
                        chassisId = String.format(
                                "mac-%02x-%02x-%02x-%02x-%02x-%02x",
                                mac[0] & 0xFF, mac[1] & 0xFF, mac[2] & 0xFF,
                                mac[3] & 0xFF, mac[4] & 0xFF, mac[5] & 0xFF);

                    } else if (idType == 7) {
                        // Locally assigned

                        if (len == 1) {
                            // Invalid length
                            return null;
                        }

                        int limit = b.limit();
                        try {
                            b.limit(b.position() + len - 1);
                            chassisId = UCS.decode(b.buffer()).toString();
                        } finally {
                            b.limit(limit);
                        }

                    } else {
                        // Unsupported chassis Id type
                        return null;
                    }

                    break;
                }

                case 2: {
                    // Port Id
                    if (len < 2) {
                        // Invalid TLV
                        return null;
                    }

                    if (portId != null) {
                        // Duplicated port id?
                        return null;
                    }

                    byte idType = b.get();
                    if (idType == 7) {
                        // Locally assigned

                        if (len == 1) {
                            // Invalid length
                            return null;
                        }

                        int limit = b.limit();
                        try {
                            b.limit(b.position() + len - 1);
                            portId = UCS.decode(b.buffer()).toString();
                        } finally {
                            b.limit(limit);
                        }
                    } else {
                        // Unsupported port Id type
                        return null;
                    }

                    break;
                }

                case 3: {
                    // TTL

                    if (len != 2) {
                        // Invalid TLV
                        return null;
                    }

                    if (ttl != -1) {
                        // Duplicated TTL?
                        return null;
                    }

                    ttl = b.getUnsignedShort();

                    break;
                }

                default:
                    if (len > b.remaining()) {
                        // Invalid length
                        return null;
                    }

                    b.skip(len);
                    break;
                }
            }

        } catch (BufferUnderflowException bue) {
            // Ignore invalid packet
        }

        if ((ttl == -1) || (chassisId == null) || (portId == null)) {
            return null;
        }

        return new ReceivedInfo(new LLDPInfo(chassisId, portId), ttl);
    }

    public synchronized void setListener(LLDPListener listener) {
        this.listener = listener;
    }

    public synchronized List<LLDPInfo> getStations() {
        List<LLDPInfo> copy = new ArrayList<LLDPInfo>(info.size());

        for (ReceivedInfo i : info) {
            copy.add(i.info);
        }

        return copy;
    }

    public synchronized void start() throws IOException {
        try {
            if (running.getAndSet(true)) {
                return;
            }

            String[] devices = EthernetUtils.getCaptureDevices();
            for (String device : devices) {
                LLDPReceiver receiver = new LLDPReceiver(device);
                receivers.add(receiver);
                receiver.startAndRun();
            }
        } catch (IOException e) {
            cleanup();

            throw e;
        }

        timeoutThread = new Thread() {
            @Override
            public void run() {
                timeoutTask();
            }
        };
        timeoutThread.setName("lldp-timeout");
        timeoutThread.setDaemon(true);
        timeoutThread.start();
    }

    public synchronized void stop() {
        cleanup();
    }
}
