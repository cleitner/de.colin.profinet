/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.pnrt;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import de.colin.profinet.BufferAdapter;

/**
 * DCP communication class.
 * 
 * @author Colin Leitner
 */
public class DCP {

    public static final MACAddress DCP_MULTICAST_ADDRESS = new MACAddress(
            new byte[] { 0x01, 0x0E, (byte) 0xCF, 0x00, 0x00, 0x00 });

    public static final int DCP_HELLO = 0xFEFC;
    public static final int DCP_GET_SET = 0xFEFD;
    public static final int DCP_IDENT_REQ = 0xFEFE;
    public static final int DCP_IDENT_RSP = 0xFEFF;

    private static final Charset ASCII = Charset.forName("US-ASCII");
    // Default timeout for blocking calls
    private static final int TIMEOUT = 150;

    private static final Throwable ABORT = new Exception();

    public static interface TransactionHandler {
        void onResponse(DCPResponse response);
    }

    public static class Transaction extends FutureTask<DCPResponse> {
        private final boolean singleResponse;
        private final int xid;
        private final TransactionHandler handler;

        Transaction(boolean single, int xid, TransactionHandler handler) {
            super(new Callable<DCPResponse>() {
                @Override
                public DCPResponse call() throws Exception {
                    return null;
                }
            });
            this.singleResponse = single;
            this.xid = xid;
            this.handler = handler;
        }

        boolean isSingle() {
            return singleResponse;
        }

        void complete(DCPResponse response) {
            set(response);
            if (handler != null) {
                handler.onResponse(response);
            }
        }

        void handle(DCPResponse response) {
            if (handler != null) {
                handler.onResponse(response);
            }
        }

        void abort() {
            setException(ABORT);
        }

        public int getXid() {
            return xid;
        }
    }

    private final AtomicInteger transactionID = new AtomicInteger(
            (int) (Math.random() * Integer.MAX_VALUE));

    private final PNRTTransport transport;
    private final Map<Integer, Transaction> transactions;

    public DCP(PNRTTransport transport) {
        if (transport == null) {
            throw new NullPointerException();
        }

        this.transport = transport;
        transactions = new HashMap<Integer, Transaction>();
    }

    public static boolean isDCPFrameID(int frameID) {
        return frameID == DCP_HELLO || frameID == DCP_GET_SET
                || frameID == DCP_IDENT_REQ || frameID == DCP_IDENT_RSP;
    }

    public void handleFrame(SocketAddress source, int frameID, ByteBuffer data) {
        switch (frameID) {
        // DCP Hello
        case DCP_HELLO:
            // Ignore
            return;

            // DCP Get or Set
        case DCP_GET_SET:
            break;

        // DCP Ident Request
        case DCP_IDENT_REQ:
            break;

        // DCP Ident Response
        case DCP_IDENT_RSP:
            break;

        default:
            // Ignore
            return;
        }

        if (data.remaining() < 10) {
            return;
        }

        data.order(ByteOrder.BIG_ENDIAN);

        int serviceID = data.get() & 0xFF;
        int serviceType = data.get() & 0xFF;
        int xid = data.getInt();
        int responseDelay = data.getShort() & 0xFFFF;
        int dataLen = data.getShort() & 0xFFFF;

        if (dataLen > data.remaining()) {
            // Invalid length
            return;
        }

        Transaction transaction;
        synchronized (this) {
            transaction = transactions.get(xid);

            if (transaction == null) {
                // Nothing to do
                return;
            }

            if (transaction.isSingle()) {
                transaction = transactions.remove(xid);
            }
        }

        byte[] dcpData = new byte[dataLen];
        data.get(dcpData);

        DCPResponse response = new DCPResponse(source, serviceID, serviceType,
                responseDelay, dcpData);

        if (transaction.isSingle()) {
            transaction.complete(response);
        } else {
            transaction.handle(response);
        }
    }

    public synchronized void abortTransactions() {
        for (Map.Entry<Integer, Transaction> t : transactions.entrySet()) {
            t.getValue().abort();
        }
        transactions.clear();
    }

    /**
     * Sends a DCP PDU. This creates a new transaction which is either completed
     * by a response (single response only) or a timeout.
     * 
     * If you expect multiple answers, the transaction will only complete with a
     * timeout.
     */
    public synchronized Transaction send(SocketAddress destination,
            int frameID, int serviceID, int serviceType, int responseDelay,
            ByteBuffer data, TransactionHandler handler, boolean singleResponse)
            throws IOException {

        int xid = transactionID.incrementAndGet();

        Transaction transaction = new Transaction(singleResponse, xid, handler);
        // We have to add the transaction before sending the request,
        // because if we receive the response fast enough, the handler would
        // discard the unknown transaction. An exception should remove the
        // transaction to prevent memory leaks.
        transactions.put(xid, transaction);

        try {
            ByteBuffer buf = ByteBuffer.allocate(10 + data.remaining());
            buf.order(ByteOrder.BIG_ENDIAN);

            buf.put((byte) serviceID);
            buf.put((byte) serviceType);
            buf.putInt(xid);
            buf.putShort((short) responseDelay);
            buf.putShort((short) data.remaining());
            buf.put(data);
            buf.flip();

            transport.send(destination, frameID, buf);
        } catch (IOException e) {
            transactions.remove(xid);
            throw e;
        } catch (RuntimeException e) {
            transactions.remove(xid);
            throw e;
        }

        return transaction;
    }

    /**
     * Sends a DCP PDU and waits for the response.
     */
    public DCPResponse sendAndWait(SocketAddress destination, int frameID,
            int serviceID, int serviceType, int responseDelay, ByteBuffer data,
            TransactionHandler handler, boolean singleResponse, int timeout)
            throws IOException {

        Transaction transaction = send(destination, frameID, serviceID,
                serviceType, responseDelay, data, handler, singleResponse);

        try {
            return transaction.get(timeout, TimeUnit.MILLISECONDS);
        } catch (TimeoutException te) {
            throw new SocketTimeoutException("DCP request timed out");
        } catch (InterruptedException e) {
            throw new IOException(e);
        } catch (ExecutionException e) {
            if (e.getCause() == ABORT) {
                throw new IOException("The DCP transction has been aborted");
            } else {
                throw new AssertionError();
            }
        } finally {
            synchronized (this) {
                transactions.remove(transaction.getXid());
            }
        }
    }

    private DCPInfo parseIdentifyResponse(SocketAddress source,
            byte[] responseData) throws IOException {
        int vendorID = -1;
        int deviceID = -1;

        String nameOfStation = null;
        String typeOfStation = null;
        StationRole role = null;
        InetAddress ipAddress = null;
        InetAddress netmask = null;
        InetAddress gateway = null;

        BufferAdapter data = new BufferAdapter(ByteBuffer.wrap(responseData));

        try {
            while (data.hasRemaining()) {
                int option = data.getUnsignedShort();
                int blockLen = data.getUnsignedShort() - 2;
                int blockInfo = data.getUnsignedShort();

                if (blockLen < 0) {
                    throw new IOException("Invalid DCP response (block length)");
                }

                switch (option) {
                case 0x0201:
                    // TypeOfStation
                    typeOfStation = data.getString(blockLen);
                    break;

                case 0x0202:
                    // NameOfStation
                    nameOfStation = data.getString(blockLen);
                    break;

                case 0x0203:
                    // Device ID
                    vendorID = data.getUnsignedShort();
                    deviceID = data.getAlignedUnsignedShort();
                    break;

                case 0x0204:
                    // Role
                    int roleValue = data.get() & 0xFF;
                    data.get();

                    switch (roleValue) {
                    case 0x01:
                        role = StationRole.DEVICE;
                        break;

                    case 0x02:
                        role = StationRole.CONTROLLER;
                        break;

                    default:
                        // Unknown
                        role = null;
                        break;
                    }

                    break;

                case 0x0102:
                    if ((blockInfo != 0x0001) && (blockInfo != 0x0002)) {
                        data.skip(blockLen);
                        break;
                    }

                    int ip = data.getInt();
                    int nm = data.getInt();
                    int gw = data.getInt();

                    ipAddress = convertIP(ip);
                    netmask = convertIP(nm);
                    gateway = convertIP(gw);

                    break;

                default:
                    data.skip(blockLen);
                    break;
                }

                data.align(2);
            }
        } catch (BufferUnderflowException bue) {
            throw new IOException("Invalid DCP response", bue);
        }

        return new DCPInfo(source, vendorID, deviceID, nameOfStation,
                typeOfStation, role, ipAddress, netmask, gateway);
    }

    /**
     * Sends an identify request with the given timeout and calls the handler
     * for every detected station.
     * 
     * @param handler
     * @param timeout
     */
    public void findStations(final DCPStationHandler handler, int timeout)
            throws IOException {

        // +1 because it could be an odd name length
        BufferAdapter identReq = new BufferAdapter(4);

        // All/All
        identReq.putUnsignedShort(0xFFFF);
        // Length
        identReq.putShort((short) 0);
        identReq.flip();

        try {
            sendAndWait(DCP_MULTICAST_ADDRESS, DCP_IDENT_REQ, 0x05, 0x00, 1,
                    identReq.buffer(), new TransactionHandler() {

                        @Override
                        public void onResponse(DCPResponse response) {
                            if (response.getServiceID() != 0x05
                                    || response.getServiceType() != 0x01) {
                                // Invalid DCP ident response
                                return;
                            }

                            DCPInfo info;
                            try {
                                info = parseIdentifyResponse(
                                        response.getSource(),
                                        response.getData());
                            } catch (IOException e) {
                                // Failed, but we'll have to cope with that
                                return;
                            }
                            handler.onStationDetected(info);
                        }
                    }, false, timeout);
        } catch (SocketTimeoutException ste) {
            // All is well
        }
    }

    /**
     * Find a station by name and returns the DCP information returned in the
     * identify response.
     * 
     * @param nameOfStation
     * @return
     * @throws IOException
     */
    public DCPInfo findStationByName(String nameOfStation) throws IOException {

        // +1 because it could be an odd name length
        BufferAdapter identReq = new BufferAdapter(
                4 + nameOfStation.length() + 1);

        // Device/Name of Station
        identReq.putUnsignedShort(0x0202);
        // Length
        identReq.putUnsignedShort(nameOfStation.length());
        identReq.put(ASCII.encode(nameOfStation));
        identReq.align(2);
        identReq.flip();

        DCPResponse identRsp;

        try {
            identRsp = sendAndWait(DCP_MULTICAST_ADDRESS, DCP_IDENT_REQ, 0x05,
                    0x00, 1, identReq.buffer(), null, true, TIMEOUT);
        } catch (SocketTimeoutException ste) {
            return null;
        }

        if (identRsp.getServiceID() != 0x05
                || identRsp.getServiceType() != 0x01) {
            throw new IOException("Invalid DCP ident response");
        }

        return parseIdentifyResponse(identRsp.getSource(), identRsp.getData());
    }

    private InetAddress convertIP(int ip) throws IOException {
        return InetAddress.getByAddress(new byte[] { (byte) (ip >> 24 & 0xFF),
                (byte) (ip >> 16 & 0xFF), (byte) (ip >> 8 & 0xFF),
                (byte) (ip & 0xFF) });
    }

    private int convertIP(InetAddress address) throws IOException {
        if (!(address instanceof Inet4Address)) {
            throw new IllegalArgumentException(
                    "Can only set IPv4 address via DCP");
        }

        Inet4Address ipv4 = (Inet4Address) address;
        byte[] a = ipv4.getAddress();

        return ((a[0] & 0xFF) << 24) | ((a[1] & 0xFF) << 16)
                | ((a[2] & 0xFF) << 8) | (a[3] & 0xFF);
    }

    public void setStationIPAddress(SocketAddress address, InetAddress ip,
            InetAddress netmask, InetAddress gateway, boolean permanent)
            throws IOException {

        BufferAdapter setReq = new BufferAdapter(6 + 12);

        // IP/IP
        setReq.putUnsignedShort(0x0102);
        // Length
        setReq.putUnsignedShort(2 + 12);
        // Qualifier
        setReq.putUnsignedShort(permanent ? 0x0001 : 0x0000);

        setReq.putInt(ip != null ? convertIP(ip) : 0);
        setReq.putInt(netmask != null ? convertIP(netmask) : 0);
        setReq.putInt(gateway != null ? convertIP(gateway) : 0);

        setReq.flip();

        DCPResponse setRsp = sendAndWait(address, DCP_GET_SET, 0x04, 0x00, 1,
                setReq.buffer(), null, true, TIMEOUT);

        int status = checkSetResponse(setRsp, 0x0102);
        if (status != 0) {
            throw new IOException(String.format(
                    "Couldn't set station IP address (code 0x%02X)", status));
        }
    }

    public void resetStationIPAddress(SocketAddress address, boolean permanent)
            throws IOException {

        BufferAdapter setReq = new BufferAdapter(6 + 12);

        // IP/IP
        setReq.putUnsignedShort(0x0102);
        // Length
        setReq.putUnsignedShort(2 + 12);
        // Qualifier
        setReq.putUnsignedShort(permanent ? 0x0001 : 0x0000);

        setReq.putInt(0);
        setReq.putInt(0);
        setReq.putInt(0);

        setReq.flip();

        DCPResponse setRsp = sendAndWait(address, DCP_GET_SET, 0x04, 0x00, 1,
                setReq.buffer(), null, true, TIMEOUT);

        int status = checkSetResponse(setRsp, 0x0102);
        if (status != 0) {
            throw new IOException(String.format(
                    "Couldn't reset station IP address (code 0x%02X)", status));
        }
    }

    public void setStationName(SocketAddress address, String name,
            boolean permanent) throws IOException {

        // +1 because it could be an odd name length
        BufferAdapter setReq = new BufferAdapter(6 + name.length() + 1);

        // Device/Name of Station
        setReq.putUnsignedShort(0x0202);
        // Length
        setReq.putUnsignedShort(2 + name.length());
        // Qualifier
        setReq.putUnsignedShort(permanent ? 0x0001 : 0x0000);
        setReq.put(ASCII.encode(name));
        setReq.align(2);
        setReq.flip();

        DCPResponse setRsp = sendAndWait(address, DCP_GET_SET, 0x04, 0x00, 1,
                setReq.buffer(), null, true, TIMEOUT);

        int status = checkSetResponse(setRsp, 0x0202);
        if (status != 0) {
            throw new IOException(String.format(
                    "Couldn't set station name (code 0x%02X)", status));
        }
    }

    public void signalStation(SocketAddress address) throws IOException {
        BufferAdapter setReq = new BufferAdapter(6 + 2);

        // Control/Signal station
        setReq.putUnsignedShort(0x0503);
        // Length
        setReq.putUnsignedShort(2 + 2);
        // Qualifier
        setReq.putUnsignedShort(0);
        setReq.putUnsignedShort(0x0100);
        setReq.flip();

        DCPResponse setRsp = sendAndWait(address, DCP_GET_SET, 0x04, 0x00, 1,
                setReq.buffer(), null, true, TIMEOUT);

        int status = checkSetResponse(setRsp, 0x0503);
        if (status != 0) {
            throw new IOException(String.format(
                    "Couldn't set signal (code 0x%02X)", status));
        }
    }

    public void resetStationToFactoryDefaults(SocketAddress address)
            throws IOException {

        BufferAdapter setReq = new BufferAdapter(6);

        // Control/Factory reset
        setReq.putUnsignedShort(0x0505);
        // Length
        setReq.putUnsignedShort(2);
        // Qualifier
        setReq.putUnsignedShort(0);
        setReq.flip();

        DCPResponse setRsp = sendAndWait(address, DCP_GET_SET, 0x04, 0x00, 1,
                setReq.buffer(), null, true, TIMEOUT);

        int status = checkSetResponse(setRsp, 0x0505);
        if (status != 0) {
            throw new IOException(String.format(
                    "Couldn't reset to factory defaults (code 0x%02X)", status));
        }
    }

    private int checkSetResponse(DCPResponse rsp, int option)
            throws IOException {
        if ((rsp.getServiceID() != 0x04) || (rsp.getServiceType() != 0x01)) {
            throw new IOException("Invalid DCP set response");
        }

        BufferAdapter data = new BufferAdapter(rsp.getData());

        try {

            if (data.getUnsignedShort() != 0x0504) {
                throw new IOException("Invalid DCP set response (option)");
            }

            if (data.getUnsignedShort() != 0x0003) {
                throw new IOException("Invalid DCP set response (block length)");
            }

            if (data.getUnsignedShort() != option) {
                throw new IOException("Invalid DCP set response (set option)");
            }

            return data.get() & 0xFF;

        } catch (BufferUnderflowException bue) {
            throw new IOException("Invalid DCP response", bue);
        }
    }
}
