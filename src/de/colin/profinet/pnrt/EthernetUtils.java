/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.pnrt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utilities for Ethernet related tasks. PN-RT either uses Ethernet type 0x8892
 * or UDP on port 0x8892.
 * 
 * <p>
 * Java has no builtin support for raw Ethernet and running a VM with JNI as
 * root is usually not a good idea either (if it's even possible). Therefore we
 * start a platform specific transport helper, that uses libpcap to handle raw
 * capture and packet sending.
 * 
 * <p>
 * This transport helper has to be setuid root and the path provided in the
 * system property {@code de.colin.profinet.pnrt.transport-helper}. Per default
 * the helper is called "pnrt-transport" and if it's available in {@code $PATH},
 * the code will just work.
 * 
 * <p>
 * To support android integration you'll have to execute the transport helper
 * via {@code su}, which can be controlled by
 * {@code de.colin.profinet.pnrt.useSU} property. Beware that setting this
 * property makes the code prone to shell-code injection if the
 * {@code de.colin.profinet.pnrt.transport-helper} property can be altered by
 * foreign code.
 * 
 * @author Colin Leitner
 */
public class EthernetUtils {

    private static final Logger LOG = Logger.getLogger(EthernetUtils.class
            .getName());

    private static final Pattern MAC_PATTERN = Pattern
            .compile("(\\p{XDigit}{2})[:\\-](\\p{XDigit}{2})[:\\-](\\p{XDigit}{2})[:\\-](\\p{XDigit}{2})[:\\-](\\p{XDigit}{2})[:\\-](\\p{XDigit}{2})");
    private static final Pattern IPV4_PATTERN = Pattern
            .compile("([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)");

    private EthernetUtils() {
    }

    public static String toString(byte[] mac) {
        assert mac.length == 6;

        return String.format("%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1],
                mac[2], mac[3], mac[4], mac[5]);
    }

    public static String toString(SocketAddress address) {
        if (address instanceof MACAddress) {
            MACAddress mac = (MACAddress) address;
            return toString(mac.getMACAddress());
        } else {
            return address.toString();
        }
    }

    public static String toString(InetAddress address) {
        if (address instanceof Inet4Address) {
            Inet4Address ipv4 = (Inet4Address) address;
            byte[] ip = ipv4.getAddress();
            return String.format("%d.%d.%d.%d", ip[0] & 0xFF, ip[1] & 0xFF,
                    ip[2] & 0xFF, ip[3] & 0xFF);
        } else {
            return address.toString();
        }
    }

    public static byte[] fromString(String mac) {

        Matcher m = MAC_PATTERN.matcher(mac.trim());
        if (!m.matches()) {
            throw new IllegalArgumentException("Invalid MAC address");
        }

        byte[] bytes = new byte[6];
        for (int n = 0; n < 6; n++) {
            try {
                bytes[n] = (byte) Integer.parseInt(m.group(n + 1), 16);
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException("Invalid MAC address", nfe);
            }
        }

        return bytes;
    }

    public static Inet4Address ipFromString(String ipv4) {

        Matcher m = IPV4_PATTERN.matcher(ipv4.trim());
        if (!m.matches()) {
            throw new IllegalArgumentException("Invalid IPV4 address");
        }

        byte[] bytes = new byte[4];
        for (int n = 0; n < 4; n++) {
            try {
                bytes[n] = (byte) Integer.parseInt(m.group(n + 1), 10);
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException("Invalid IPV4 address", nfe);
            }
        }

        try {
            return (Inet4Address) InetAddress.getByAddress(bytes);
        } catch (UnknownHostException e) {
            throw new IllegalArgumentException("Invalid IPV4 address", e);
        }
    }

    /**
     * Returns the first non-loopback IP address of any interface. This is only
     * useful for machines with one NIC and shouldn't be depended on to give
     * predictable results.
     */
    public static Inet4Address getDefaultIPAddress() throws IOException {
        for (NetworkInterface ni : Collections.list(NetworkInterface
                .getNetworkInterfaces())) {
            for (InetAddress ina : Collections.list(ni.getInetAddresses())) {
                if (!(ina instanceof Inet4Address)) {
                    continue;
                }

                Inet4Address address = (Inet4Address) ina;

                if (address.isLoopbackAddress()) {
                    continue;
                }

                return address;
            }
        }

        return null;
    }

    public static byte[] getMACForInetAddress(Inet4Address address)
            throws IOException {

        EthernetInterfaces interfaces = getAvailableInterfaces();

        for (MACAddress mac : interfaces.getInterfaces()) {
            if (interfaces.getIP(mac).equals(address)) {
                return mac.getMACAddress();
            }
        }

        LOG.info("Couldn't find MAC address for " + address.getHostAddress());

        return null;
    }

    /**
     * Returns all available capture devices that have at least one IPv4 address
     * set.
     * 
     * @return the array of available capture devices
     * @throws IOException
     *             if calling the helper fails
     */
    static String[] getCaptureDevices() throws IOException {
        // Now spawn the helper to list all available capture devices
        Process helper = spawnTransportHelper("list");

        int exitCode;
        try {
            exitCode = helper.waitFor();
        } catch (InterruptedException e) {
            throw new IOException(e);
        }

        if (exitCode != 0) {
            throw new IOException("Transport helper process exited with "
                    + exitCode);
        }

        List<String> devices = new ArrayList<String>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(
                helper.getInputStream()));
        String device = null;

        String line;
        while ((line = reader.readLine()) != null) {
            if (!line.startsWith("   ")) {
                device = line;
                continue;
            }

            line = line.trim();

            if (!line.startsWith("ipv4:")) {
                continue;
            }

            if (device != null) {
                devices.add(device);
                // Add the device only once
                device = null;
            }
        }

        return devices.toArray(new String[0]);
    }

    static String getCaptureDevice(MACAddress address) throws IOException {
        // Now spawn the helper to list all available capture devices
        Process helper = spawnTransportHelper("list");

        int exitCode;
        try {
            exitCode = helper.waitFor();
        } catch (InterruptedException e) {
            throw new IOException(e);
        }

        if (exitCode != 0) {
            LOG.info("Transport helper process exited with " + exitCode);
            return null;
        }

        String ll = "ll: " + address;

        BufferedReader reader = new BufferedReader(new InputStreamReader(
                helper.getInputStream()));
        String device = null;

        String line;
        while ((line = reader.readLine()) != null) {
            if (!line.startsWith("   ")) {
                device = line;
                continue;
            }

            line = line.trim();

            if (!line.startsWith("ll:")) {
                continue;
            }

            if (line.equals(ll)) {
                if (device == null) {
                    // Helper must have produced garbage output
                    LOG.info("Found a matching address but no associated device");
                    continue;
                }

                return device;
            }
        }

        // No device for the MAC address
        LOG.info("Couldn't find capture device for " + address);
        return null;
    }

    /**
     * Returns all Ethernet interfaces that can be used for PN-RT.
     */
    public static EthernetInterfaces getAvailableInterfaces()
            throws IOException {
        EthernetInterfaces interfaces = new EthernetInterfaces();

        // Now spawn the helper to list all available capture devices
        Process helper = spawnTransportHelper("list");

        int exitCode;
        try {
            exitCode = helper.waitFor();
        } catch (InterruptedException e) {
            throw new IOException(e);
        }

        if (exitCode != 0) {
            throw new IOException("Transport helper process exited with "
                    + exitCode);
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(
                helper.getInputStream()));
        String device = null;

        // We remember all duplicate MACs and skip these devices
        Set<MACAddress> macs = new HashSet<MACAddress>();
        MACAddress mac = null;
        InetAddress ip = null;

        String line;
        while ((line = reader.readLine()) != null) {
            if (!line.startsWith("   ")) {
                if (device != null && mac != null && ip != null) {
                    if (macs.contains(mac)) {
                        LOG.info("Found duplicate MAC address: "
                                + mac.toString());
                    } else {
                        macs.add(mac);
                        interfaces.addInterface(mac, ip);
                    }
                }

                device = line;
                mac = null;
                ip = null;
                continue;
            }

            line = line.trim();

            if (line.startsWith("ll:")) {
                try {
                    mac = new MACAddress(fromString(line.substring(3)));
                } catch (IllegalArgumentException e) {
                    // Helper must have produced garbage output
                    LOG.info("Found garbled MAC address");
                }
            } else if (line.startsWith("ipv4:")) {
                try {
                    ip = ipFromString(line.substring(5));
                } catch (IllegalArgumentException e) {
                    // Helper must have produced garbage output
                    LOG.info("Found garbled IPV4 address");
                }
            }
        }

        if (device != null && mac != null && ip != null) {
            if (macs.contains(mac)) {
                LOG.info("Found duplicate MAC address: " + mac.toString());
            } else {
                macs.add(mac);
                interfaces.addInterface(mac, ip);
            }
        }

        return interfaces;
    }

    /**
     * Spawns a new PN-RT Ethernet helper process. This allows to decouple raw
     * packet handling into a process with limited capabilities but higher
     * permissions.
     * 
     * @param args
     *            additional arguments to the process
     * @return the running process
     * 
     * @throws IOException
     *             if the helper couldn't be started
     */
    static Process spawnTransportHelper(String... args) throws IOException {
        // TODO: This can be abused to execute any binary as root if useSU is
        // set
        String pnrtTransport = System.getProperty(
                "de.colin.profinet.pnrt.transport-helper", "pnrt-transport");

        List<String> command = new ArrayList<String>();

        if (Boolean.getBoolean("de.colin.profinet.pnrt.useSU")) {
            final String WHITELIST = "[a-zA-Z0-9:_-]+";

            command.add("su");
            command.add("-c");

            StringBuilder c = new StringBuilder();

            c.append("exec ");
            // TODO: This is not safe against shell code injection
            c.append(pnrtTransport);
            for (String a : args) {
                // The whitelist should take care of injection for arguments
                if (!a.matches(WHITELIST)) {
                    throw new IOException("Illegal argument for SU transport ("
                            + a + ")");
                }

                c.append(" ");
                c.append(a);
            }

            command.add(c.toString());

        } else {
            command.add(pnrtTransport);
            command.addAll(Arrays.asList(args));
        }

        ProcessBuilder builder = new ProcessBuilder(command);
        builder.redirectErrorStream(true);

        return builder.start();
    }
}
