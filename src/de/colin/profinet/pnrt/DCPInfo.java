/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.pnrt;

import java.net.InetAddress;
import java.net.SocketAddress;

/**
 * Contains the properties that a PROFINET device will send in its DCP identify
 * response.
 */
public class DCPInfo {

    private final SocketAddress pnrtAddress;

    private final int vendorID;
    private final int deviceID;

    private final String nameOfStation;
    private final String typeOfStation;

    private final StationRole role;

    private final InetAddress ipAddress;
    private final InetAddress netmask;
    private final InetAddress gateway;

    public DCPInfo(SocketAddress pnrtAddress, int vendorID, int deviceID,
            String nameOfStation, String typeOfStation, StationRole role,
            InetAddress ipAddress, InetAddress netmask, InetAddress gateway) {

        this.pnrtAddress = pnrtAddress;
        this.vendorID = vendorID;
        this.deviceID = deviceID;
        this.nameOfStation = nameOfStation;
        this.typeOfStation = typeOfStation;
        this.role = role;
        this.ipAddress = ipAddress;
        this.netmask = netmask;
        this.gateway = gateway;
    }

    public SocketAddress getPNRTAddress() {
        return pnrtAddress;
    }

    public int getVendorID() {
        return vendorID;
    }

    public int getDeviceID() {
        return deviceID;
    }

    public String getNameOfStation() {
        return nameOfStation;
    }

    public String getTypeOfStation() {
        return typeOfStation;
    }

    public StationRole getRole() {
        return role;
    }

    public InetAddress getIPAddress() {
        return ipAddress;
    }

    public InetAddress getNetmask() {
        return netmask;
    }

    public InetAddress getGateway() {
        return gateway;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        if (nameOfStation != null) {
            builder.append("nameOfStation=");
            builder.append(nameOfStation);
            builder.append(", ");
        }
        if (typeOfStation != null) {
            builder.append("typeOfStation=");
            builder.append(typeOfStation);
            builder.append(", ");
        }
        if (pnrtAddress != null) {
            builder.append("pnrtAddress=");
            builder.append(pnrtAddress);
            builder.append(", ");
        }
        if (ipAddress != null) {
            builder.append("ipAddress=");
            builder.append(ipAddress);
            builder.append(", ");
        }
        if (netmask != null) {
            builder.append("netmask=");
            builder.append(netmask);
            builder.append(", ");
        }
        if (gateway != null) {
            builder.append("gateway=");
            builder.append(gateway);
            builder.append(", ");
        }
        if (vendorID != -1) {
            builder.append("vendorID=");
            builder.append(String.format("0x%04X", vendorID));
            builder.append(", ");
        }
        if (deviceID != -1) {
            builder.append("deviceID=");
            builder.append(String.format("0x%04X", deviceID));
            builder.append(", ");
        }
        if (role != null) {
            builder.append("role=");
            builder.append(role);
        }
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + deviceID;
        result = prime * result + ((gateway == null) ? 0 : gateway.hashCode());
        result = prime * result
                + ((ipAddress == null) ? 0 : ipAddress.hashCode());
        result = prime * result
                + ((nameOfStation == null) ? 0 : nameOfStation.hashCode());
        result = prime * result + ((netmask == null) ? 0 : netmask.hashCode());
        result = prime * result
                + ((pnrtAddress == null) ? 0 : pnrtAddress.hashCode());
        result = prime * result + ((role == null) ? 0 : role.hashCode());
        result = prime * result
                + ((typeOfStation == null) ? 0 : typeOfStation.hashCode());
        result = prime * result + vendorID;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DCPInfo other = (DCPInfo) obj;
        if (deviceID != other.deviceID)
            return false;
        if (gateway == null) {
            if (other.gateway != null)
                return false;
        } else if (!gateway.equals(other.gateway))
            return false;
        if (ipAddress == null) {
            if (other.ipAddress != null)
                return false;
        } else if (!ipAddress.equals(other.ipAddress))
            return false;
        if (nameOfStation == null) {
            if (other.nameOfStation != null)
                return false;
        } else if (!nameOfStation.equals(other.nameOfStation))
            return false;
        if (netmask == null) {
            if (other.netmask != null)
                return false;
        } else if (!netmask.equals(other.netmask))
            return false;
        if (pnrtAddress == null) {
            if (other.pnrtAddress != null)
                return false;
        } else if (!pnrtAddress.equals(other.pnrtAddress))
            return false;
        if (role != other.role)
            return false;
        if (typeOfStation == null) {
            if (other.typeOfStation != null)
                return false;
        } else if (!typeOfStation.equals(other.typeOfStation))
            return false;
        if (vendorID != other.vendorID)
            return false;
        return true;
    }
}
