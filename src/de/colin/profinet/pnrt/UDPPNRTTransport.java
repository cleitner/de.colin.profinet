/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.pnrt;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.DatagramChannel;

/**
 * Implementation of the PN-RT transport for UDP.
 */
final class UDPPNRTTransport extends PNRTTransport {

    private DatagramChannel channel;

    private final ByteBuffer sendPacket = ByteBuffer.allocate(1514);

    public UDPPNRTTransport(InetSocketAddress localAddress, PNRTHandler handler)
            throws IOException {
        super(localAddress, handler);

        try {
            channel = DatagramChannel.open();
            channel.socket().setReuseAddress(true);
            channel.socket().bind(localAddress);
        } catch (IOException e) {
            implClose();

            throw e;
        }
    }

    @Override
    protected void implClose() {
        if (channel != null) {
            try {
                channel.close();
            } catch (IOException e) {
                // Ignore
            }
        }
    }

    @Override
    public InetSocketAddress getLocalAddress() {
        return (InetSocketAddress) super.getLocalAddress();
    }

    @Override
    public void run() {
        // Don't even attempt to run the receiver if nobody is listening
        if (getHandler() == null) {
            return;
        }

        // This check is insufficient, however for the common abuse it should be
        // good enough
        if (!isOpen()) {
            throw new IllegalStateException();
        }

        ByteBuffer incoming = ByteBuffer.allocate(1500);
        incoming.order(ByteOrder.BIG_ENDIAN);

        while (isOpen()) {
            InetSocketAddress src;

            incoming.clear();

            try {
                src = (InetSocketAddress) channel.receive(incoming);
            } catch (IOException e) {
                close(e);
                break;
            }

            incoming.flip();

            if (incoming.remaining() < 2) {
                // Invalid frame if the frame ID is missing
                continue;
            }

            int frameID = incoming.getShort() & 0xFFFF;

            getHandler().onFrame(this, src, frameID, incoming.slice());
        }
    }

    @Override
    public void send(SocketAddress destination, int frameID, ByteBuffer buf)
            throws IOException {
        synchronized (sendPacket) {
            sendPacket.clear();
            sendPacket.put((byte) (frameID >> 8));
            sendPacket.put((byte) (frameID & 0xFF));
            sendPacket.put(buf);
            sendPacket.flip();

            try {
                channel.send(sendPacket, destination);
            } catch (IOException e) {
                close(e);
                throw e;
            }
        }
    }
}
