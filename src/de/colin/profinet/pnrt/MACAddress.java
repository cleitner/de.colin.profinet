/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.pnrt;

import java.net.SocketAddress;
import java.util.Arrays;

/**
 * 48-Bit MAC address.
 */
public class MACAddress extends SocketAddress {

    private static final long serialVersionUID = 4301535357776178269L;

    private final byte[] macAddress;

    public MACAddress(byte[] macAddress) {
        this.macAddress = Arrays.copyOf(macAddress, 6);
    }

    public byte[] getMACAddress() {
        return macAddress.clone();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(macAddress);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MACAddress other = (MACAddress) obj;
        if (!Arrays.equals(macAddress, other.macAddress))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return EthernetUtils.toString(macAddress);
    }
}
