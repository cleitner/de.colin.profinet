/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.pnrt;

import java.net.InetAddress;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Stores all local Ethernet interfaces which could be used as PN-RT devices.
 */
public class EthernetInterfaces {

    private final Set<MACAddress> interfaces;
    private final Set<MACAddress> roInterfaces;
    private final Map<MACAddress, InetAddress> macToIP;

    public EthernetInterfaces() {
        interfaces = new HashSet<MACAddress>();
        roInterfaces = Collections.unmodifiableSet(interfaces);
        macToIP = new HashMap<MACAddress, InetAddress>();
    }

    void addInterface(MACAddress mac, InetAddress ip) {
        if (interfaces.contains(mac)) {
            throw new IllegalArgumentException("Duplicate MAC address");
        }

        if (ip == null) {
            throw new NullPointerException();
        }

        interfaces.add(mac);
        macToIP.put(mac, ip);
    }

    public Set<MACAddress> getInterfaces() {
        return roInterfaces;
    }

    public InetAddress getIP(MACAddress mac) {
        return macToIP.get(mac);
    }
}
