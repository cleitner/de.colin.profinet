/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.pnrt;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.colin.ipc.IPCProcess;

/**
 * Implementation of the PN-RT transport for raw Ethernet.
 */
final class EthernetPNRTTransport extends PNRTTransport {

    private static final Logger LOG = Logger
            .getLogger(EthernetPNRTTransport.class.getName());

    private static final int IPC_ID_SEND = 1;
    private static final int IPC_ID_RECEIVE = 2;
    private static final int IPC_ID_HELLO = 3;

    private IPCProcess sendProcess;
    private IPCProcess receiveProcess;

    private final ByteBuffer sendPacket;

    public EthernetPNRTTransport(MACAddress localAddress, PNRTHandler handler)
            throws IOException {
        super(localAddress, handler);

        sendPacket = ByteBuffer.allocate(1514);

        final String device;
        device = EthernetUtils.getCaptureDevice(localAddress);
        if (device == null) {
            throw new IOException(
                    "Couldn't find the network interface for MAC address "
                            + localAddress);
        }

        try {
            sendProcess = new IPCProcess() {
                @Override
                protected Process createProcess() throws IOException {
                    return EthernetUtils.spawnTransportHelper("send", device,
                            getLocalAddress().toString());
                }

                @Override
                protected void onError(IOException e) {
                    EthernetPNRTTransport.this.close(e);
                }
            };
            sendProcess.startAndRun();
            sendProcess.call(IPC_ID_HELLO, ByteBuffer.allocate(0));

            if (handler != null) {
                receiveProcess = new IPCProcess() {
                    @Override
                    protected Process createProcess() throws IOException {
                        return EthernetUtils.spawnTransportHelper("receive",
                                device, getLocalAddress().toString());
                    }

                    @Override
                    protected void onReceive(int id, ByteBuffer buf) {
                        if (id == IPC_ID_RECEIVE) {
                            EthernetPNRTTransport.this.onReceive(buf);
                        } else {
                            super.onReceive(id, buf);
                        }
                    }

                    @Override
                    protected void onError(IOException e) {
                        EthernetPNRTTransport.this.close(e);
                    }
                };
                receiveProcess.start();
            }

        } catch (IOException e) {
            implClose();

            throw e;
        }
    }

    @Override
    public MACAddress getLocalAddress() {
        return (MACAddress) super.getLocalAddress();
    }

    @Override
    protected void implClose() {
        if (sendProcess != null) {
            sendProcess.close();
        }

        if (receiveProcess != null) {
            receiveProcess.close();
        }
    }

    @Override
    public void send(SocketAddress destination, int frameID, ByteBuffer buf)
            throws IOException {
        // This check is insufficient, however for the common abuse it should be
        // good enough
        if (!isOpen()) {
            throw new IOException("Transport is closed");
        }

        if ((frameID < 0) || (frameID > 0xFFFF)) {
            throw new IllegalArgumentException(String.format(
                    "Invalid frame ID 0x%04X", frameID));
        }

        MACAddress dstAddress = (MACAddress) destination;

        synchronized (sendPacket) {
            sendPacket.clear();
            sendPacket.put(dstAddress.getMACAddress());
            sendPacket.put(getLocalAddress().getMACAddress());
            sendPacket.put((byte) 0x88);
            sendPacket.put((byte) 0x92);
            sendPacket.put((byte) (frameID >> 8));
            sendPacket.put((byte) (frameID & 0xFF));
            sendPacket.put(buf);
            sendPacket.flip();

            try {
                if (LOG.isLoggable(Level.FINEST)) {
                    LOG.log(Level.FINEST, String.format(
                            "Sending frame %04X to %s", frameID,
                            EthernetUtils.toString(dstAddress.getMACAddress())));
                }

                sendProcess.send(IPC_ID_SEND, sendPacket);
            } catch (IOException e) {
                close(e);

                throw e;
            }
        }
    }

    @Override
    public void run() {
        // Don't even attempt to run the receiver if nobody is listening
        if (getHandler() == null) {
            return;
        }

        // This check is insufficient, however for the common abuse it should be
        // good enough
        if (!isOpen()) {
            throw new IllegalStateException();
        }

        receiveProcess.run();
    }

    private void onReceive(ByteBuffer packet) {
        int offset = 0;

        // Strip VLAN header if present
        if (packet.remaining() >= 18) {
            if (((packet.get(12) & 0xFF) == 0x81)
                    && ((packet.get(13) & 0xFF) == 0x00)) {
                offset += 4;
            }
        }

        byte[] srcMac = new byte[6];
        packet.position(6);
        packet.get(srcMac);

        packet.order(ByteOrder.BIG_ENDIAN);
        packet.position(14 + offset);

        int frameID = packet.getShort() & 0xFFFF;

        if (LOG.isLoggable(Level.FINEST)) {
            LOG.log(Level.FINEST, String.format("Receiving frame %04X from %s",
                    frameID, EthernetUtils.toString(srcMac)));
        }

        getHandler().onFrame(this, new MACAddress(srcMac), frameID,
                packet.slice());
    }

}
