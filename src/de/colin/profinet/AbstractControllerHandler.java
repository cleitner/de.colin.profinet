/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet;

import java.net.SocketAddress;

import de.colin.profinet.io.Frame;
import de.colin.profinet.pnrt.DCPInfo;

// Not really abstract to force us to implement all interface methods
public class AbstractControllerHandler implements ControllerHandler {

    protected AbstractControllerHandler() {
    }

    @Override
    public void onConnect(Controller controller) {
    }

    @Override
    public void onApplicationReady(Controller controller) {
    }

    @Override
    public void onDisconnect(Controller controller) {
    }

    @Override
    public void onStationDetected(Controller controller, SocketAddress station,
            DCPInfo info) {
    }

    @Override
    public void onStationChanged(Controller controller, SocketAddress station,
            DCPInfo info) {
    }

    @Override
    public void onStationLost(Controller controller, SocketAddress station) {
    }

    @Override
    public void onAlarmAppears(Controller controller, AlarmPriority priority,
            int slot, int subslot, int channel, int errorType) {
    }

    @Override
    public void onAlarmDisappears(Controller controller,
            AlarmPriority priority, int slot, int subslot, int channel) {
    }

    @Override
    public void onIOOutput(Controller controller, long timestamp, Frame frame) {
    }

    @Override
    public void onIOInput(Controller controller, long timestamp, Frame frame) {
    }

    @Override
    public void onIOTimeout(Controller controller, long timestamp) {
    }

}
