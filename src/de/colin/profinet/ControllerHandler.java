/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet;

import java.net.SocketAddress;

import de.colin.profinet.io.Frame;
import de.colin.profinet.pnrt.DCPInfo;

/**
 * Handler for {@code Controller} events.
 * 
 * @author Colin Leitner
 */
public interface ControllerHandler {

    /**
     * Called after the controller successfully connected to the device. The
     * device requires parameters and completes the connection after
     * {@link Controller#parameterEnd()} has been called.
     * 
     * @param controller
     *            the controller that connected successfully
     * 
     * @see {@link Controller#parameterEnd() parameterEnd}
     * @see {@link #onApplicationReady(Controller) onApplicationReady}
     */
    void onConnect(Controller controller);

    /**
     * Called on reception of the application ready indication from the device.
     * The session is in regular I/O exchange now.
     * 
     * @param controller
     *            the controller that received the application ready
     */
    void onApplicationReady(Controller controller);

    // TODO: More documentation
    void onDisconnect(Controller controller);

    // TODO: More documentation
    void onStationDetected(Controller controller, SocketAddress station,
            DCPInfo info);

    void onStationChanged(Controller controller, SocketAddress station,
            DCPInfo info);

    void onStationLost(Controller controller, SocketAddress station);

    /**
     * Called after reception of an appearing alarm.
     * 
     * @param controller
     *            the controller that received the alarm
     * @param priority
     *            the alarm priority
     * @param slot
     *            the module slot
     * @param subslot
     *            the submodule slot
     * @param errorType
     *            the diagnosis code
     */
    void onAlarmAppears(Controller controller, AlarmPriority priority,
            int slot, int subslot, int channel, int errorType);

    /**
     * Called after reception of a disappearing alarm.
     * 
     * @param controller
     *            the controller that received the alarm
     * @param priority
     *            the alarm priority
     * @param slot
     *            the module slot
     * @param subslot
     *            the submodule slot
     */
    void onAlarmDisappears(Controller controller, AlarmPriority priority,
            int slot, int subslot, int channel);

    /**
     * Called to update a frame before sending it. The data service instance
     * will always use the same frame for sending, so it will always contain the
     * state from the last call.
     * 
     * This method can be called even if no connection exits (or exists yet).
     * 
     * @param controller
     *            the controller that called this handler
     * @param timestamp
     *            the timestamp since sending began in nanoseconds. this value
     *            is used for the outgoing cycle counter in PN-IO and can be
     *            used for timing
     * @param frame
     *            the outgoing frame
     */
    void onIOOutput(Controller controller, long timestamp, Frame frame);

    /**
     * Called with the most recent input frame, right after reception.
     * 
     * This method can be called even if no connection exits (or exists yet).
     * 
     * @param controller
     *            the controller that called this handler
     * @param timestamp
     *            the timestamp since reception began in nanoseconds. This
     *            timestamp reflects the same timer used for the
     *            {@code onIOOutput} timestamp.
     * @param frame
     *            the updated frame
     */
    void onIOInput(Controller controller, long timestamp, Frame frame);

    /**
     * Called after the device stopped sending input frames. The connection is
     * automatically released. The timeout is enabled after application ready
     * has been received.
     * 
     * @param controller
     *            the controller that detected the timeout
     * @param timestamp
     *            the timestamp since reception began in nanoseconds. This
     *            timestamp reflects the same timer used for the
     *            {@code onIOOutput} timestamp.
     */
    void onIOTimeout(Controller controller, long timestamp);

}
