/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public enum DataType {
    UNSIGNED8(1, new Formatter() {
        public String format(ByteBuffer image) {
            return Integer.toString(image.get() & 0xff);
        }
    }), UNSIGNED16(2, new Formatter() {
        public String format(ByteBuffer image) {
            return Integer.toString(image.getShort() & 0xffff);
        }
    }), UNSIGNED32(4, new Formatter() {
        public String format(ByteBuffer image) {
            return Long.toString(image.getInt() & 0xffffffffl);
        }
    }), UNSIGNED64(8, new Formatter() {
        public String format(ByteBuffer image) {
            return BigInteger.valueOf(image.getLong()).abs().toString();
        }
    }), INTEGER8(1, new Formatter() {
        public String format(ByteBuffer image) {
            return Integer.toString(image.get());
        }
    }), INTEGER16(2, new Formatter() {
        public String format(ByteBuffer image) {
            return Integer.toString(image.getShort());
        }
    }), INTEGER32(4, new Formatter() {
        public String format(ByteBuffer image) {
            return Integer.toString(image.getInt());
        }
    }), INTEGER64(8, new Formatter() {
        public String format(ByteBuffer image) {
            return Long.toString(image.getLong());
        }
    }), FLOAT32(4, new Formatter() {
        public String format(ByteBuffer image) {
            return Float.toString(image.getFloat());
        }
    }), FLOAT64(8, new Formatter() {
        public String format(ByteBuffer image) {
            return Double.toString(image.getDouble());
        }
    }), VISIBLE_STRING(-1, new Formatter() {

        public String format(ByteBuffer image) {
            return CHARSET.decode(image).toString();
        }
    }), OCTET_STRING(-1, new Formatter() {
        public String format(ByteBuffer image) {
            StringBuilder str = new StringBuilder();

            boolean first = true;
            while (image.hasRemaining()) {
                int b = image.get() & 0xff;

                if (first) {
                    first = false;
                } else {
                    str.append(" ");
                }

                str.append(String.format("%02X", b));
            }

            return str.toString();
        }
    }), F_MESSAGETRAILER4BYTE(4, new Formatter() {
        public String format(ByteBuffer image) {
            StringBuilder str = new StringBuilder();

            str.append("[C/SB: ");

            String bits = Integer.toBinaryString(image.get() & 0xff);
            for (int b = 0; b < 8 - bits.length(); b++) {
                str.append("0");
            }
            str.append(bits);

            str.append(", CRC:");
            for (int c = 0; c < 3; c++) {
                int b = image.get() & 0xff;
                str.append(String.format(" %02X", b));
            }

            str.append("]");

            return str.toString();
        }
    });

    private static final Charset CHARSET = Charset.forName("ISO-8859-1");

    private final int length;
    private final Formatter formatter;

    private DataType(int length, Formatter formatter) {
        this.length = length;
        this.formatter = formatter;
    }

    public int getLength() {
        return length;
    }

    public String format(ByteBuffer image) {
        return formatter.format(image);
    }
}

interface Formatter {
    String format(ByteBuffer image);
}
