/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Module {

    private final int moduleIdentNumber;
    private final ModuleInfo moduleInfo;

    private final List<Submodule> submodules;

    public Module(int moduleIdentNumber, ModuleInfo info,
            List<Submodule> submodules) {
        if (info == null) {
            throw new NullPointerException();
        }

        this.moduleInfo = info;
        this.moduleIdentNumber = moduleIdentNumber;

        if (submodules.isEmpty()) {
            throw new IllegalArgumentException();
        }

        this.submodules = Collections
                .unmodifiableList(new ArrayList<Submodule>(submodules));
    }

    public int getModuleIdentNumber() {
        return moduleIdentNumber;
    }

    public Text getName() {
        return moduleInfo.getName();
    }

    public ModuleInfo getModuleInfo() {
        return moduleInfo;
    }

    public List<Submodule> getSubmodules() {
        return submodules;
    }

    public Submodule getDefaultSubmodule() {
        return submodules.get(0);
    }

    @Override
    public String toString() {
        return moduleInfo.getName().toString();
    }
}
