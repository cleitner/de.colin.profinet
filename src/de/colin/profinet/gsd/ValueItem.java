/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ValueItem {

    private final Text help;
    private final List<Value> values;

    public ValueItem(Text help, List<Value> values) {
        this.help = help;
        this.values = Collections
                .unmodifiableList(new ArrayList<Value>(values));
    }

    public Text getHelp() {
        return help;
    }

    public List<Value> getValues() {
        return values;
    }

    public long getValue(String text) {
        for (Value v : values) {
            if (v.getText().toString().equals(text)) {
                return v.getValue();
            }
        }

        throw new IllegalArgumentException();
    }
}
