/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

public class Value {
    private final Text text;
    private final long value;

    public Value(Text text, long value) {
        if (text == null) {
            throw new NullPointerException();
        }

        this.text = text;
        this.value = value;
    }

    public Text getText() {
        return text;
    }

    public long getValue() {
        return value;
    }

    @Override
    public String toString() {
        return text.toString();
    }
}
