/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class GSDRegistry {

    private final Deque<GSD> gsds = new LinkedList<GSD>();

    public GSD add(String fileName) throws IOException, GSDException {
        return add(fileName, new FileInputStream(fileName));
    }

    public GSD add(String fileName, InputStream input) throws IOException,
            GSDException {
        GSD gsd = GSDMLImporter.importGSDML(fileName, input);

        gsds.push(gsd);

        return gsd;
    }

    public DeviceAccessPoint getDeviceAccessPoint(String name) {
        Iterator<GSD> g = gsds.descendingIterator();
        while (g.hasNext()) {
            GSD gsd = g.next();

            DeviceAccessPoint dap = gsd.getDeviceAccessPoint(name);
            if (dap != null) {
                return dap;
            }
        }

        return null;
    }
}
