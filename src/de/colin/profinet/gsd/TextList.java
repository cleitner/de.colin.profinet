/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Support class for {@code GSDMLImporter}
 */
class TextList {

    public static final String PRIMARY_LANGUAGE = "en";

    private final Map<String, Map<String, String>> langTexts = new HashMap<String, Map<String, String>>();

    private String currentLang = PRIMARY_LANGUAGE;

    public void clear() {
        langTexts.clear();
    }

    public String getCurrentLanguage() {
        return currentLang;
    }

    public void setCurrentLanguage(String lang) {
        if (lang == null) {
            lang = PRIMARY_LANGUAGE;
        }

        this.currentLang = lang;
    }

    public Set<String> getTextIds() {
        Map<String, String> texts = langTexts.get(PRIMARY_LANGUAGE);
        if (texts == null) {
            return Collections.unmodifiableSet(new HashSet<String>());
        }

        return Collections.unmodifiableSet(texts.keySet());
    }

    public Set<String> getLanguages() {
        return Collections.unmodifiableSet(langTexts.keySet());
    }

    public void setText(String lang, String id, String text) {
        lang = lang.toLowerCase();

        Map<String, String> texts = langTexts.get(lang);
        if (texts == null) {
            texts = new HashMap<String, String>();
            langTexts.put(lang, texts);
        }

        texts.put(id, text);
    }

    public void setText(String id, String text) {
        setText(getCurrentLanguage(), id, text);
    }

    public String getText(String lang, String id) {
        lang = lang.toLowerCase();

        Map<String, String> texts = langTexts.get(lang);
        if (texts == null) {
            return null;
        }

        String text = texts.get(id);
        if (text == null && !lang.equals(PRIMARY_LANGUAGE)) {
            // Retry with the primary language
            return getText(PRIMARY_LANGUAGE, id);
        } else {
            return text;
        }
    }

    public String getText(String id) {
        return getText(getCurrentLanguage(), id);
    }

    /**
     * Creates a map from text Ids to Text instances, containing all available
     * translations.
     */
    public Map<String, Text> createTextMapping() {
        Map<String, Text> texts = new HashMap<String, Text>();
        Set<String> langs = getLanguages();

        for (String id : getTextIds()) {
            Map<String, String> translations = new HashMap<String, String>();

            for (String lang : langs) {
                if (lang.equals(PRIMARY_LANGUAGE)) {
                    continue;
                }

                Map<String, String> langTexts = this.langTexts.get(lang);

                String text = langTexts.get(id);
                if (text != null) {
                    translations.put(lang, text);
                }
            }

            texts.put(id, new Text(getText(PRIMARY_LANGUAGE, id), translations));
        }

        return texts;
    }
}
