/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

public class ModuleInfo {
    private final Text name;
    private final Text infoText;

    private final String vendorName;
    private final String orderNumber;

    private final String hardwareRelease;
    private final String softwareRelease;

    public ModuleInfo(Text name, Text infoText, String vendorName,
            String orderNumber, String hardwareRelease, String softwareRelease) {
        if (name == null) {
            throw new NullPointerException();
        }
        this.name = name;
        this.infoText = infoText;
        this.vendorName = vendorName;
        this.orderNumber = orderNumber;
        this.hardwareRelease = hardwareRelease;
        this.softwareRelease = softwareRelease;
    }

    public Text getName() {
        return name;
    }

    public Text getInfoText() {
        return infoText;
    }

    public String getVendorName() {
        return vendorName;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public String getHardwareRelease() {
        return hardwareRelease;
    }

    public String getSoftwareRelease() {
        return softwareRelease;
    }
}
