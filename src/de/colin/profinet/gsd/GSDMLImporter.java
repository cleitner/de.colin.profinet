/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;

public class GSDMLImporter {

    public static interface ProgressCallback {
        void onImportProgress(int percent);
    }

    private static final Logger LOG = Logger.getLogger(GSDMLImporter.class
            .getName());

    private static final Namespace DEVICE_PROFILE = Namespace
            .getNamespace("http://www.profibus.com/GSDML/2003/11/DeviceProfile");

    private static final Text PROFISAFE = new Text("PROFIsafe");
    private static final Text F_SIL = new Text("F_SIL");
    private static final Text SIL1 = new Text("SIL1");
    private static final Text SIL2 = new Text("SIL2");
    private static final Text SIL3 = new Text("SIL3");

    public static GSD importGSDML(File file) throws IOException, GSDException {
        return importGSDML(file.getName(), new FileInputStream(file));
    }

    public static GSD importGSDML(String fileName, InputStream in)
            throws IOException, GSDException {
        return importGSDML(fileName, in, null);
    }

    public static GSD importGSDML(String fileName, InputStream in,
            ProgressCallback callback) throws IOException, GSDException {
        // GSDML is not designed for efficient parsing. We would like to have a
        // structure where all references could be resolved during parsing:
        // 1. Texts
        // 2. Values
        // 3. Modules
        // 4. DAPs
        // It's the opposite of course x_X which leaves us with DOM only (or
        // multiple passes).

        Document doc;
        try {
            SAXBuilder builder = new SAXBuilder();
            builder.setXMLReaderFactory(XMLReaders.NONVALIDATING);
            doc = builder.build(in);
            in.close();
        } catch (JDOMException e) {
            throw new GSDException(e);
        }

        return new GSDMLImporter(doc, callback).run();
    }

    private final Document doc;
    private final ProgressCallback callback;

    private Element ap;

    private Map<String, Text> texts;
    private Map<String, ValueItem> values;

    private short vendorId;
    private short deviceId;
    private String vendorName;
    private Text infoText;

    private Map<String, Module> idToModules;
    private Set<Module> modules;

    private Set<DeviceAccessPoint> daps;

    private GSDMLImporter(Document doc, ProgressCallback callback) {
        this.doc = doc;
        this.callback = callback;
    }

    private void setProgress(int progress) {
        if (callback != null) {
            callback.onImportProgress(progress);
        }
    }

    private Text getText(String id) {
        assert texts != null;

        Text t = texts.get(id);

        if (t == null) {
            LOG.warning("Missing text for Id \"" + id + "\"");

            // Use the id as text instead
            t = new Text(id);
            texts.put(id, t);
        }

        return t;
    }

    private Text getText(Element node) {
        if (node == null) {
            return null;
        }

        String id = node.getAttributeValue("TextId");
        if (id == null) {
            LOG.warning("Missing TextId attribute");
            return null;
        }

        return getText(id);
    }

    private String getValue(Element node) {
        if (node == null) {
            return null;
        }

        return node.getAttributeValue("Value");
    }

    private void importDeviceInfo() throws GSDException {
        Element deviceIdentity = doc.getRootElement()
                .getChild("ProfileBody", DEVICE_PROFILE)
                .getChild("DeviceIdentity", DEVICE_PROFILE);
        if (deviceIdentity == null) {
            throw new GSDException("Missing <DeviceIdentity> element");
        }

        String vendorIDStr = deviceIdentity.getAttributeValue("VendorID");
        String deviceIDStr = deviceIdentity.getAttributeValue("DeviceID");
        if (vendorIDStr == null || deviceIDStr == null) {
            throw new GSDException(
                    "Missing vendor or device ID in <DeviceIdentity>");
        }

        try {
            if (vendorIDStr.startsWith("0x") || vendorIDStr.startsWith("0X")) {
                vendorIDStr = vendorIDStr.substring(2);
                vendorId = (short) Integer.parseInt(vendorIDStr, 16);
            } else {
                vendorId = (short) Integer.parseInt(vendorIDStr);
            }
        } catch (NumberFormatException nfe) {
            throw new GSDException("Invalid vendor ID", nfe);
        }

        try {
            if (deviceIDStr.startsWith("0x") || deviceIDStr.startsWith("0X")) {
                deviceIDStr = deviceIDStr.substring(2);
                deviceId = (short) Integer.parseInt(deviceIDStr, 16);
            } else {
                deviceId = (short) Integer.parseInt(deviceIDStr);
            }
        } catch (NumberFormatException nfe) {
            throw new GSDException("Invalid device ID", nfe);
        }

        vendorName = getValue(deviceIdentity.getChild("VendorName",
                DEVICE_PROFILE));
        infoText = getText(deviceIdentity.getChild("InfoText", DEVICE_PROFILE));
        if (vendorName == null || infoText == null) {
            throw new GSDException(
                    "Missing name or info text in <DeviceIdentity>");
        }
    }

    private void importTextList() throws GSDException {
        Element externalTextList = ap.getChild("ExternalTextList",
                DEVICE_PROFILE);
        if (externalTextList == null) {
            LOG.warning("No <ExternalTextList> in GSDML. Expect lots of missing text IDs");
            texts = new HashMap<String, Text>();
            return;
        }

        TextList tl = new TextList();

        for (Element lang : (List<Element>) externalTextList.getChildren()) {
            if (lang.getName().equals("PrimaryLanguage")) {
                tl.setCurrentLanguage(TextList.PRIMARY_LANGUAGE);
            } else {
                String langCode = lang.getAttributeValue("lang",
                        Namespace.XML_NAMESPACE);
                if (langCode == null) {
                    LOG.warning("Missing xml:lang attribute on <Language>");
                    continue;
                }

                tl.setCurrentLanguage(langCode);
            }

            for (Element textItem : (List<Element>) lang.getChildren()) {
                String id = textItem.getAttributeValue("TextId");
                String value = textItem.getAttributeValue("Value");

                if (id == null || value == null) {
                    LOG.warning("Missing attribute(s) in <Text>");
                    continue;
                }

                tl.setText(id, value);
            }
        }

        // Finally we have all texts with associated translations
        texts = tl.createTextMapping();
    }

    private void importValues() {
        values = new HashMap<String, ValueItem>();

        Element valueList = ap.getChild("ValueList", DEVICE_PROFILE);
        if (valueList == null) {
            return;
        }

        for (Element valueItem : (List<Element>) valueList.getChildren()) {

            String id = valueItem.getAttributeValue("ID");
            if (id == null) {
                LOG.warning("Missing ID for <ValueItem>");
                continue;
            }

            Text help = getText(valueItem.getChild("Help", DEVICE_PROFILE));

            List<Value> values = new ArrayList<Value>();

            Element assignments = valueItem.getChild("Assignments",
                    DEVICE_PROFILE);
            if (assignments != null) {
                for (Element a : (List<Element>) assignments.getChildren()) {
                    Text text = getText(a);
                    if (text == null) {
                        LOG.warning("Missing TextId attribute for <Assign>");
                        continue;
                    }

                    String content = a.getAttributeValue("Content");
                    if (content == null) {
                        LOG.warning("Missing Content attribute for <Assign>");
                        continue;
                    }

                    long value;
                    try {
                        value = new BigInteger(content).longValue();
                    } catch (NumberFormatException nfe) {
                        LOG.warning("Invalid value \"" + content + "\"");
                        continue;
                    }

                    values.add(new Value(text, value));
                }
            }

            ValueItem vi = new ValueItem(help, values);
            if (this.values.put(id, vi) != null) {
                LOG.warning("Duplicate <ValueItem> with ID \"" + id + "\"");
            }
        }
    }

    private void importModules() throws GSDException {
        modules = new HashSet<Module>();
        idToModules = new HashMap<String, Module>();

        Element moduleList = ap.getChild("ModuleList", DEVICE_PROFILE);
        if (moduleList == null) {
            return;
        }

        for (Element moduleItem : (List<Element>) moduleList.getChildren()) {
            String id = moduleItem.getAttributeValue("ID");
            if (id == null) {
                LOG.warning("Missing ID in <Module>");
                continue;
            }

            String moduleIdentNumberStr = moduleItem
                    .getAttributeValue("ModuleIdentNumber");
            if (moduleIdentNumberStr == null) {
                LOG.warning("Missing ModuleIdentNumber for ID \"" + id + "\"");
                continue;
            }

            int moduleIdentNumber;
            try {
                if (moduleIdentNumberStr.startsWith("0x")
                        || moduleIdentNumberStr.startsWith("0X")) {
                    moduleIdentNumberStr = moduleIdentNumberStr.substring(2);
                    moduleIdentNumber = Integer.parseInt(moduleIdentNumberStr,
                            16);
                } else {
                    moduleIdentNumber = Integer.parseInt(moduleIdentNumberStr);
                }
            } catch (NumberFormatException nfe) {
                LOG.warning("Invalid ModuleIdentNumber for ID \"" + id + "\"");
                continue;
            }

            Element moduleInfoElement = moduleItem.getChild("ModuleInfo",
                    DEVICE_PROFILE);
            if (moduleInfoElement == null) {
                LOG.warning("Missing module info for ID \"" + id + "\"");
                continue;
            }

            ModuleInfo moduleInfo = getModuleInfo(moduleInfoElement);

            Element virtualSubmoduleList = moduleItem.getChild(
                    "VirtualSubmoduleList", DEVICE_PROFILE);
            if (virtualSubmoduleList == null) {
                LOG.warning("Missing <VirtualSubmoduleList> for ID \"" + id
                        + "\"");
                continue;
            }

            List<Submodule> submodules = getSubmodules(virtualSubmoduleList);

            Module module = new Module(moduleIdentNumber, moduleInfo,
                    submodules);

            modules.add(module);
            if (idToModules.put(id, module) != null) {
                LOG.warning("Duplicate module ID \"" + id + "\"");
            }
        }
    }

    private List<Submodule> getSubmodules(Element virtualSubmoduleList) {
        List<Submodule> submodules = new ArrayList<Submodule>();
        for (Element virtualSubmoduleItem : (List<Element>) virtualSubmoduleList
                .getChildren()) {

            // No need to parse the ID. It couldn't be used anywhere anyway

            String submoduleIdentNumberStr = virtualSubmoduleItem
                    .getAttributeValue("SubmoduleIdentNumber");
            if (submoduleIdentNumberStr == null) {
                LOG.warning("Missing SubmoduleIdentNumber");
                continue;
            }

            int submoduleIdentNumber;
            try {
                if (submoduleIdentNumberStr.startsWith("0x")
                        || submoduleIdentNumberStr.startsWith("0X")) {
                    submoduleIdentNumberStr = submoduleIdentNumberStr
                            .substring(2);
                    submoduleIdentNumber = Integer.parseInt(
                            submoduleIdentNumberStr, 16);
                } else {
                    submoduleIdentNumber = Integer
                            .parseInt(submoduleIdentNumberStr);
                }
            } catch (NumberFormatException nfe) {
                LOG.warning("Invalid SubmoduleIdentNumber \""
                        + submoduleIdentNumberStr + "\"");
                continue;
            }

            Element moduleInfoElement = virtualSubmoduleItem.getChild(
                    "ModuleInfo", DEVICE_PROFILE);
            if (moduleInfoElement == null) {
                LOG.warning("Missing module info for submodule \""
                        + submoduleIdentNumberStr + "\"");
                continue;
            }

            ModuleInfo moduleInfo = getModuleInfo(moduleInfoElement);

            Data inputData;
            Data outputData;

            Element ioData = virtualSubmoduleItem.getChild("IOData",
                    DEVICE_PROFILE);
            if (ioData != null) {
                inputData = getIOData(ioData.getChild("Input", DEVICE_PROFILE));
                outputData = getIOData(ioData
                        .getChild("Output", DEVICE_PROFILE));
            } else {
                inputData = null;
                outputData = null;
            }

            // There is no need to parse the transfer sequence as the GSDML
            // standard requires them to be either unspecified or in order
            List<ParameterRecord> parameters = new ArrayList<ParameterRecord>();

            Element recordDataList = virtualSubmoduleItem.getChild(
                    "RecordDataList", DEVICE_PROFILE);
            if (recordDataList != null) {
                for (Element parameterRecordDataItem : (List<Element>) recordDataList
                        .getChildren()) {
                    ParameterRecord record = getParameterRecord(parameterRecordDataItem);
                    if (record != null) {
                        parameters.add(record);
                    }
                }
            }

            submodules.add(new Submodule(submoduleIdentNumber, moduleInfo,
                    inputData, outputData, parameters));
        }

        return submodules;
    }

    private ParameterRecord getPROFIsafeParameterRecord(Element record) {
        short index;
        String indexStr = record.getAttributeValue("Index");
        if (indexStr == null) {
            LOG.warning("Missing Index attribute for <F_ParameterRecordDataItem>");
            return null;
        }

        try {
            if (indexStr.startsWith("0x") || indexStr.startsWith("0X")) {
                indexStr = indexStr.substring(2);
                index = (short) Integer.parseInt(indexStr, 16);
            } else {
                index = (short) Integer.parseInt(indexStr);
            }
        } catch (NumberFormatException nfe) {
            LOG.warning("Invalid Index attribute \"" + indexStr + "\"");
            return null;
        }

        ByteBuffer defaultData;
        List<ParameterItem> items = new ArrayList<ParameterItem>();

        // TODO: Ich bin mir nicht sicher ob der iPar CRC in diesem Feld steckt,
        // oder ob bei !NoCheck ein weiteres Feld auftaucht
        // TODO: Das entsprechende ParameterItem muss noch zu items hinzugefügt
        // werden
        String checkIPar = record.getChild("F_Check_iPar", DEVICE_PROFILE)
                .getAttributeValue("DefaultValue", "0");
        if (checkIPar.equalsIgnoreCase("NoCheck")) {
            defaultData = ByteBuffer.allocate(10);
            defaultData.order(ByteOrder.BIG_ENDIAN);
        } else {
            defaultData = ByteBuffer.allocate(14);
            defaultData.order(ByteOrder.BIG_ENDIAN);
            defaultData.putInt(Integer.parseInt(checkIPar, 16));
        }

        Element f_sil = record.getChild("F_SIL", DEVICE_PROFILE);
        {
            String defaultValue = f_sil.getAttributeValue("DefaultValue");
            String allowedValues = f_sil.getAttributeValue("AllowedValues");

            byte sil;
            if (defaultValue.equals("SIL1")) {
                sil = 1;
            } else if (defaultValue.equals("SIL2")) {
                sil = 2;
            } else if (defaultValue.equals("SIL3")) {
                sil = 3;
            } else {
                LOG.warning("Unknown SIL Level \"" + defaultValue + "\"");
                return null;
            }

            List<Value> silValues = new ArrayList<Value>();

            if (allowedValues.contains("SIL1")) {
                silValues.add(new Value(SIL1, 1));
            }
            if (allowedValues.contains("SIL2")) {
                silValues.add(new Value(SIL2, 2));
            }
            if (allowedValues.contains("SIL3")) {
                silValues.add(new Value(SIL3, 3));
            }

            ValueItem values = new ValueItem(null, silValues);

            items.add(new ParameterItem(F_SIL, DataType.UNSIGNED8, 0, 0, 8,
                    sil, values));

            defaultData.put(sil);
        }

        return new ParameterRecord(index, PROFISAFE, defaultData.array(), items);
    }

    private ParameterRecord getParameterRecord(Element record) {
        // PROFIsafe?
        if (record.getName().equals("F_ParameterRecordDataItem")) {
            return getPROFIsafeParameterRecord(record);
        }

        Text name = getText(record.getChild("Name", DEVICE_PROFILE));
        if (name == null) {
            LOG.warning("Missing <Name> for <RecordDataItem>");
            return null;
        }

        short index;
        String indexStr = record.getAttributeValue("Index");
        if (indexStr == null) {
            LOG.warning("Missing Index attribute for <RecordDataItem>");
            return null;
        }

        try {
            if (indexStr.startsWith("0x") || indexStr.startsWith("0X")) {
                indexStr = indexStr.substring(2);
                index = (short) Integer.parseInt(indexStr, 16);
            } else {
                index = (short) Integer.parseInt(indexStr);
            }
        } catch (NumberFormatException nfe) {
            LOG.warning("Invalid Index attribute \"" + indexStr + "\"");
            return null;
        }

        int length;
        String lengthStr = record.getAttributeValue("Length");
        if (lengthStr == null) {
            LOG.warning("Missing Length attribute for <RecordDataItem>");
            return null;
        }

        try {
            if (lengthStr.startsWith("0x") || lengthStr.startsWith("0X")) {
                lengthStr = lengthStr.substring(2);
                length = Integer.parseInt(lengthStr, 16);
            } else {
                length = Integer.parseInt(lengthStr);
            }
        } catch (NumberFormatException nfe) {
            LOG.warning("Invalid Length attribute \"" + lengthStr + "\"");
            return null;
        }

        if (length < 0) {
            LOG.warning("Invalid Length " + length);
            return null;
        }

        ByteBuffer defaultData = ByteBuffer.allocate(length);
        defaultData.order(ByteOrder.BIG_ENDIAN);

        List<ParameterItem> items = new ArrayList<ParameterItem>();

        for (Element c : (List<Element>) record.getChildren("Const",
                DEVICE_PROFILE)) {
            int byteOffset;
            String byteOffsetStr = c.getAttributeValue("ByteOffet", "0");
            try {
                if (byteOffsetStr.startsWith("0x")
                        || byteOffsetStr.startsWith("0X")) {
                    byteOffsetStr = byteOffsetStr.substring(2);
                    byteOffset = Integer.parseInt(byteOffsetStr, 16);
                } else {
                    byteOffset = Integer.parseInt(byteOffsetStr);
                }
            } catch (NumberFormatException nfe) {
                LOG.warning("Invalid ByteOffset attribute \"" + byteOffsetStr
                        + "\"");
                return null;
            }

            if (byteOffset < 0 || byteOffset > length) {
                LOG.warning("Invalid ByteOffset " + byteOffset);
                return null;
            }

            defaultData.position(byteOffset);

            fillData(defaultData, c.getAttributeValue("Data", ""));
        }

        for (Element r : (List<Element>) record.getChildren("Ref",
                DEVICE_PROFILE)) {

            ParameterItem item = getParameterItem(defaultData, r);
            if (item != null) {
                items.add(item);
            }
        }

        return new ParameterRecord(index, name, defaultData.array(), items);
    }

    private ParameterItem getParameterItem(ByteBuffer defaultData, Element ref) {

        Text name = getText(ref.getAttributeValue("TextId"));
        if (name == null) {
            return null;
        }

        final int byteOffset;
        {
            String byteOffsetStr = ref.getAttributeValue("ByteOffset");
            try {
                byteOffset = Integer.parseInt(byteOffsetStr);
            } catch (NumberFormatException e) {
                LOG.warning("Illegal ByteOffset \"" + byteOffsetStr + "\"");
                return null;
            }
        }

        int bitOffset;
        {
            String bitOffsetStr = ref.getAttributeValue("BitOffset", "0");
            try {
                bitOffset = Integer.parseInt(bitOffsetStr);
            } catch (NumberFormatException e) {
                LOG.warning("Illegal BitOffset \"" + bitOffsetStr + "\"");
                return null;
            }
        }

        int bitLength;
        {
            String bitLengthStr = ref.getAttributeValue("BitLength", "1");
            try {
                bitLength = Integer.parseInt(bitLengthStr);
            } catch (NumberFormatException e) {
                LOG.warning("Illegal BitLength \"" + bitLengthStr + "\"");
                return null;
            }
        }

        String dataType = ref.getAttributeValue("DataType");
        if (dataType == null) {
            LOG.warning("Missing DataType for parameter \"" + name + "\"");
            return null;
        }

        ValueItem values = null;

        String valueItemTarget = ref.getAttributeValue("ValueItemTarget");
        if (valueItemTarget != null) {
            values = this.values.get(valueItemTarget);
            if (values == null) {
                LOG.warning("Unknown ValueItem \"" + valueItemTarget + "\"");
                return null;
            }
        }

        long defaultValue;
        {
            String defaultValueStr = ref.getAttributeValue("DefaultValue");
            if (defaultValueStr == null) {
                LOG.warning("Missing DefaultValue for parameter \"" + name
                        + "\"");
                return null;
            }
            try {
                defaultValue = new BigInteger(defaultValueStr).longValue();
                ;
            } catch (NumberFormatException e) {
                LOG.warning("Illegal DefaultValue \"" + defaultValueStr + "\"");
                return null;
            }
        }

        // TODO: AllowedValues, Changeable, Visible

        boolean bitArea = false;

        DataType type;
        if (dataType.equalsIgnoreCase("Bit")) {
            bitArea = true;

            if (bitOffset < 0 || bitOffset > 7) {
                LOG.warning("Illegal BitOffset");
                return null;
            }

            bitLength = 1;

            type = DataType.UNSIGNED8;

        } else if (dataType.equalsIgnoreCase("BitArea")) {
            bitArea = true;

            if (bitOffset < 0 || bitOffset > 7) {
                LOG.warning("Illegal BitOffset");
                return null;
            }

            // The standard says 1 to 15
            if (bitLength < 1 || bitLength > 16) {
                LOG.warning("Illegal BitLength");
                return null;
            }

            if (bitLength <= 8) {
                type = DataType.UNSIGNED8;
            } else {
                type = DataType.UNSIGNED16;
            }
        } else if (dataType.equalsIgnoreCase("Integer8")) {
            type = DataType.INTEGER8;
        } else if (dataType.equalsIgnoreCase("Integer16")) {
            type = DataType.INTEGER16;
        } else if (dataType.equalsIgnoreCase("Integer32")) {
            type = DataType.INTEGER32;
        } else if (dataType.equalsIgnoreCase("Signed64")) {
            type = DataType.INTEGER64;
        } else if (dataType.equalsIgnoreCase("Unsigned8")) {
            type = DataType.UNSIGNED8;
        } else if (dataType.equalsIgnoreCase("Unsigned16")) {
            type = DataType.UNSIGNED16;
        } else if (dataType.equalsIgnoreCase("Unsigned32")) {
            type = DataType.UNSIGNED32;
        } else if (dataType.equalsIgnoreCase("Unsigned64")) {
            type = DataType.UNSIGNED64;
        } else {
            LOG.warning("Unknown data type \"" + dataType + "\"");
            return null;
        }

        if (bitArea) {
            // TODO: why is this block missing code? :) Is it the default data?
        } else {
            bitOffset = 0;
            bitLength = type.getLength() * 8;

            // TODO: Ditto, setting the position doesn't benefit anything
            defaultData.position(byteOffset);
        }

        return new ParameterItem(name, type, byteOffset, bitOffset, bitLength,
                defaultValue, values);
    }

    private void fillData(ByteBuffer buf, String data) {
        String[] parts = data.split(",");
        for (String p : parts) {
            if (p.length() < 2) {
                LOG.warning("Invalid record data \"" + p + "\"");
                buf.put((byte) 0);
            } else {
                buf.put((byte) Integer.parseInt(p.trim().substring(2), 16));
            }
        }
    }

    private Data getIOData(Element data) {
        if (data == null) {
            return null;
        }

        DataConsistency consistency = !"Item consistency".equals(data
                .getAttributeValue("Consistency")) ? DataConsistency.ALL_ITEMS
                : DataConsistency.SINGLE_ITEM;

        List<DataItem> items = new ArrayList<DataItem>();
        for (Element i : (List<Element>) data.getChildren()) {

            Text name = getText(i);
            if (name == null) {
                LOG.warning("Missing name for DataItem");
                return null;
            }

            // We need the DataType attribute to determine the length

            String typeStr = i.getAttributeValue("DataType");
            if (typeStr == null) {
                LOG.warning("Missing DataType attribute");
                return null;
            }

            DataType type = DataType.valueOf(typeStr.toUpperCase());
            if (type == null) {
                LOG.warning("Unsupported DataType \"" + typeStr + "\"");
                return null;
            }

            int length = -1;
            String lengthStr = i.getAttributeValue("Length");
            if (lengthStr != null) {
                try {
                    length = Integer.parseInt(lengthStr);
                } catch (NumberFormatException e) {
                    LOG.warning("Illegal Length \"" + lengthStr + "\"");
                    return null;
                }
            }

            boolean useAsBits = "true".equals(i.getAttributeValue("UseAsBits"));

            items.add(new DataItem(name, type, length, useAsBits));
        }

        return new Data(consistency, items);
    }

    private ModuleInfo getModuleInfo(Element moduleInfo) {
        Text name = getText(moduleInfo.getChild("Name", DEVICE_PROFILE));
        Text infoText = getText(moduleInfo.getChild("InfoText", DEVICE_PROFILE));
        String orderNumber = getValue(moduleInfo.getChild("OrderNumber",
                DEVICE_PROFILE));

        return new ModuleInfo(name, infoText, null, orderNumber, null, null);
    }

    private void importDeviceAccessPoints() throws GSDException {
        daps = new HashSet<DeviceAccessPoint>();

        Element deviceAccessPointList = ap.getChild("DeviceAccessPointList",
                DEVICE_PROFILE);
        if (deviceAccessPointList == null) {
            return;
        }

        for (Element deviceAccessPointItem : (List<Element>) deviceAccessPointList
                .getChildren()) {

            String moduleIdentNumberStr = deviceAccessPointItem
                    .getAttributeValue("ModuleIdentNumber");
            if (moduleIdentNumberStr == null) {
                LOG.warning("Missing ModuleIdentNumber in <DeviceAccessPointItem>");
                continue;
            }

            int moduleIdentNumber;
            try {
                if (moduleIdentNumberStr.startsWith("0x")
                        || moduleIdentNumberStr.startsWith("0X")) {
                    moduleIdentNumberStr = moduleIdentNumberStr.substring(2);
                    moduleIdentNumber = Integer.parseInt(moduleIdentNumberStr,
                            16);
                } else {
                    moduleIdentNumber = Integer.parseInt(moduleIdentNumberStr);
                }
            } catch (NumberFormatException nfe) {
                LOG.warning("Invalid ModuleIdentNumber \""
                        + moduleIdentNumberStr
                        + "\" in in <DeviceAccessPointItem>");
                continue;
            }

            Element moduleInfoElement = deviceAccessPointItem.getChild(
                    "ModuleInfo", DEVICE_PROFILE);
            if (moduleInfoElement == null) {
                LOG.warning("Missing module info in <DeviceAccessPointItem>");
                continue;
            }

            ModuleInfo moduleInfo = getModuleInfo(moduleInfoElement);

            Element virtualSubmoduleList = deviceAccessPointItem.getChild(
                    "VirtualSubmoduleList", DEVICE_PROFILE);
            if (virtualSubmoduleList == null) {
                LOG.warning("Missing <VirtualSubmoduleList> in <DeviceAccessPointItem>");
                continue;
            }

            List<Submodule> submodules = getSubmodules(virtualSubmoduleList);

            ValueList physicalSlots = getSlots(deviceAccessPointItem
                    .getAttributeValue("PhysicalSlots", "0"));
            if (physicalSlots == null) {
                LOG.warning("Invalid PhysicalSlots in <DeviceAccessPointItem>");
                continue;
            }

            Map<Integer, Module> fixedModules = new HashMap<Integer, Module>();

            ValueList fixedInSlots;
            fixedInSlots = getSlots(deviceAccessPointItem
                    .getAttributeValue("FixedInSlots"));
            if (fixedInSlots == null) {
                LOG.warning("Missing FixedInSlots attribute. Currently only FixedInSlots is supported for <DeviceAccessPointItem>");
            }
            if (fixedInSlots.count() != 1) {
                LOG.warning("Only one fixed slot is supported for a DeviceAccessPoint");
            }

            String objectIndexStr = deviceAccessPointItem
                    .getAttributeValue("ObjectUUID_LocalIndex");
            if (objectIndexStr == null) {
                LOG.warning("Missing ObjectUUID_LocalIndex in <DeviceAccessPointItem>");
                continue;
            }

            short objectIndex;
            try {
                if (objectIndexStr.startsWith("0x")
                        || objectIndexStr.startsWith("0X")) {
                    objectIndexStr = moduleIdentNumberStr.substring(2);
                    objectIndex = (short) Integer.parseInt(
                            moduleIdentNumberStr, 16);
                } else {
                    objectIndex = (short) Integer
                            .parseInt(moduleIdentNumberStr);
                }
            } catch (NumberFormatException nfe) {
                LOG.warning("Invalid ObjectUUID_LocalIndex \"" + objectIndexStr
                        + "\" in in <DeviceAccessPointItem>");
                continue;
            }

            Set<UseableModule> useableModules = getUseableModules(
                    deviceAccessPointItem.getChild("UseableModules",
                            DEVICE_PROFILE), physicalSlots, fixedModules);

            DeviceAccessPoint dap = new DeviceAccessPoint(vendorId, deviceId,
                    moduleIdentNumber, moduleInfo, submodules, physicalSlots,
                    fixedInSlots.iterator().next(), objectIndex,
                    useableModules, fixedModules);

            daps.add(dap);
        }
    }

    private Set<UseableModule> getUseableModules(Element useableModules,
            ValueList physicalSlots, Map<Integer, Module> fixedModules) {
        Set<UseableModule> modules = new HashSet<UseableModule>();
        if (useableModules == null) {
            return modules;
        }

        for (Element moduleItemRef : (List<Element>) useableModules
                .getChildren()) {

            String moduleItemTarget = moduleItemRef
                    .getAttributeValue("ModuleItemTarget");
            if (moduleItemTarget == null) {
                LOG.warning("Missing ModuleItemTarget in <ModuleItemRef>");
                continue;
            }

            Module module = idToModules.get(moduleItemTarget);
            if (module == null) {
                LOG.warning("Couldn't find module with ID \""
                        + moduleItemTarget + "\"");
                continue;
            }

            ValueList allowedInSlots = getSlots(moduleItemRef
                    .getAttributeValue("AllowedInSlots"));
            if (allowedInSlots == null) {
                allowedInSlots = physicalSlots;
            }

            modules.add(new UseableModule(module, allowedInSlots));

            // Add the fixed modules
            ValueList fixedInSlots = getSlots(moduleItemRef
                    .getAttributeValue("FixedInSlots"));

            if (fixedInSlots != null) {
                for (int slot : fixedInSlots) {
                    fixedModules.put(slot, module);
                }
            }
        }

        return modules;
    }

    private ValueList getSlots(String value) {
        if (value == null) {
            return null;
        }

        try {
            return ValueList.parse(value.trim());
        } catch (IllegalArgumentException iae) {
            LOG.warning("Invalid slot list \"" + value + "\"");
            return null;
        }
    }

    private GSD run() throws GSDException {
        Element root = doc.getRootElement();
        if (!root.getName().equals("ISO15745Profile")) {
            throw new GSDException(
                    "Expected a <ISO15745Profile> root element but got <"
                            + root.getName() + ">");
        }

        Element body = root.getChild("ProfileBody", DEVICE_PROFILE);
        if (body == null
                || (ap = body.getChild("ApplicationProcess", DEVICE_PROFILE)) == null) {
            throw new GSDException("Missing <ApplicationProcess> element");
        }

        setProgress(20);
        importTextList();
        setProgress(30);
        importDeviceInfo();
        setProgress(40);
        importValues();
        setProgress(50);
        importModules();
        setProgress(80);
        importDeviceAccessPoints();
        setProgress(100);

        return new GSD(vendorId, deviceId, vendorName, infoText, daps, modules);
    }
}
