/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

public class DataItem {

    private final Text name;
    private final DataType type;
    private final int length;
    private final boolean useAsBits;

    public DataItem(Text name, DataType type, int length, boolean useAsBits) {
        if (name == null || type == null) {
            throw new NullPointerException();
        }

        this.name = name;
        this.type = type;

        if (length == -1) {
            length = type.getLength();

            if (length == -1) {
                throw new IllegalArgumentException(
                        "Missing length for data type");
            }
        }

        if (type.getLength() != -1 && type.getLength() != length) {
            throw new IllegalArgumentException(
                    "Length doesn't match data type length");
        }

        this.length = length;
        this.useAsBits = useAsBits;
    }

    public Text getName() {
        return name;
    }

    public DataType getType() {
        return type;
    }

    public int getLength() {
        return length;
    }

    public boolean isUseAsBits() {
        return useAsBits;
    }

    /**
     * Creates a string representation of the given image using this data item
     * representation.
     * 
     * @param image
     *            data image
     * @return a formatted string
     * 
     * @throws BufferUnderflowException
     *             if the image length is less than the length of this
     *             representation
     */
    public String format(ByteBuffer image) {
        StringBuilder str = new StringBuilder();

        str.append(getName());
        str.append(": ");

        if (useAsBits) {
            for (int l = 0; l < length; l++) {
                int octet = image.get() & 0xff;

                // TODO: This is LSB first, which is not very intuitive
                for (int b = 0; b < 8; b++) {
                    str.append(((octet & (1 << b)) != 0) ? '1' : '0');
                }
            }
        } else {
            ByteBuffer slice = image.slice();
            slice.limit(getLength());

            str.append(type.format(slice));
        }

        return str.toString();
    }
}
