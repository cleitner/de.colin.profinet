/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ParameterRecord implements Iterable<ParameterItem> {

    private short index;

    private final Text name;

    private final ByteBuffer defaultData;

    private final List<ParameterItem> items;

    public ParameterRecord(short index, Text name, byte[] defaultData,
            List<ParameterItem> items) {

        this.index = index;

        if (name == null) {
            throw new NullPointerException();
        }
        this.name = name;

        this.defaultData = ByteBuffer.wrap(
                Arrays.copyOf(defaultData, defaultData.length))
                .asReadOnlyBuffer();

        this.items = Collections.unmodifiableList(new ArrayList<ParameterItem>(
                items));
    }

    public short getIndex() {
        return index;
    }

    public Text getName() {
        return name;
    }

    public int getLength() {
        return defaultData.capacity();
    }

    public ByteBuffer getDefaultData() {
        return defaultData.slice();
    }

    public List<ParameterItem> getItems() {
        return items;
    }

    @Override
    public Iterator<ParameterItem> iterator() {
        return items.iterator();
    }

    /**
     * Returns the first parameter item with the given name.
     */
    public ParameterItem getItem(String name) {
        for (ParameterItem i : items) {
            if (i.getName().toString().equals(name)) {
                return i;
            }
        }

        return null;
    }

    /**
     * Sets the value of a record item.
     * 
     * @param data
     *            the data on which to set the item
     * @param item
     *            the item to set
     * @param value
     *            the named value to set
     * 
     * @throws IllegalArgumentException
     *             if item or name are not valid
     */
    public void set(ByteBuffer data, String item, String value) {
        ParameterItem pitem = getItem(item);

        if (pitem == null) {
            throw new IllegalArgumentException("Unknown record item \"" + item
                    + "\"");
        }

        pitem.set(data, value);
    }

    public String toString(ByteBuffer data) {
        StringBuilder str = new StringBuilder();

        str.append(name);

        str.append(" {");
        boolean first = true;
        for (ParameterItem i : items) {
            if (first) {
                first = false;
            } else {
                str.append(", ");
            }
            str.append(i);
        }
        str.append("} = ");
        str.append(Utils.formatByteBuffer(data));

        return str.toString();
    }

    @Override
    public String toString() {
        return toString(getDefaultData());
    }
}
