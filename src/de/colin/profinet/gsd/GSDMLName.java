/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A representation of a GSDML file name
 */
public class GSDMLName {

    public static final int NO_VENDOR_ID = -1;

    // GSDML-[GSDML Schemaversion]-[Herstellername]-[Name der
    // Gerätefamilie]-[Datum].xml
    private static Pattern NAME_PATTERN = Pattern
            .compile("GSDML-[vV](\\d+)\\.(\\d+)-([^-]+)(?:-([0-9a-fA-F]{4}))?-(.+?)-(\\d{8})(?:\\.xml)?");

    private final int versionMajor;
    private final int versionMinor;
    private final String vendor;
    private final int vendorId;
    private final String device;
    private final int date;

    public static GSDMLName fromFilename(String fileName) {
        Matcher m = NAME_PATTERN.matcher(fileName);
        if (!m.matches()) {
            throw new IllegalArgumentException("\"" + fileName
                    + "\" is not a valid GSDML filename");
        }

        int major = Integer.parseInt(m.group(1));
        int minor = Integer.parseInt(m.group(2));

        String vendor = m.group(3);

        int vendorId = NO_VENDOR_ID;
        if (m.group(4) != null) {
            vendorId = Integer.parseInt(m.group(4), 16);
        }

        String device = m.group(5);

        int date = Integer.parseInt(m.group(6));

        return new GSDMLName(major, minor, vendor, vendorId, device, date);
    }

    public GSDMLName(int versionMajor, int versionMinor, String vendor,
            int vendorId, String device, int date) {
        super();
        this.versionMajor = versionMajor;
        this.versionMinor = versionMinor;
        this.vendor = vendor;
        this.vendorId = vendorId;
        this.device = device;
        this.date = date;
    }

    public int getVersionMajor() {
        return versionMajor;
    }

    public int getVersionMinor() {
        return versionMinor;
    }

    public String getVendor() {
        return vendor;
    }

    public int getVendorId() {
        return vendorId;
    }

    public String getDevice() {
        return device;
    }

    public int getDate() {
        return date;
    }

    public String toString() {
        StringBuilder str = new StringBuilder();

        str.append("GSDML-V");
        str.append(getVersionMajor());
        str.append(".");
        str.append(getVersionMinor());
        str.append("-");
        str.append(vendor);
        if (vendorId != NO_VENDOR_ID) {
            str.append("-");
            str.append(String.format("%04X", vendorId));
        }
        str.append("-");
        str.append(device);
        str.append("-");
        str.append(date);
        str.append(".xml");

        return str.toString();
    }
}
