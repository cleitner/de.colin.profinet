/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A value list contains signed values and value ranges.
 */
public class ValueList implements Iterable<Integer> {

    private static final Pattern VALUE_PATTERN = Pattern
            .compile("((-?\\d+\\.\\.-?\\d+)|(-?\\d+))((\\s+-?\\d+\\.\\.-?\\d+)|(\\s+-?\\d+))*");

    private static class Range {
        Range next;

        int begin;
        int end;

        Range(int begin, int end) {
            this.begin = begin;
            this.end = end;
        }

        boolean contains(int value) {
            return begin <= value && end >= value;
        }
    }

    private static class UnmodifiableValueList extends ValueList {
        @Override
        public void addRange(int begin, int end) {
            throw new UnsupportedOperationException();
        }
    }

    private static class ValueListIterator implements Iterator<Integer> {
        private Range range;
        private int nextValue;

        public ValueListIterator(Range head) {
            this.range = head;
            if (head != null) {
                nextValue = head.begin;
            }
        }

        @Override
        public boolean hasNext() {
            return range != null;
        }

        @Override
        public Integer next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            int v = nextValue;

            nextValue += 1;
            if (nextValue > range.end) {
                range = range.next;
                if (range != null) {
                    nextValue = range.begin;
                }
            }

            return v;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    public static ValueList parse(String values) {
        values = values.trim();

        if (values.isEmpty()) {
            return new ValueList();
        }

        Matcher m = VALUE_PATTERN.matcher(values);
        if (!m.matches()) {
            throw new IllegalArgumentException();
        }

        String[] parts = values.split("\\s+");

        ValueList list = new ValueList();

        for (String p : parts) {
            if (!p.contains("..")) {
                list.addValue(Integer.parseInt(p));
            } else {
                String[] rangeParts = p.split("\\.\\.");

                int begin = Integer.parseInt(rangeParts[0]);
                int end = Integer.parseInt(rangeParts[1]);

                if (begin > end) {
                    int swap = begin;
                    begin = end;
                    end = swap;
                }

                list.addRange(begin, end);
            }
        }

        return list;
    }

    private Range head;

    protected Range getHead() {
        return head;
    }

    protected void setHead(Range r) {
        head = r;
    }

    private void copyInto(ValueList list) {
        Range last = null;
        Range r = getHead();
        while (r != null) {
            Range copy = new Range(r.begin, r.end);
            if (last != null) {
                last.next = copy;
            }

            if (list.getHead() == null) {
                list.setHead(copy);
            }

            last = copy;

            r = r.next;
        }
    }

    /**
     * Returns an unmodifiable copy of this value list.
     */
    public ValueList unmodifiableCopy() {
        ValueList vl = new UnmodifiableValueList();

        copyInto(vl);

        return vl;
    }

    public ValueList copy() {
        ValueList vl = new ValueList();

        copyInto(vl);

        return vl;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new ValueListIterator(getHead());
    }

    public int count() {
        int count = 0;

        Range r = getHead();
        while (r != null) {
            count += r.end - r.begin + 1;

            r = r.next;
        }

        return count;
    }

    public boolean contains(int value) {
        Range r = getHead();
        while (r != null) {

            if (r.contains(value)) {
                return true;
            }

            r = r.next;
        }

        return false;
    }

    public void addValue(int n) {
        addRange(n, n);
    }

    public void addRange(int begin, int end) {
        if (begin > end) {
            throw new IllegalArgumentException();
        }

        if (isEmpty()) {
            setHead(new Range(begin, end));
            return;
        }

        // Find the Range before the new range and ditto for the following
        Range before = null, after = null;

        Range r = getHead();
        while (r != null) {
            if (r.begin < begin) {
                // Already included
                if (r.end >= end) {
                    return;
                }

                before = r;
            }

            if (after == null && r.end >= end) {
                after = r;
            }

            r = r.next;
        }

        if (before == null) {
            if (after != null) {
                if (after.begin <= end + 1) {
                    after.begin = begin;
                    setHead(after);
                    return;
                }
            }

            setHead(new Range(begin, end));
            getHead().next = after;
        } else if (after == null) {
            if (begin <= before.end + 1) {
                before.end = end;
                before.next = null;
            } else {
                Range range = new Range(begin, end);
                before.next = range;
            }
        } else {
            // In between, possibly overlapping

            // Close the gap. We'll change this if necessary
            before.next = after;

            if (before.end >= begin - 1) {
                before.end = end;

                if (before.end >= after.begin - 1) {
                    before.end = after.end;
                    before.next = after.next;
                }
            } else if (after.begin <= end + 1) {
                after.begin = begin;
            } else {
                Range range = new Range(begin, end);
                before.next = range;
                range.next = after;
            }
        }
    }

    public boolean isEmpty() {
        return getHead() == null;
    }

    @Override
    public String toString() {
        if (isEmpty()) {
            return "";
        }

        StringBuilder str = new StringBuilder();

        boolean first = true;
        Range r = getHead();
        while (r != null) {
            if (first) {
                first = false;
            } else {
                str.append(" ");
            }

            str.append(r.begin);
            if (r.end != r.begin) {
                str.append("..");
                str.append(r.end);
            }

            r = r.next;
        }

        return str.toString();
    }
}
