/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Diagnostics {

    private final Map<Integer, DiagnosticMessage> messages = new HashMap<Integer, DiagnosticMessage>();

    public void clear() {
        messages.clear();
    }

    public Set<Integer> getDiagnosticIds() {
        return Collections.unmodifiableSet(messages.keySet());
    }

    public void setMessage(int id, Text message, Text help) {
        setMessage(id, new DiagnosticMessage(message, help));
    }

    public void setMessage(int id, DiagnosticMessage message) {
        if (message == null) {
            throw new NullPointerException();
        }

        messages.put(id, message);
    }

    public DiagnosticMessage getMessage(int id) {
        return messages.get(id);
    }

}
