/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class GSD {

    private static final Logger LOG = Logger.getLogger(GSD.class.getName());

    private final short vendorId;
    private final short deviceId;
    private final String vendorName;
    private final Text infoText;

    private final Set<DeviceAccessPoint> daps;
    private final Set<Module> modules;

    private final Map<String, DeviceAccessPoint> nameToDap;
    private final Map<Integer, DeviceAccessPoint> idToDap;

    public GSD(short vendorId, short deviceId, String vendorName,
            Text infoText, Set<DeviceAccessPoint> daps, Set<Module> modules) {

        if (vendorName == null || infoText == null) {
            throw new NullPointerException();
        }

        this.vendorId = vendorId;
        this.deviceId = deviceId;
        this.vendorName = vendorName;
        this.infoText = infoText;

        this.daps = Collections.unmodifiableSet(new HashSet<DeviceAccessPoint>(
                daps));
        this.modules = Collections
                .unmodifiableSet(new HashSet<Module>(modules));

        nameToDap = new HashMap<String, DeviceAccessPoint>();
        idToDap = new HashMap<Integer, DeviceAccessPoint>();

        for (DeviceAccessPoint d : this.daps) {
            DeviceAccessPoint old;

            old = nameToDap.put(d.getName().toString(), d);
            if (old != null) {
                LOG.warning("Ambiguous DeviceAccessPoint name \""
                        + d.getName().toString() + "\"");
            }

            old = idToDap.put(d.getModuleIdentNumber(), d);
            if (old != null) {
                LOG.warning(String.format(
                        "Ambiguous Module ident number 0x%08X",
                        d.getModuleIdentNumber()));
            }
        }
    }

    public short getVendorId() {
        return vendorId;
    }

    public short getDeviceId() {
        return deviceId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public Text getInfoText() {
        return infoText;
    }

    public Set<DeviceAccessPoint> getDeviceAccessPoints() {
        return daps;
    }

    public Set<Module> getModules() {
        return modules;
    }

    public DeviceAccessPoint getDeviceAccessPoint(String name) {
        return nameToDap.get(name);
    }

    public DeviceAccessPoint getDeviceAccessPoint(int moduleIdentNumber) {
        return idToDap.get(moduleIdentNumber);
    }
}
