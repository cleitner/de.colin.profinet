/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.nio.ByteBuffer;
import java.util.List;

public class ParameterItem {

    private final Text name;

    private final DataType type;

    private final int byteOffset;
    private final int bitOffset;
    private final int bitLength;

    private final long defaultValue;

    private final ValueItem values;

    public ParameterItem(Text name, DataType type, int byteOffset,
            int bitOffset, int bitLength, long defaultValue, ValueItem values) {

        this.name = name;
        this.type = type;
        this.byteOffset = byteOffset;
        this.bitOffset = bitOffset;
        this.bitLength = bitLength;
        this.defaultValue = defaultValue;
        this.values = values;
    }

    public Text getName() {
        return name;
    }

    public DataType getType() {
        return type;
    }

    public int getByteOffset() {
        return byteOffset;
    }

    public int getBitOffset() {
        return bitOffset;
    }

    public int getBitLength() {
        return bitLength;
    }

    public long getDefaultValue() {
        return defaultValue;
    }

    public Text getHelp() {
        if (values == null) {
            return null;
        }

        return values.getHelp();
    }

    public List<Value> getValues() {
        if (values == null) {
            return null;
        }

        return values.getValues();
    }

    public long getValue(String text) {
        if (values == null) {
            throw new IllegalArgumentException(
                    "Parameter item has no named values");
        }

        return values.getValue(text);
    }

    public void set(ByteBuffer params, String namedValue) {
        final long value = getValue(namedValue) << bitOffset;
        final int mask = ((1 << bitLength) - 1) << bitOffset;

        params.position(byteOffset);
        for (int bits = 0; bits < (bitOffset + bitLength); bits += 8) {
            if ((bits + 8) < bitOffset) {
                continue;
            }

            byte p = params.get();
            params.position(params.position() - 1);

            p &= (byte) ((~mask) >> bits);
            p |= (byte) (value >> bits);

            params.put(p);
        }
    }

    @Override
    public String toString() {
        return name.toString();
    }
}
