/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Data {

    private final DataConsistency consistency;
    private final List<DataItem> items;
    private final int length;

    public Data(DataConsistency consistency, List<DataItem> items) {
        if (consistency == null) {
            throw new NullPointerException();
        }

        this.consistency = consistency;

        this.items = Collections
                .unmodifiableList(new ArrayList<DataItem>(items));

        int len = 0;
        for (DataItem i : this.items) {
            len += i.getLength();
        }

        length = len;
    }

    public DataConsistency getConsistency() {
        return consistency;
    }

    public List<DataItem> getItems() {
        return items;
    }

    public int getLength() {
        return length;
    }

    /**
     * Creates a string representation of the given image using this data
     * representation.
     * 
     * @param image
     *            data image. Data will be interpreted in big endian order
     * @return a formatted string
     * 
     * @throws BufferUnderflowException
     *             if the image length is less than the length of this
     *             representation
     */
    public String format(byte[] image) {
        return format(ByteBuffer.wrap(image).order(ByteOrder.BIG_ENDIAN));
    }

    /**
     * Creates a string representation of the given image using this data
     * representation.
     * 
     * @param image
     *            data image. Should be set to {@code BIG_ENDIAN}
     * @return a formatted string
     * 
     * @throws BufferUnderflowException
     *             if the image length is less than the length of this
     *             representation
     */
    public String format(ByteBuffer image) {
        StringBuilder str = new StringBuilder();

        for (DataItem i : items) {
            str.append(i.format(image));
            str.append("\n");
        }

        return str.toString();
    }
}
