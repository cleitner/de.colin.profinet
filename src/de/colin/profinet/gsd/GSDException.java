/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

public class GSDException extends Exception {

    private static final long serialVersionUID = -3321094083199245175L;

    public GSDException() {
        super();
    }

    public GSDException(String message, Throwable cause) {
        super(message, cause);
    }

    public GSDException(String message) {
        super(message);
    }

    public GSDException(Throwable cause) {
        super(cause);
    }

}
