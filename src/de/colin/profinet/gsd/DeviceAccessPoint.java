/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class DeviceAccessPoint extends Module {

    private static final Logger LOG = Logger.getLogger(DeviceAccessPoint.class
            .getName());

    // Duplicates from the GSD instance
    private final short vendorId;
    private final short deviceId;

    private final ValueList physicalSlots;
    // No support for useableSlots
    private final int slot;

    private final short objectIndex;

    private final Set<UseableModule> useableModules;

    private final Map<String, UseableModule> nameToUseableModule;
    private final Map<Integer, UseableModule> idToUseableModule;
    private final Map<Integer, Module> fixedModules;

    public DeviceAccessPoint(short vendorId, short deviceId,
            int moduleIdentNumber, ModuleInfo info, List<Submodule> submodules,
            ValueList physicalSlots, int slot, short objectIndex,
            Set<UseableModule> useableModules, Map<Integer, Module> fixedModules) {
        super(moduleIdentNumber, info, submodules);

        this.vendorId = vendorId;
        this.deviceId = deviceId;

        this.physicalSlots = physicalSlots.unmodifiableCopy();
        this.slot = slot;
        this.objectIndex = objectIndex;

        this.useableModules = Collections
                .unmodifiableSet(new HashSet<UseableModule>(useableModules));

        nameToUseableModule = new HashMap<String, UseableModule>();
        idToUseableModule = new HashMap<Integer, UseableModule>();

        for (UseableModule m : this.useableModules) {
            UseableModule old;

            old = nameToUseableModule
                    .put(m.getModule().getName().toString(), m);
            if (old != null) {
                LOG.warning("Ambiguous module name \""
                        + m.getModule().getName().toString() + "\"");
            }

            old = idToUseableModule
                    .put(m.getModule().getModuleIdentNumber(), m);
            if (old != null) {
                LOG.warning(String.format(
                        "Ambiguous Module ident number 0x%08X", m.getModule()
                                .getModuleIdentNumber()));
            }
        }

        Map<Integer, Module> completeFixedModules = new HashMap<Integer, Module>(
                fixedModules);
        completeFixedModules.put(slot, this);
        this.fixedModules = Collections.unmodifiableMap(completeFixedModules);
    }

    public short getVendorId() {
        return vendorId;
    }

    public short getDeviceId() {
        return deviceId;
    }

    /**
     * Returns the range of available physical slots in the device if used with
     * this DeviceAccessPoint.
     */
    public ValueList getPhysicalSlots() {
        return physicalSlots;
    }

    public int getSlot() {
        return slot;
    }

    public short getObjectIndex() {
        return objectIndex;
    }

    /**
     * Returns all useable modules for this DeviceAccessPoint.
     */
    public Set<UseableModule> getUseableModules() {
        return useableModules;
    }

    public UseableModule getUseableModule(String name) {
        return nameToUseableModule.get(name);
    }

    public UseableModule getUseableModule(int moduleIdentNumber) {
        return idToUseableModule.get(moduleIdentNumber);
    }

    /**
     * Returns the map of fixed modules, which can't be removed.
     */
    public Map<Integer, Module> getFixedModules() {
        return fixedModules;
    }

}
