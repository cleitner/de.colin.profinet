/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Text {

    private final String primary;
    private final Map<String, String> translations;

    public Text(String text) {
        this(text, null);
    }

    public Text(String primary, Map<String, String> translations) {
        if (primary == null) {
            throw new NullPointerException();
        }

        this.primary = primary;

        if (translations != null && translations.isEmpty()) {
            translations = null;
        }

        if (translations == null) {
            this.translations = null;
        } else {
            this.translations = new HashMap<String, String>(translations);
        }
    }

    public String toLocalizedString(String lang) {
        if (translations == null) {
            return primary;
        }

        String text = translations.get(lang);
        if (text == null) {
            return primary;
        }

        return text;
    }

    public String toLocalizedString() {
        return toLocalizedString(Locale.getDefault().getCountry().toLowerCase());
    }

    public String toString() {
        return primary;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((primary == null) ? 0 : primary.hashCode());
        result = prime * result
                + ((translations == null) ? 0 : translations.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Text other = (Text) obj;
        if (primary == null) {
            if (other.primary != null)
                return false;
        } else if (!primary.equals(other.primary))
            return false;
        if (translations == null) {
            if (other.translations != null)
                return false;
        } else if (!translations.equals(other.translations))
            return false;
        return true;
    }
}
