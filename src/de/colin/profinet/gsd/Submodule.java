/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Submodule {

    private final int submoduleIdentNumber;
    private final ModuleInfo moduleInfo;

    private final Data inputData;
    private final Data outputData;

    private final List<ParameterRecord> parameters;

    public Submodule(int submoduleIdentNumber, ModuleInfo info, Data inputData,
            Data outputData, List<ParameterRecord> parameters) {
        if (info == null) {
            throw new NullPointerException();
        }

        this.moduleInfo = info;
        this.submoduleIdentNumber = submoduleIdentNumber;

        this.inputData = inputData;
        this.outputData = outputData;

        this.parameters = Collections
                .unmodifiableList(new ArrayList<ParameterRecord>(parameters));
    }

    public int getSubmoduleIdentNumber() {
        return submoduleIdentNumber;
    }

    public Text getName() {
        return moduleInfo.getName();
    }

    public ModuleInfo getModuleInfo() {
        return moduleInfo;
    }

    public Data getInputData() {
        return inputData;
    }

    public Data getOutputData() {
        return outputData;
    }

    public List<ParameterRecord> getParameterRecords() {
        return parameters;
    }
}
