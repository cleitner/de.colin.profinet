/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.gsd;

import java.nio.ByteBuffer;

public class Utils {

    public static String formatByteBuffer(ByteBuffer buf) {
        if (!buf.hasRemaining()) {
            return "-";
        }

        StringBuilder str = new StringBuilder();

        while (buf.hasRemaining()) {
            str.append(String.format("%02X", buf.get() & 0xff));
        }

        return str.toString();
    }

}
