/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc;

import de.colin.profinet.ProfinetException;

public class PNIOError extends ProfinetException {

    private static final long serialVersionUID = 8264351239781865432L;

    public static final int CONNECT_RPC_REJECTED = 0xDB814501;
    public static final int RELEASE_RPC_REJECTED = 0xDC814501;
    public static final int CONTROL_RPC_REJECTED = 0xDD814501;
    public static final int READ_RPC_REJECTED = 0xDE814501;
    public static final int WRITE_RPC_REJECTED = 0xDF814501;

    private final int code;

    public PNIOError(int code) {
        super(String.format("PNIO Error: 0x%08x", code));

        this.code = code;
    }

    public int getErrorCode() {
        return code;
    }

    public int getCode() {
        return (code >> 24) & 0xff;
    }

    public int getDecode() {
        return (code >> 16) & 0xff;
    }

    public int getCode1() {
        return (code >> 8) & 0xff;
    }

    public int getCode2() {
        return code & 0xff;
    }

}
