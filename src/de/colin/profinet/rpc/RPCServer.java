/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.DatagramChannel;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.colin.profinet.BufferAdapter;
import de.colin.profinet.ProfinetException;

/**
 * A connection-less DCE/RPC server, limited to the necessary subset to support
 * PN-IO.
 * 
 * {@link RPCClient} is a companion class that implements the client side of the
 * RPC connection. The RPC interface version is always expected to be 1.
 * 
 * This class is meant to be extended in order to handle incoming calls.
 * 
 * @author Colin Leitner
 */
public abstract class RPCServer {

    private static final Logger LOG = Logger.getLogger(RPCServer.class
            .getName());

    private class ReceiveTask implements Runnable {
        private final DatagramChannel channel;

        public ReceiveTask(DatagramChannel channel) {
            this.channel = channel;
        }

        @Override
        public void run() {
            Thread.currentThread().setName("PROFINET RPC Server");
            try {
                receive(channel);
            } catch (IOException e) {
                LOG.log(Level.INFO, "RPC reception failed", e);
            }
        }
    }

    private final ExecutorService executor;
    private final AtomicBoolean running = new AtomicBoolean();
    private DatagramChannel channel;
    private Future<Void> rpcTask;

    /**
     * Creates a new RPC server with an internal thread to handle incoming
     * calls.
     */
    protected RPCServer() {
        this(Executors.newSingleThreadExecutor(new ThreadFactory() {

            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread("DCE/RPC server");
                t.setDaemon(true);
                return t;
            }
        }));
    }

    /**
     * Creates a new RPC server that uses the given executor to start a task
     * that handles all incoming calls.
     */
    protected RPCServer(ExecutorService executor) {
        if (executor == null) {
            throw new NullPointerException();
        }

        this.executor = executor;
    }

    public void start() throws IOException {
        start(new InetSocketAddress(0x8894));
    }

    public synchronized void start(SocketAddress address) throws IOException {
        if (running.get()) {
            throw new IllegalStateException();
        }

        DatagramChannel c;
        try {
            c = DatagramChannel.open();
            c.socket().setReuseAddress(true);
            c.socket().bind(address);
        } catch (IOException e) {
            throw new ProfinetException(e);
        }

        channel = c;

        running.set(true);
        rpcTask = executor.submit(new ReceiveTask(channel), null);
    }

    public synchronized void stop() {
        if (!running.get()) {
            return;
        }

        running.set(false);
        rpcTask.cancel(false);
        try {
            channel.close();
        } catch (IOException e1) {
            // Who cares?
        }

        try {
            rpcTask.get(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            // Ignore
        } catch (TimeoutException e) {
            // Ignore
        } catch (CancellationException e) {
            // Ignore            
        }

        channel = null;
        rpcTask = null;
    }

    private void receive(DatagramChannel c) throws IOException {
        ByteBuffer incoming = ByteBuffer.allocate(1500);
        SocketAddress address;

        while (running.get()) {
            incoming.clear();

            try {
                address = c.receive(incoming);
            } catch (IOException e) {
                if (!c.isOpen() || !running.get()) {
                    return;
                }

                LOG.log(Level.WARNING, "I/O error while receiving RPC request",
                        e);
                return;
            }

            incoming.flip();

            try {
                onReceived(incoming, address);
            } catch (ProfinetException pe) {
                LOG.log(Level.INFO, "Failed RPC request", pe);
            }
        }
    }

    /**
     * Handles a new incoming packet
     */
    protected synchronized void onReceived(ByteBuffer buf, SocketAddress src)
            throws IOException {
        LOG.fine("Received RPC request");

        BufferAdapter req = new BufferAdapter(buf);

        // -- DCE/RPC Request --

        req.order(ByteOrder.LITTLE_ENDIAN);

        if (req.remaining() < DCERPC.HEADER_SIZE) {
            throw new ProfinetException("RPC request is too short");
        }

        // rpc_vers
        if (req.get() != DCERPC.rpc_vers) {
            throw new ProfinetException("Wrong RPC version");
        }

        // ptype
        int ptype = req.get();
        switch (ptype) {
        case DCERPC.request:
            break;
        case DCERPC.cl_cancel:
            break;
        default:
            throw new ProfinetException("Unknown RPC packet type " + ptype);
        }

        // flags1/2
        // TODO: Bail out on fragmentation etc.
        req.get();
        req.get();

        // drep
        if (req.get() != 0x10 || req.get() != 0x00 || req.get() != 0x00) {
            throw new ProfinetException("Unsupported data representation");
        }

        // serial_hi
        req.get();

        UUID object = req.getUUID();
        UUID interface_ = req.getUUID();
        UUID activity = req.getUUID();

        // server_boot
        req.getInt();

        // if_vers
        if (req.getInt() != 1) {
            throw new ProfinetException("Interface version must be 1");
        }

        // seqnum
        int sequence = req.getInt();

        // opnum
        short operation = req.getShort();

        // ihint/ahint
        short ihint = req.getShort();
        short ahint = req.getShort();

        // len
        int reqLen = req.getShort();

        // fragnum
        if (req.getShort() != 0) {
            throw new ProfinetException("Invalid fragment number");
        }

        // auth_proto
        if (req.get() != 0) {
            throw new ProfinetException("RPC auth is not supported");
        }

        // serial_lo
        req.get();

        if (req.remaining() < reqLen) {
            throw new ProfinetException("RPC request is too short");
        }

        // Cut the rest
        req.limit(req.position() + reqLen);

        int argsMax = 0;
        ByteBuffer result = null;
        int status = 0;

        if (ptype == DCERPC.request) {

            if (reqLen < 20) {
                throw new ProfinetException("PN-IO RPC request is too short");
            }

            argsMax = req.getAlignedInt();
            if (argsMax > 1500) {
                throw new ProfinetException(
                        "PN-IO RPC response buffer size is too large");
            }

            // ArgsLength
            int argsLength = req.getAlignedInt();

            // Args (uniform array)
            req.getAlignedInt();
            req.getAlignedInt();
            req.getAlignedInt();

            if (argsLength != req.remaining()) {
                throw new ProfinetException("Invalid PN-IO argument length");
            }

            result = ByteBuffer.allocate(argsMax);
            status = onCall(src, activity, object, interface_, operation, buf,
                    result);
        } else if (ptype == DCERPC.cl_cancel) {
            // Orphan the call by sending no body
        } else {
            throw new AssertionError();
        }

        BufferAdapter rsp = new BufferAdapter(DCERPC.HEADER_SIZE + 20 + argsMax);

        // -- DCE/RPC Response --

        rsp.order(ByteOrder.LITTLE_ENDIAN);

        // rpc_vers
        rsp.put(DCERPC.rpc_vers);

        // ptype
        rsp.put(DCERPC.response);

        // flags1/2
        rsp.put(DCERPC.idempotent);
        rsp.put((byte) 0);

        // drep
        rsp.put((byte) 0x10);
        rsp.put((byte) 0x00);
        rsp.put((byte) 0x00);

        // serial_hi
        rsp.put((byte) 0);

        rsp.putUUID(object);
        rsp.putUUID(interface_);
        rsp.putUUID(activity);

        // server_boot. We don't conform to the standard by sending the running
        // time of the server
        rsp.putInt(0);

        // if_vers. Always 1, although that theoretically could be wrong. Nobody
        // cares
        rsp.putInt(1);

        // seqnum
        rsp.putInt(sequence);

        // opnum
        rsp.putShort(operation);

        // ihint/ahint
        rsp.putShort(ihint);
        rsp.putShort(ahint);

        // len (content + ArgsMax + ArgsLen + ArgsHeader)
        if (ptype == DCERPC.cl_cancel) {
            rsp.putShort((short) 0);
        } else if (status != 0) {
            rsp.putShort((short) 4);
        } else {
            rsp.putShort((short) (20 + result.remaining()));
        }

        // fragnum
        rsp.putShort((short) 0);

        // auth_proto
        rsp.put((byte) 0);

        // serial_lo
        rsp.put((byte) 0);

        // -- PN-IO Response --

        if (ptype != DCERPC.cl_cancel) {
            rsp.putAlignedInt(status);
            if (status == 0) {
                // Result length
                rsp.putAlignedInt(result.remaining());
                // Result (uniform array)
                rsp.putAlignedInt(result.capacity());
                rsp.putAlignedInt(0);
                rsp.putAlignedInt(result.remaining());

                rsp.put(result);
            }
        }

        rsp.flip();

        respond(rsp.buffer(), src);
    }

    /**
     * Sends the outgoing packet
     */
    protected synchronized void respond(ByteBuffer buf, SocketAddress dst) {
        try {
            channel.send(buf, dst);
        } catch (IOException e) {
            if (!channel.isOpen() || !running.get()) {
                return;
            }

            LOG.log(Level.WARNING, "I/O error while sending RPC response", e);
        }
    }

    /**
     * Handles an incoming RPC call
     * 
     * @param address
     *            client address
     * @param object
     * @param interface_
     * @param operation
     * @param args
     * @param response
     * @return PNIO status code. 0 is taken as success
     */
    protected abstract int onCall(SocketAddress address, UUID activity,
            UUID object, UUID interface_, int operation, ByteBuffer args,
            ByteBuffer result) throws RPCReject;
}
