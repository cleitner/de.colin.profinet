/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc;

import java.io.Closeable;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.UUID;

import de.colin.profinet.BufferAdapter;
import de.colin.profinet.ProfinetException;

/**
 * A connection-less DCE/RPC client, limited to the necessary subset to support
 * PN-IO.
 * 
 * @author Colin Leitner
 */
public class RPCClient implements Closeable {

    private static final int MAX_ARGS = 1043;

    private DatagramSocket socket;
    private final UUID activity = UUID.randomUUID();
    private int sequence;

    public RPCClient() throws IOException {
        try {
            socket = new DatagramSocket();
            socket.setSoTimeout(100);
        } catch (IOException e) {
            throw new ProfinetException(e);
        }
    }

    @Override
    public void close() {
        socket.close();
    }

    /**
     * Executes a connection-less RPC call on the remote server
     * 
     * @param address
     *            UDP address of the server
     * @param object
     *            object UUID
     * @param interface_
     *            interface UUID
     * @param operation
     *            operation index
     * @param args
     *            PNIO arguments
     * 
     * @throws PNIOError
     *             if the PNIO call failed
     * @throws ProfinetException
     *             on any RPC or I/O errors
     */
    public synchronized void call(SocketAddress address, UUID object,
            UUID interface_, short operation, ByteBuffer args, ByteBuffer result)
            throws IOException {

        sequence += 1;

        BufferAdapter req = new BufferAdapter(DCERPC.HEADER_SIZE + 20
                + args.remaining());

        BufferAdapter rsp = new BufferAdapter(DCERPC.HEADER_SIZE + 16
                + MAX_ARGS);

        // -- DCE/RPC Request --

        req.order(ByteOrder.LITTLE_ENDIAN);

        // rpc_vers
        req.put(DCERPC.rpc_vers);

        // ptype
        req.put(DCERPC.request);

        // flags1/2
        req.put(DCERPC.idempotent);
        req.put((byte) 0);

        // drep
        req.put((byte) 0x10);
        req.put((byte) 0x00);
        req.put((byte) 0x00);

        // serial_hi
        req.put((byte) 0);

        req.putUUID(object);
        req.putUUID(interface_);
        req.putUUID(activity);

        // server_boot. We don't conform to the standard by reflecting the
        // boot-time, but that shouldn't matter as all operations are supposed
        // to be idempotent (which they probably aren't)
        req.putInt(0);

        // if_vers. Always 1, although that theoretically could be wrong. Nobody
        // cares
        req.putInt(1);

        // seqnum
        req.putInt(sequence);

        // opnum
        req.putShort(operation);

        // ihint/ahint
        req.putShort((short) 0);
        req.putShort((short) 0);

        // len (content + ArgsMax + ArgsLen + ArgsHeader)
        req.putShort((short) (20 + args.remaining()));

        // fragnum
        req.putShort((short) 0);

        // auth_proto
        req.put((byte) 0);

        // serial_lo
        req.put((byte) 0);

        // -- PN-IO Request --

        // ArgsMax (Size of response(!) buffer)
        req.putAlignedInt(Math.max(MAX_ARGS, result.remaining()));

        // ArgsLength
        req.putAlignedInt(args.remaining());

        // Args (uniform array)
        // TODO: Check the reason for this
        // req.putAlignedInt(args.capacity());
        req.putAlignedInt(Math.max(MAX_ARGS, args.capacity()));
        req.putAlignedInt(0);
        req.putAlignedInt(args.remaining());

        req.put(args);

        req.flip();

        // Done with our request buffer. Lets see what the server says

        try {
            // This is an ugly hack to establish a connection through a Firewall
            // on the PC. The FB33/34 and probably all PROFINET devices based on
            // the Siemens stack use two sockets internally - one for reception,
            // one for transmission
            //
            // This will obviously not fly well with any Firewall because your
            // response is sent from a different endpoint.
            //
            // We can guess that the return port will always be the same (49161
            // observed) and punch a hole for that before sending our request.
            //
            // This makes two assumptions:
            //
            //   1. the UDP port will always stay the same - this is quite
            //      likely as the devices we're talking to are not dynamic in
            //      any way and the port is most likely not really chosen
            //      randomly.
            //   2. the device will forgive us sending garbage to the tx-only
            //      endpoint
            socket.send(new DatagramPacket(new byte[0], 0,
                    ((InetSocketAddress) address).getAddress(), 49161));
            socket.send(new DatagramPacket(new byte[0], 0,
                    ((InetSocketAddress) address).getAddress(), 49162));
            
            socket.send(new DatagramPacket(req.buffer().array(), req.buffer()
                    .arrayOffset() + req.buffer().position(), req.buffer()
                    .remaining(), address));

            DatagramPacket responsePacket = new DatagramPacket(rsp.buffer()
                    .array(), rsp.buffer().arrayOffset()
                    + rsp.buffer().position(), rsp.buffer().remaining());
            socket.receive(responsePacket);
            rsp.position(rsp.position() + responsePacket.getLength());
            rsp.flip();
        } catch (IOException e) {
            throw new ProfinetException(e);
        }

        // -- DCE/RPC Response --

        rsp.order(ByteOrder.LITTLE_ENDIAN);

        if (rsp.remaining() < DCERPC.HEADER_SIZE) {
            throw new DCEException("RPC response is too short");
        }

        // rpc_vers
        if (rsp.get() != DCERPC.rpc_vers) {
            throw new DCEException("Wrong RPC version");
        }

        // ptype
        int ptype = rsp.get();
        switch (ptype) {
        case DCERPC.response:
            break;
        case DCERPC.reject:
            break;
        case DCERPC.fault:
            break;
        default:
            throw new DCEException("Unknown RPC packet type " + ptype);
        }

        // flags1/2
        rsp.get();
        rsp.get();

        // drep
        if (rsp.get() != 0x10 || rsp.get() != 0x00 || rsp.get() != 0x00) {
            throw new DCEException("Unsupported data representation");
        }

        // serial_hi
        rsp.get();

        if (!object.equals(rsp.getUUID())) {
            throw new DCEException("Object ID mismatch in response");
        }
        if (!interface_.equals(rsp.getUUID())) {
            throw new DCEException("Interface ID mismatch in response");
        }
        if (!activity.equals(rsp.getUUID())) {
            throw new DCEException("Activity ID mismatch in response");
        }

        // server_boot
        rsp.getInt();

        // if_vers
        if (rsp.getInt() != 1) {
            throw new DCEException("Interface version mismatch in response");
        }

        // seqnum
        if (rsp.getInt() != sequence) {
            throw new DCEException("Wrong sequence number in response");
        }

        // opnum
        if (rsp.getShort() != operation) {
            throw new DCEException("Operation ID mismatch in response");
        }

        // ihint/ahint
        rsp.getShort();
        rsp.getShort();

        // len (content + Status (+ ArgsLen + ArgsHeader) or reject/fault
        // status)
        int rspLen = rsp.getShort();
        if (rspLen < 4) {
            throw new DCEException("Fragment length is too short");
        }

        // fragnum
        rsp.getShort();

        // auth_proto
        rsp.get();

        // serial_lo
        rsp.get();

        if (ptype != DCERPC.response) {
            // reject or fault. The length check should ensure this condition
            assert rsp.remaining() >= 4;

            int error = rsp.getAlignedInt();

            throw new DCEException(String.format(
                    "RPC call failed with status 0x%x", error));
        }

        // -- PN-IO Response --

        int pnioStatus = rsp.getAlignedInt();
        if (pnioStatus != 0) {
            throw new PNIOError(pnioStatus);
        }

        if (rsp.remaining() < 16) {
            throw new DCEException("Fragment length is too short");
        }

        int resultLength = rsp.getAlignedInt();
        if (resultLength > result.remaining()) {
            throw new ProfinetException("Result is too large");
        }

        rsp.getAlignedInt();
        rsp.getAlignedInt();
        if (rsp.getAlignedInt() != resultLength) {
            throw new ProfinetException("Invalid result array length");
        }

        if (rsp.remaining() < resultLength) {
            throw new ProfinetException("Invalid result array length");
        }

        if (rsp.remaining() > result.remaining()) {
            rsp.limit(rsp.position() + result.remaining());
        }

        result.put(rsp.buffer());

        result.flip();
    }
}
