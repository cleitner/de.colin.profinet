/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc;

import de.colin.profinet.ProfinetException;

/**
 * Exception thrown by RPC calls that have been rejected by the server.
 */
public class RPCReject extends ProfinetException {

    public static final int nca_rpc_version_mismatch = 0x1c000008;
    public static final int nca_unspec_reject = 0x1c010009;
    public static final int nca_proto_error = 0x1c01000b;
    public static final int nca_unk_if = 0x1c010003;
    public static final int nca_op_rng_error = 0x1c010002;

    private static final long serialVersionUID = -3469762842885238501L;

    private final int error;

    public RPCReject() {
        this(nca_unspec_reject);
    }

    public RPCReject(int error) {
        super("RPC call rejected with status " + error);

        this.error = error;
    }

    public int getError() {
        return error;
    }

}
