/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc;

import de.colin.profinet.ProfinetException;

public class DCEException extends ProfinetException {

    private static final long serialVersionUID = 4685313156076376633L;

    public DCEException() {
    }

    public DCEException(String message) {
        super(message);
    }

    public DCEException(Throwable cause) {
        super(cause);
    }

    public DCEException(String message, Throwable cause) {
        super(message, cause);
    }

}
