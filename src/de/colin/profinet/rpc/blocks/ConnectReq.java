/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import java.util.ArrayList;
import java.util.List;

import de.colin.profinet.BufferAdapter;
import de.colin.profinet.config.DeviceConfiguration;
import de.colin.profinet.config.ModuleConfiguration;
import de.colin.profinet.config.SubmoduleConfiguration;

public class ConnectReq {

    // TODO: Get rid of this or integrate it better. It is solely used to try
    // RT_CLASS_UDP, which doesn't really seem to work at the time being
    private final static boolean UDP = false;

    public ARBlockReq ar = new ARBlockReq();

    public List<IOCRBlockReq> iocrs = new ArrayList<IOCRBlockReq>();

    public List<ExpectedSubmoduleBlockReq> expectedSubmodules = new ArrayList<ExpectedSubmoduleBlockReq>();

    public AlarmCRBlockReq alarmCR = new AlarmCRBlockReq();

    public void read(BufferAdapter buf) {
        throw new UnsupportedOperationException();
    }

    public void write(BufferAdapter buf) {
        BlockBuilder bb = new BlockBuilder(buf);

        bb.addBlock(ar.getType());
        ar.write(buf);

        for (IOCRBlockReq iocr : iocrs) {
            bb.addBlock(iocr.getType());
            iocr.write(buf);
        }

        for (ExpectedSubmoduleBlockReq esm : expectedSubmodules) {
            bb.addBlock(esm.getType());
            esm.write(buf);
        }

        bb.addBlock(alarmCR.getType());
        alarmCR.write(buf);

        bb.complete();
    }

    public void setIOCRs(DeviceConfiguration config, int inputFrameID,
            int sendClockMillis) {
        iocrs.clear();

        // 31,25µs * 32 = 1ms
        int sendClockFactor = 32;
        // 1 ... 16384
        int reductionRatio = sendClockMillis;

        if (reductionRatio < 1 || reductionRatio > 16384) {
            throw new IllegalArgumentException(
                    "Send clock must be between 1 ms and 16384 ms");
        }

        double exp = Math.log(reductionRatio) / Math.log(2);
        if ((exp - Math.floor(exp)) > 0.00001) {
            throw new IllegalArgumentException(
                    "Send clock must be a power of 2");
        }

        // TODO: do we have to make this configurable?
        int watchdogFactor = 10;
        // TODO: It seems as if dataHoldFactor has to be equal to the
        // watchdogFactor
        int dataHoldFactor = watchdogFactor;

        // Total number of submodule inputs. Submodules without I/O have an
        // IODataObject with no data.
        int inputIODataCount = 0;
        // Total length of the input data objects, including IOPS
        int inputIODataLength = 0;

        // Total number of submodule outputs
        int outputIODataCount = 0;
        // Total length of the output data objects, including IOPS
        int outputIODataLength = 0;

        // First pass: Gather lengths
        for (ModuleConfiguration module : config.getModules()) {
            for (SubmoduleConfiguration submodule : module.getSubmodules()) {
                if ((submodule.getInputLength() > 0)
                        || (submodule.getOutputLength() == 0)) {

                    inputIODataCount += 1;
                    inputIODataLength += submodule.getInputLength() + 1;
                }

                if (submodule.getOutputLength() > 0) {
                    outputIODataCount += 1;
                    outputIODataLength += submodule.getOutputLength() + 1;
                }
            }
        }

        // Second pass: Write input IOCR

        IOCRBlockReq input = new IOCRBlockReq();

        input.IOCRType = 0x0001 /* Input */;
        input.IOCRReference = 1;

        if (UDP) {
            input.LT = 0x0800;
        } else {
            input.LT = 0x8892;
        }

        if (UDP) {
            input.IOCRProperties = 0x00000004 /* RT_CLASS_UDP */;
        } else {
            input.IOCRProperties = 0x00000002 /* RT_CLASS_2 */;
        }

        int dataLength = 0;
        ;
        // The input frame contains all input IO data objects and an IOCS for
        // every output
        dataLength += inputIODataLength + outputIODataCount * 1;
        // Finally include padding if the PDU is too short
        // TODO: min 12 for UDP
        // TODO: Can't exceed 1440
        dataLength = Math.max(40, dataLength);

        input.DataLength = dataLength;

        if (UDP) {
            input.FrameID = 0xC000;
        } else {
            input.FrameID = inputFrameID;
        }

        // -- Timings --

        input.SendClockFactor = sendClockFactor;
        input.ReductionRatio = reductionRatio;
        input.Phase = 1;
        input.Sequence = 0;
        input.FrameSendOffset = 0xFFFFFFFF;
        input.WatchdogFactor = watchdogFactor;
        input.DataHoldFactor = dataHoldFactor;
        input.IOCRTagHeader = 0xC000;
        // IOCRMulticaseMACAdd
        // TODO: nothing

        IOCRBlockReq.API api = new IOCRBlockReq.API();
        api.API = 0;

        int frameOffset = 0;

        for (ModuleConfiguration module : config.getModules()) {
            for (SubmoduleConfiguration submodule : module.getSubmodules()) {
                if (!((submodule.getInputLength() > 0) || (submodule
                        .getOutputLength() == 0))) {
                    continue;
                }

                IOCRBlockReq.IODataObject iodo = new IOCRBlockReq.IODataObject();

                iodo.SlotNumber = module.getSlot();
                iodo.SubslotNumber = submodule.getSubslot();
                iodo.IODataObjectFrameOffset = frameOffset;

                api.IODataObjects.add(iodo);

                frameOffset += submodule.getInputLength() + 1;
            }
        }

        assert api.IODataObjects.size() == inputIODataCount;

        for (ModuleConfiguration module : config.getModules()) {
            for (SubmoduleConfiguration submodule : module.getSubmodules()) {
                if (submodule.getOutputLength() == 0) {
                    continue;
                }

                IOCRBlockReq.IOCS iocs = new IOCRBlockReq.IOCS();

                iocs.SlotNumber = module.getSlot();
                iocs.SubslotNumber = submodule.getSubslot();
                iocs.IOCSFrameOffset = frameOffset;

                api.IOCSs.add(iocs);

                frameOffset += 1;
            }
        }

        assert api.IOCSs.size() == outputIODataCount;

        input.APIs.add(api);
        iocrs.add(input);

        // Third pass: Write output IOCR

        IOCRBlockReq output = new IOCRBlockReq();

        output.IOCRType = 0x0002 /* Output */;
        output.IOCRReference = 2;

        if (UDP) {
            output.LT = 0x0800;
        } else {
            output.LT = 0x8892;
        }

        if (UDP) {
            output.IOCRProperties = 0x00000004 /* RT_CLASS_UDP */;
        } else {
            output.IOCRProperties = 0x00000002 /* RT_CLASS_2 */;
        }

        // DataLength
        dataLength = 0;
        // The input frame contains all input IO data objects and an IOCS for
        // every output
        dataLength += inputIODataCount * 1 + outputIODataLength;
        // Finally include padding if the PDU is too short
        // TODO: min 12 for UDP
        // TODO: Can't exceed 1440
        dataLength = Math.max(40, dataLength);

        output.DataLength = dataLength;

        // FrameID
        output.FrameID = 0xFFFF;

        // -- Timings --

        output.SendClockFactor = sendClockFactor;
        output.ReductionRatio = reductionRatio;
        output.Phase = 1;
        output.Sequence = 0;
        output.FrameSendOffset = 0xFFFFFFFF;
        output.WatchdogFactor = watchdogFactor;
        output.DataHoldFactor = dataHoldFactor;
        output.IOCRTagHeader = 0xC000;
        // IOCRMulticaseMACAdd
        // TODO: nothing

        api = new IOCRBlockReq.API();
        api.API = 0;

        // Follow the Siemens style of ordering the offset. Input IOCS are sent
        // before the IODataObjects

        frameOffset = inputIODataCount * 1;

        for (ModuleConfiguration module : config.getModules()) {
            for (SubmoduleConfiguration submodule : module.getSubmodules()) {
                if (submodule.getOutputLength() == 0) {
                    continue;
                }

                IOCRBlockReq.IODataObject iodo = new IOCRBlockReq.IODataObject();

                iodo.SlotNumber = module.getSlot();
                iodo.SubslotNumber = submodule.getSubslot();
                iodo.IODataObjectFrameOffset = frameOffset;

                api.IODataObjects.add(iodo);

                frameOffset += submodule.getOutputLength() + 1;
            }
        }

        assert api.IODataObjects.size() == outputIODataCount;

        frameOffset = 0;

        for (ModuleConfiguration module : config.getModules()) {
            for (SubmoduleConfiguration submodule : module.getSubmodules()) {
                if (!((submodule.getInputLength() > 0) || (submodule
                        .getOutputLength() == 0))) {
                    continue;
                }

                IOCRBlockReq.IOCS iocs = new IOCRBlockReq.IOCS();

                iocs.SlotNumber = module.getSlot();
                iocs.SubslotNumber = submodule.getSubslot();
                iocs.IOCSFrameOffset = frameOffset;

                api.IOCSs.add(iocs);

                frameOffset += 1;
            }
        }

        assert api.IOCSs.size() == inputIODataCount;

        output.APIs.add(api);
        iocrs.add(output);
    }

    public void setExpectedSubmodules(DeviceConfiguration config) {
        expectedSubmodules.clear();

        for (ModuleConfiguration module : config.getModules()) {
            ExpectedSubmoduleBlockReq esm = new ExpectedSubmoduleBlockReq();
            ExpectedSubmoduleBlockReq.API api = new ExpectedSubmoduleBlockReq.API();

            // We support only API 0 atm.
            api.API = 0;
            api.SlotNumber = module.getSlot();
            api.ModuleIdentNumber = module.getIdentNumber();
            api.ModuleProperties = 0;

            for (SubmoduleConfiguration submodule : module.getSubmodules()) {
                ExpectedSubmoduleBlockReq.Submodule sm = new ExpectedSubmoduleBlockReq.Submodule();

                sm.SubslotNumber = submodule.getSubslot();
                sm.SubmoduleIdentNumber = submodule.getIdentNumber();

                // SubmoduleProperties
                if ((submodule.getInputLength() > 0)
                        || (submodule.getOutputLength() == 0)) {
                    sm.SubmoduleProperties |= ExpectedSubmoduleBlockReq.Submodule.SUBMODULE_INPUT;
                }
                if (submodule.getOutputLength() != 0) {
                    sm.SubmoduleProperties |= ExpectedSubmoduleBlockReq.Submodule.SUBMODULE_OUTPUT;
                }

                // Missing I/O is translated into an Input IOCS
                if ((submodule.getInputLength() > 0)
                        || (submodule.getOutputLength() == 0)) {
                    ExpectedSubmoduleBlockReq.DataDescription desc = new ExpectedSubmoduleBlockReq.DataDescription();

                    desc.DataDescription = ExpectedSubmoduleBlockReq.Submodule.SUBMODULE_INPUT;
                    desc.SubmoduleDataLength = submodule.getInputLength();
                    desc.LengthIOCS = 1;
                    desc.LengthIOPS = 1;

                    sm.DataDescriptions.add(desc);
                }

                if (submodule.getOutputLength() > 0) {
                    ExpectedSubmoduleBlockReq.DataDescription desc = new ExpectedSubmoduleBlockReq.DataDescription();

                    desc.DataDescription = ExpectedSubmoduleBlockReq.Submodule.SUBMODULE_OUTPUT;
                    desc.SubmoduleDataLength = submodule.getOutputLength();
                    desc.LengthIOCS = 1;
                    desc.LengthIOPS = 1;

                    sm.DataDescriptions.add(desc);
                }

                api.Submodules.add(sm);
            }

            esm.APIs.add(api);
            expectedSubmodules.add(esm);
        }
    }
}
