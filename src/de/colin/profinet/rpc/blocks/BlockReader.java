/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import de.colin.profinet.BufferAdapter;

public class BlockReader {

    public static void readBlocks(ByteBuffer blocks, BlockHandler handler) {
        blocks.order(ByteOrder.BIG_ENDIAN);

        final int limit = blocks.limit();

        while (blocks.hasRemaining()) {
            int type = blocks.getShort() & 0xFFFF;
            int length = blocks.getShort() & 0xFFFF;
            int version = blocks.getShort() & 0xFFFF;

            int blockEnd = blocks.position() + length - 2;
            blocks.limit(blockEnd);

            handler.onBlock(type, version, blocks.slice());

            blocks.limit(limit);
            blocks.position(blockEnd);
        }
    }

    private BufferAdapter buf;
    private int limit;

    public BlockReader(BufferAdapter buf) {
        this.buf = buf;

        this.limit = buf.limit();
        buf.limit(buf.position());
    }

    /**
     * Reads the next block and checks it for the given type. The buffer will be
     * limited to the blocks contents until {@code nextBlock} is called again.
     * 
     * @param expectedType
     *            the expected type of the next block
     * @return the length of the block
     */
    public int nextBlock(short expectedType) throws IllegalBlockException {
        if (buf.remaining() != 0) {
            throw new IllegalBlockException("Block contains unread data");
        }

        buf.limit(limit);

        int type = buf.getShort() & 0xffff;
        int length = buf.getShort() & 0xffff;
        int version = buf.getShort() & 0xffff;

        if (type != expectedType) {
            throw new IllegalBlockException(String.format(
                    "Block type mismatch (expected 0x%04X, got 0x%04X)",
                    expectedType, type));
        }

        if (version != 0x0100) {
            throw new IllegalBlockException(String.format(
                    "Block version mismatch (expected 0x0100, got 0x%04X)",
                    version));
        }

        buf.limit(buf.position() + length - 2);

        return length;
    }
}
