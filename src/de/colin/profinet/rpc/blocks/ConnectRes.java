/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import de.colin.profinet.BufferAdapter;
import de.colin.profinet.config.DeviceConfiguration;
import de.colin.profinet.config.ModuleConfiguration;
import de.colin.profinet.config.ModuleState;
import de.colin.profinet.config.ParameterRecordConfiguration;
import de.colin.profinet.config.SubmoduleConfiguration;
import de.colin.profinet.config.SubmoduleState;

public class ConnectRes {

    private static final Logger LOG = Logger.getLogger(ConnectRes.class
            .getName());

    public ARBlockRes ar = new ARBlockRes();

    public List<IOCRBlockRes> iocrs = new ArrayList<IOCRBlockRes>();

    public AlarmCRBlockRes alarmCR = new AlarmCRBlockRes();

    public ModuleDiffBlock moduleDiff = new ModuleDiffBlock();

    /**
     * Converts the module diff block to a device configuration
     */
    public DeviceConfiguration getDeviceConfiguration(
            DeviceConfiguration requestedConfig) {

        List<ModuleConfiguration> modules = new ArrayList<ModuleConfiguration>();

        for (ModuleDiffBlock.API api : moduleDiff.APIs) {
            // Ignore all APIs != 0
            if (api.API != 0) {
                continue;
            }

            for (ModuleDiffBlock.Module m : api.Modules) {

                ModuleConfiguration reqM = requestedConfig
                        .getModule(m.SlotNumber);

                List<SubmoduleConfiguration> submodules = new ArrayList<SubmoduleConfiguration>();

                for (ModuleDiffBlock.Submodule sm : m.Submodules) {
                    SubmoduleState submoduleState = null;
                    // The submodule state is either encoded as "Detail" or as a
                    // bitfield
                    if ((sm.SubmoduleState & 0x8000) != 0) {
                        // Bitfield
                        switch ((sm.SubmoduleState >>> 11) & 0xF) {
                        case 0:
                            submoduleState = SubmoduleState.PROPER_SUBMODULE;
                            break;
                        case 1:
                            submoduleState = SubmoduleState.SUBSTITUTE;
                            break;
                        case 2:
                            submoduleState = SubmoduleState.WRONG_SUBMODULE;
                            break;
                        case 3:
                            submoduleState = SubmoduleState.NO_SUBMODULE;
                            break;
                        default:
                            // reserved
                            break;
                        }
                    } else {
                        // Detail
                        switch (sm.SubmoduleState) {
                        case 0:
                            submoduleState = SubmoduleState.NO_SUBMODULE;
                            break;
                        case 1:
                            submoduleState = SubmoduleState.WRONG_SUBMODULE;
                            break;
                        case 7:
                            submoduleState = SubmoduleState.SUBSTITUTE;
                            break;
                        default:
                            // reserved
                            break;
                        }
                    }

                    int inputLength = -1;
                    int outputLength = -1;

                    if (reqM != null) {
                        SubmoduleConfiguration reqSm = reqM
                                .getSubmodule(sm.SubslotNumber);

                        if (reqSm != null) {
                            inputLength = reqSm.getInputLength();
                            outputLength = reqSm.getOutputLength();
                        }
                    }

                    submodules.add(new SubmoduleConfiguration(sm.SubslotNumber,
                            sm.SubmoduleIdentNumber, inputLength, outputLength,
                            submoduleState,
                            new ArrayList<ParameterRecordConfiguration>()));
                }

                ModuleState moduleState = null;
                switch (m.ModuleState) {
                case 0:
                    moduleState = ModuleState.NO_MODULE;
                    break;
                case 1:
                    moduleState = ModuleState.WRONG_MODULE;
                    break;
                case 2:
                    moduleState = ModuleState.PROPER_MODULE;
                    break;
                case 3:
                    moduleState = ModuleState.SUBSTITUTE;
                    break;
                default:
                    // reserved
                    break;
                }

                modules.add(new ModuleConfiguration(m.SlotNumber,
                        m.ModuleIdentNumber, moduleState, submodules));
            }
        }

        return new DeviceConfiguration(requestedConfig.getVendorId(),
                requestedConfig.getDeviceId(), modules);

    }

    public void read(BufferAdapter buf) {

        ar = new ARBlockRes();
        iocrs.clear();
        alarmCR = new AlarmCRBlockRes();
        moduleDiff = null;

        BlockReader.readBlocks(buf.buffer(), new BlockHandler() {
            @Override
            public void onBlock(int type, int version, ByteBuffer content) {
                BufferAdapter buf = new BufferAdapter(content);

                switch (type) {
                case ARBlockRes.BLOCK_TYPE:
                    ar.read(buf);
                    break;

                case IOCRBlockRes.BLOCK_TYPE:
                    IOCRBlockRes iocr = new IOCRBlockRes();
                    iocr.read(buf);
                    iocrs.add(iocr);
                    break;

                case ModuleDiffBlock.BLOCK_TYPE:
                    moduleDiff = new ModuleDiffBlock();
                    moduleDiff.read(buf);
                    break;

                case AlarmCRBlockRes.BLOCK_TYPE:
                    alarmCR.read(buf);
                    break;

                default:
                    LOG.info(String.format(
                            "Unknown block type 0x%04X. Ignoring it", type));
                    break;
                }
            }
        });
    }

    public void write(BufferAdapter buf) {
        throw new UnsupportedOperationException();
    }
}
