/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import java.util.ArrayList;
import java.util.List;

import de.colin.profinet.BufferAdapter;

public class IOCRBlockReq implements Block {

    public static final int BLOCK_TYPE = 0x0102;

    public static class API {
        public int API;
        public List<IODataObject> IODataObjects = new ArrayList<IOCRBlockReq.IODataObject>();
        public List<IOCS> IOCSs = new ArrayList<IOCRBlockReq.IOCS>();
    }

    // TODO: These two classes could be merged. However for the moment we follow
    // the Wireshark nomenclature

    public static class IODataObject {
        public int SlotNumber;
        public int SubslotNumber;
        public int IODataObjectFrameOffset;
    }

    public static class IOCS {
        public int SlotNumber;
        public int SubslotNumber;
        public int IOCSFrameOffset;
    }

    // -- Fields --

    public int IOCRType;

    public int IOCRReference;

    // 0x8892 for RT_CLASS_1/2/3, 0x0800 for RT_CLASS_UDP
    public int LT = 0x8892;

    public int IOCRProperties;

    // Size of IO data. Minimum 40 for RT_CLASS_1/2/3, 12 for RT_CLASS_UDP. Max
    // 1440 or 1412.
    public int DataLength;

    public int FrameID;

    // SendClock = ReductionRatio * SendClockFactor * 31.25 µs
    public int SendClockFactor;

    // Must be a power of 2
    public int ReductionRatio;

    public int Phase = 1;

    public int Sequence = 0;

    // Is this in ns?
    public int FrameSendOffset = 0xFFFFFFFF;

    public int WatchdogFactor = 3;

    // Must be >= WatchdogFactor
    public int DataHoldFactor = 3;

    public int IOCRTagHeader = 0xC000;

    public MACAddress IOCRMulticastMACAdd = new MACAddress();

    public List<API> APIs = new ArrayList<IOCRBlockReq.API>();

    // -- Encoding --

    @Override
    public short getType() {
        return (short) BLOCK_TYPE;
    }

    @Override
    public void read(BufferAdapter buf) {
        IOCRType = buf.getUnsignedShort();
        IOCRReference = buf.getUnsignedShort();
        LT = buf.getUnsignedShort();
        IOCRProperties = buf.getInt();
        DataLength = buf.getUnsignedShort();
        FrameID = buf.getUnsignedShort();
        SendClockFactor = buf.getUnsignedShort();
        ReductionRatio = buf.getUnsignedShort();
        Phase = buf.getUnsignedShort();
        Sequence = buf.getUnsignedShort();
        FrameSendOffset = buf.getInt();
        WatchdogFactor = buf.getUnsignedShort();
        DataHoldFactor = buf.getUnsignedShort();
        IOCRTagHeader = buf.getUnsignedShort();
        buf.get(IOCRMulticastMACAdd.address);

        APIs.clear();
        int apiCount = buf.getUnsignedShort();
        while (apiCount > 0) {
            API api = new API();

            api.API = buf.getInt();

            int ioDataObjectCount = buf.getUnsignedShort();
            while (ioDataObjectCount > 0) {
                IODataObject iodo = new IODataObject();

                iodo.SlotNumber = buf.getUnsignedShort();
                iodo.SubslotNumber = buf.getUnsignedShort();
                iodo.IODataObjectFrameOffset = buf.getUnsignedShort();

                api.IODataObjects.add(iodo);
                ioDataObjectCount -= 1;
            }

            int iocsCount = buf.getUnsignedShort();
            while (iocsCount > 0) {
                IOCS iocs = new IOCS();

                iocs.SlotNumber = buf.getUnsignedShort();
                iocs.SubslotNumber = buf.getUnsignedShort();
                iocs.IOCSFrameOffset = buf.getUnsignedShort();

                api.IOCSs.add(iocs);
                iocsCount -= 1;
            }

            APIs.add(api);
            apiCount -= 1;
        }
    }

    @Override
    public void write(BufferAdapter buf) {
        buf.putUnsignedShort(IOCRType);
        buf.putUnsignedShort(IOCRReference);
        buf.putUnsignedShort(LT);
        buf.putInt(IOCRProperties);
        buf.putUnsignedShort(DataLength);
        buf.putUnsignedShort(FrameID);
        buf.putUnsignedShort(SendClockFactor);
        buf.putUnsignedShort(ReductionRatio);
        buf.putUnsignedShort(Phase);
        buf.putUnsignedShort(Sequence);
        buf.putInt(FrameSendOffset);
        buf.putUnsignedShort(WatchdogFactor);
        buf.putUnsignedShort(DataHoldFactor);
        buf.putUnsignedShort(IOCRTagHeader);
        buf.put(IOCRMulticastMACAdd.address);

        buf.putUnsignedShort(APIs.size());
        for (API api : APIs) {
            buf.putInt(api.API);

            buf.putUnsignedShort(api.IODataObjects.size());
            for (IODataObject iodo : api.IODataObjects) {
                buf.putUnsignedShort(iodo.SlotNumber);
                buf.putUnsignedShort(iodo.SubslotNumber);
                buf.putUnsignedShort(iodo.IODataObjectFrameOffset);
            }

            buf.putUnsignedShort(api.IOCSs.size());
            for (IOCS iocs : api.IOCSs) {
                buf.putUnsignedShort(iocs.SlotNumber);
                buf.putUnsignedShort(iocs.SubslotNumber);
                buf.putUnsignedShort(iocs.IOCSFrameOffset);
            }
        }
    }
}
