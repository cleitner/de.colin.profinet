/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import de.colin.profinet.BufferAdapter;

public class AlarmCRBlockRes implements Block {

    public static final int BLOCK_TYPE = 0x8103;

    // -- Fields --

    public int AlarmCRType = 0x0001;

    public int LocalAlarmReference;

    public int MaxAlarmDataLength = 1432;

    // -- Encoding --

    @Override
    public short getType() {
        return (short) BLOCK_TYPE;
    }

    @Override
    public void read(BufferAdapter buf) {
        AlarmCRType = buf.getUnsignedShort();
        LocalAlarmReference = buf.getUnsignedShort();
        MaxAlarmDataLength = buf.getUnsignedShort();
    }

    @Override
    public void write(BufferAdapter buf) {
        buf.putUnsignedShort(AlarmCRType);
        buf.putUnsignedShort(LocalAlarmReference);
        buf.putUnsignedShort(MaxAlarmDataLength);
    }
}
