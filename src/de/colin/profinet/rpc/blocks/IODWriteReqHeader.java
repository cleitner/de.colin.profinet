/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import java.util.UUID;

import de.colin.profinet.BufferAdapter;

public class IODWriteReqHeader implements Block {

    public static final int BLOCK_TYPE = 0x0008;

    // -- Fields --

    public int SeqNumber;

    public UUID ARUUID;

    public int API;

    public int SlotNumber;

    public int SubslotNumber;

    public int Index;

    public int RecordDataLength;

    // -- Encoding --

    @Override
    public short getType() {
        return (short) BLOCK_TYPE;
    }

    @Override
    public void read(BufferAdapter buf) {
        SeqNumber = buf.getUnsignedShort();
        ARUUID = buf.getUUID();
        API = buf.getInt();
        SlotNumber = buf.getUnsignedShort();
        SubslotNumber = buf.getUnsignedShort();
        buf.fill(2);
        Index = buf.getUnsignedShort();
        RecordDataLength = buf.getInt();
        buf.fill(24);
    }

    @Override
    public void write(BufferAdapter buf) {
        buf.putUnsignedShort(SeqNumber);
        buf.putUUID(ARUUID);
        buf.putInt(API);
        buf.putUnsignedShort(SlotNumber);
        buf.putUnsignedShort(SubslotNumber);
        buf.fill(2);
        buf.putUnsignedShort(Index);
        buf.putInt(RecordDataLength);
        buf.fill(24);
    }
}
