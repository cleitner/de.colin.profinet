/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import java.nio.ByteBuffer;
import java.util.Deque;
import java.util.LinkedList;

import de.colin.profinet.BufferAdapter;

/**
 * The block builder simplifies writing of PNIO control blocks, which require
 * the block length to be written at the start of each block.
 */
public class BlockBuilder {

    private final BufferAdapter buf;
    private final Deque<Integer> blockLengthOffsets = new LinkedList<Integer>();

    public BlockBuilder(BufferAdapter buf) {
        this.buf = buf;
    }

    public BufferAdapter getBuffer() {
        return buf;
    }

    public void addBlock(short type) {
        buf.putShort(type);

        // Remember position of current block
        blockLengthOffsets.push(buf.position());

        // Write a dummy length
        buf.putShort((short) 0);

        // Write version
        buf.put((byte) 1);
        buf.put((byte) 0);
    }

    public void addBlock(short type, byte[] content) {
        addBlock(type, ByteBuffer.wrap(content));
    }

    public void addBlock(short type, ByteBuffer content) {
        buf.putShort(type);
        buf.putShort((short) content.remaining());
        buf.put((byte) 1);
        buf.put((byte) 0);
        buf.put(content);
    }

    public void complete() {
        int position = buf.position();

        while (!blockLengthOffsets.isEmpty()) {
            int offset = blockLengthOffsets.pop();

            int length = buf.position() - offset - 2;

            buf.position(offset);
            buf.putShort((short) length);
            buf.position(buf.position() - 4);
        }

        buf.position(position);
    }
}
