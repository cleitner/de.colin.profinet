/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import java.util.UUID;

import de.colin.profinet.BufferAdapter;

public class ARBlockReq implements Block {

    public static final int BLOCK_TYPE = 0x0101;

    // -- Fields --

    public int ARType = 0x0001;

    public UUID ARUUID;

    public int SessionKey;

    public MACAddress CMInitiatorMacAdd = new MACAddress();

    public UUID CMInitiatorObjectUUID;

    public int ARProperties = 0x00000011;

    public int CMInitiatorActivityTimeoutFactor = 600;

    // Must be 0x8892
    public int CMInitiatorUDPRTPort = 0x8892;

    public String CMInitiatorStationName;

    // -- Encoding --

    @Override
    public short getType() {
        return (short) BLOCK_TYPE;
    }

    @Override
    public void read(BufferAdapter buf) {
        ARType = buf.getUnsignedShort();
        ARUUID = buf.getUUID();
        SessionKey = buf.getUnsignedShort();
        buf.get(CMInitiatorMacAdd.address);
        CMInitiatorObjectUUID = buf.getUUID();
        ARProperties = buf.getInt();
        CMInitiatorActivityTimeoutFactor = buf.getUnsignedShort();
        CMInitiatorUDPRTPort = buf.getUnsignedShort();
        CMInitiatorStationName = buf.getLengthPrefixedString();
    }

    @Override
    public void write(BufferAdapter buf) {
        buf.putUnsignedShort(ARType);
        buf.putUUID(ARUUID);
        buf.putUnsignedShort(SessionKey);
        buf.put(CMInitiatorMacAdd.address);
        buf.putUUID(CMInitiatorObjectUUID);
        buf.putInt(ARProperties);
        buf.putUnsignedShort(CMInitiatorActivityTimeoutFactor);
        buf.putUnsignedShort(CMInitiatorUDPRTPort);
        buf.putLengthPrefixedString(CMInitiatorStationName);
    }
}
