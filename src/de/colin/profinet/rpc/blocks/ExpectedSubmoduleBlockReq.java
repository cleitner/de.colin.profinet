/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import java.util.ArrayList;
import java.util.List;

import de.colin.profinet.BufferAdapter;

public class ExpectedSubmoduleBlockReq implements Block {

    public static class API {
        public int API;
        public int SlotNumber;
        public int ModuleIdentNumber;
        public int ModuleProperties;
        public List<Submodule> Submodules = new ArrayList<Submodule>();
    }

    public static class Submodule {
        public static int SUBMODULE_INPUT = 0x0001;
        public static int SUBMODULE_OUTPUT = 0x0002;

        public int SubslotNumber;
        public int SubmoduleIdentNumber;
        public int SubmoduleProperties;
        public List<DataDescription> DataDescriptions = new ArrayList<DataDescription>();
    }

    public static class DataDescription {
        public int DataDescription;
        public int SubmoduleDataLength;
        public int LengthIOCS = 1;
        public int LengthIOPS = 1;
    }

    // -- Fields --

    public List<API> APIs = new ArrayList<ExpectedSubmoduleBlockReq.API>();

    // -- Encoding --

    @Override
    public short getType() {
        return (short) 0x0104;
    }

    @Override
    public void read(BufferAdapter buf) {
        APIs.clear();
        int apiCount = buf.getUnsignedShort();
        while (apiCount > 0) {
            API api = new API();

            api.API = buf.getInt();
            api.SlotNumber = buf.getUnsignedShort();
            api.ModuleIdentNumber = buf.getInt();
            api.ModuleProperties = buf.getUnsignedShort();

            int submoduleCount = buf.getUnsignedShort();
            while (submoduleCount > 0) {
                Submodule submodule = new Submodule();

                submodule.SubslotNumber = buf.getUnsignedShort();
                submodule.SubmoduleIdentNumber = buf.getInt();
                submodule.SubmoduleProperties = buf.getUnsignedShort();

                if ((submodule.SubmoduleProperties & 0x0001) != 0) {
                    DataDescription desc = new DataDescription();

                    desc.DataDescription = buf.getUnsignedShort();
                    desc.SubmoduleDataLength = buf.getUnsignedShort();
                    desc.LengthIOCS = buf.get() & 0xFF;
                    desc.LengthIOPS = buf.get() & 0xFF;

                    submodule.DataDescriptions.add(desc);
                }

                if ((submodule.SubmoduleProperties & 0x0002) != 0) {
                    DataDescription desc = new DataDescription();

                    desc.DataDescription = buf.getUnsignedShort();
                    desc.SubmoduleDataLength = buf.getUnsignedShort();
                    desc.LengthIOCS = buf.get() & 0xFF;
                    desc.LengthIOPS = buf.get() & 0xFF;

                    submodule.DataDescriptions.add(desc);
                }

                api.Submodules.add(submodule);
                submoduleCount -= 1;
            }

            APIs.add(api);
            apiCount -= 1;
        }
    }

    @Override
    public void write(BufferAdapter buf) {
        buf.putUnsignedShort(APIs.size());
        for (API api : APIs) {
            buf.putInt(api.API);

            buf.putUnsignedShort(api.SlotNumber);
            buf.putInt(api.ModuleIdentNumber);
            buf.putUnsignedShort(api.ModuleProperties);

            buf.putUnsignedShort(api.Submodules.size());
            for (Submodule submodule : api.Submodules) {
                buf.putUnsignedShort(submodule.SubslotNumber);
                buf.putInt(submodule.SubmoduleIdentNumber);
                buf.putUnsignedShort(submodule.SubmoduleProperties);

                for (DataDescription desc : submodule.DataDescriptions) {
                    buf.putUnsignedShort(desc.DataDescription);
                    buf.putUnsignedShort(desc.SubmoduleDataLength);
                    buf.put((byte) desc.LengthIOCS);
                    buf.put((byte) desc.LengthIOPS);
                }
            }
        }
    }

}
