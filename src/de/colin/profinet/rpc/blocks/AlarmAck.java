/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import de.colin.profinet.BufferAdapter;

public class AlarmAck implements Block {

    public static final int BLOCK_TYPE_ALARM_ACK_HIGH = 0x8001;
    public static final int BLOCK_TYPE_ALARM_ACK_LOW = 0x8002;

    private final short type;

    public AlarmAck(int type) {
        this.type = (short) type;
    }

    // -- Fields --

    public int AlarmType;

    public int API;

    public int SlotNumber;

    public int SubslotNumber;

    public int AlarmSpecifier;

    public int Status;

    // -- Encoding --

    public short getType() {
        return type;
    }

    @Override
    public void read(BufferAdapter buf) {
        AlarmType = buf.getUnsignedShort();
        API = buf.getInt();
        SlotNumber = buf.getUnsignedShort();
        SubslotNumber = buf.getUnsignedShort();
        AlarmSpecifier = buf.getUnsignedShort();
        Status = buf.getInt();
    }

    @Override
    public void write(BufferAdapter buf) {
        buf.putUnsignedShort(AlarmType);
        buf.putInt(API);
        buf.putUnsignedShort(SlotNumber);
        buf.putUnsignedShort(SubslotNumber);
        buf.putUnsignedShort(AlarmSpecifier);
        buf.putInt(Status);
    }
}
