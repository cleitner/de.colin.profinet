/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import java.util.UUID;

import de.colin.profinet.BufferAdapter;

public class IODBlockRes implements Block {

    public static final int BLOCK_TYPE_PARAMETER_END = 0x8110;
    public static final int BLOCK_TYPE_APPLICATION_READY = 0x8112;
    public static final int BLOCK_TYPE_RELEASE = 0x8114;

    private final short type;

    public IODBlockRes(int type) {
        this.type = (short) type;
    }

    // -- Fields --

    public UUID ARUUID;

    public int SessionKey;

    public int ControlCommand;

    public int ControlBlockProperties;

    // -- Encoding --

    public short getType() {
        return type;
    }

    @Override
    public void read(BufferAdapter buf) {
        // reserved
        buf.getUnsignedShort();
        ARUUID = buf.getUUID();
        SessionKey = buf.getUnsignedShort();
        // reserved
        buf.getUnsignedShort();
        ControlCommand = buf.getUnsignedShort();
        ControlBlockProperties = buf.getUnsignedShort();
    }

    @Override
    public void write(BufferAdapter buf) {
        // reserved
        buf.putUnsignedShort(0);
        buf.putUUID(ARUUID);
        buf.putUnsignedShort(SessionKey);
        // reserved
        buf.putUnsignedShort(0);
        buf.putUnsignedShort(ControlCommand);
        buf.putUnsignedShort(ControlBlockProperties);
    }
}
