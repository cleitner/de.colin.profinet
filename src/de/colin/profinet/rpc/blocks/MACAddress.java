/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

public class MACAddress {

    public byte[] address = new byte[6];

    public void fromAddress(byte[] mac) {
        System.arraycopy(mac, 0, address, 0, 6);
    }

    @Override
    public String toString() {
        return String.format("%02X:%02X:%02X:%02X:%02X:%02X",
                address[0] & 0xFF, address[1] & 0xFF, address[2] & 0xFF,
                address[3] & 0xFF, address[4] & 0xFF, address[5] & 0xFF);
    }
}
