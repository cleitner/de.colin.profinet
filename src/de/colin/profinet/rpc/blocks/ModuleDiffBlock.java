/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import java.util.ArrayList;
import java.util.List;

import de.colin.profinet.BufferAdapter;

public class ModuleDiffBlock implements Block {

    public static final int BLOCK_TYPE = 0x8104;

    public static class API {
        public int API;

        public List<Module> Modules = new ArrayList<Module>();
    }

    public static class Module {
        public static final int SUBSTITUTE = 0x0003;

        public int SlotNumber;
        public int ModuleIdentNumber;
        public int ModuleState;

        public List<Submodule> Submodules = new ArrayList<Submodule>();
    }

    public static class Submodule {
        public int SubslotNumber;
        public int SubmoduleIdentNumber;
        public int SubmoduleState;
    }

    // -- Fields --

    public List<API> APIs = new ArrayList<API>();

    // -- Encoding --

    @Override
    public short getType() {
        return (short) BLOCK_TYPE;
    }

    @Override
    public void read(BufferAdapter buf) {
        APIs.clear();
        int apiCount = buf.getUnsignedShort();
        while (apiCount > 0) {
            API api = new API();

            api.API = buf.getInt();

            int moduleCount = buf.getUnsignedShort();
            while (moduleCount > 0) {
                Module module = new Module();

                module.SlotNumber = buf.getUnsignedShort();
                module.ModuleIdentNumber = buf.getInt();
                module.ModuleState = buf.getUnsignedShort();

                int submoduleCount = buf.getUnsignedShort();
                while (submoduleCount > 0) {
                    Submodule submodule = new Submodule();

                    submodule.SubslotNumber = buf.getUnsignedShort();
                    submodule.SubmoduleIdentNumber = buf.getInt();
                    submodule.SubmoduleState = buf.getUnsignedShort();

                    module.Submodules.add(submodule);
                    submoduleCount -= 1;
                }

                api.Modules.add(module);
                moduleCount -= 1;
            }

            APIs.add(api);
            apiCount -= 1;
        }
    }

    @Override
    public void write(BufferAdapter buf) {
        buf.putUnsignedShort(APIs.size());
        for (API api : APIs) {
            buf.putInt(api.API);

            buf.putUnsignedShort(api.Modules.size());
            for (Module module : api.Modules) {
                buf.putUnsignedShort(module.SlotNumber);
                buf.putInt(module.ModuleIdentNumber);
                buf.putUnsignedShort(module.ModuleState);

                buf.putUnsignedShort(module.Submodules.size());
                for (Submodule submodule : module.Submodules) {
                    buf.putUnsignedShort(submodule.SubslotNumber);
                    buf.putInt(submodule.SubmoduleIdentNumber);
                    buf.putUnsignedShort(submodule.SubmoduleState);
                }
            }
        }
    }

}
