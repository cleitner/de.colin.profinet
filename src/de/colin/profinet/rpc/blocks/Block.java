/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import de.colin.profinet.BufferAdapter;

public interface Block {

    /**
     * Returns the block type code. Response blocks have the highest bit (15)
     * set.
     * 
     * @return the block type code
     */
    short getType();

    /**
     * Reads the block content (excluding the header) from the given buffer.
     * 
     * @param buf
     *            the buffer to read from
     */
    void read(BufferAdapter buf);

    /**
     * Writes the block content (excluding the header) to the given buffer.
     * 
     * @param buf
     *            the buffer to write to
     */
    void write(BufferAdapter buf);
}
