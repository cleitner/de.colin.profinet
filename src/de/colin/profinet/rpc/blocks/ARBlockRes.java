/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import java.util.UUID;

import de.colin.profinet.BufferAdapter;

public class ARBlockRes implements Block {

    public static final int BLOCK_TYPE = 0x8101;

    // -- Fields --

    public int ARType;

    public UUID ARUUID;

    public int SessionKey;

    public MACAddress CMResponderMacAdd = new MACAddress();

    // Must be 0x8892
    public int CMResponderUDPRTPort = 0x8892;

    // -- Encoding --

    @Override
    public short getType() {
        return (short) BLOCK_TYPE;
    }

    @Override
    public void read(BufferAdapter buf) {
        ARType = buf.getShort() & 0xFFFF;
        ARUUID = buf.getUUID();
        SessionKey = buf.getShort() & 0xFFFF;
        buf.get(CMResponderMacAdd.address);
        CMResponderUDPRTPort = buf.getShort() & 0xFFFF;
    }

    @Override
    public void write(BufferAdapter buf) {
        buf.putShort((short) ARType);
        buf.putUUID(ARUUID);
        buf.putShort((short) SessionKey);
        buf.put(CMResponderMacAdd.address);
        buf.putShort((short) CMResponderUDPRTPort);
    }

}
