/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc.blocks;

import de.colin.profinet.BufferAdapter;

public class AlarmCRBlockReq implements Block {

    // -- Fields --

    public int AlarmCRType = 0x0001;

    // Must be 0x8892
    public int LT = 0x8892;

    public int AlarmCRProperties;

    public int RTATimeoutFactor = 3;

    public int RTARetries = 3;

    public int LocalAlarmReference = 2;

    public int MaxAlarmDataLength = 200;

    public int AlarmCRTagHeaderHigh = 0xC000;

    public int AlarmCRTagHeaderLow = 0xA000;

    // -- Encoding --

    @Override
    public short getType() {
        return (short) 0x0103;
    }

    @Override
    public void read(BufferAdapter buf) {
        AlarmCRType = buf.getUnsignedShort();
        LT = buf.getUnsignedShort();
        AlarmCRProperties = buf.getInt();
        RTATimeoutFactor = buf.getUnsignedShort();
        RTARetries = buf.getUnsignedShort();
        LocalAlarmReference = buf.getUnsignedShort();
        MaxAlarmDataLength = buf.getUnsignedShort();
        AlarmCRTagHeaderHigh = buf.getUnsignedShort();
        AlarmCRTagHeaderLow = buf.getUnsignedShort();
    }

    @Override
    public void write(BufferAdapter buf) {
        buf.putUnsignedShort(AlarmCRType);
        buf.putUnsignedShort(LT);
        buf.putInt(AlarmCRProperties);
        buf.putUnsignedShort(RTATimeoutFactor);
        buf.putUnsignedShort(RTARetries);
        buf.putUnsignedShort(LocalAlarmReference);
        buf.putUnsignedShort(MaxAlarmDataLength);
        buf.putUnsignedShort(AlarmCRTagHeaderHigh);
        buf.putUnsignedShort(AlarmCRTagHeaderLow);
    }
}
