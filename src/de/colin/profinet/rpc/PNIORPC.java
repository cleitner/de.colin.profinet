/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.rpc;

import java.util.UUID;

public class PNIORPC {

    private PNIORPC() {
    }

    public static final UUID UUID_NULL = new UUID(0, 0);

    // -- Objects --

    /**
     * Creates a new PN-IO object instance UUID
     * 
     * @param x
     *            instance or node number
     * @param y
     *            device ID or vendor specific number for the device class
     * @param z
     *            vendor ID
     */
    public static UUID UUID_IO_ObjectInstance(short x, short y, short z) {
        return new UUID(0xDEA000006C9711D1l, 0x8271000000000000l
                | (((long) x & 0xffff) << 32) | (((long) y & 0xffff) << 16)
                | ((long) z & 0xffff));
    }

    // -- Interfaces --

    public static final UUID UUID_IO_DeviceInterface = UUID
            .fromString("DEA00001-6C97-11D1-8271-00A02442DF7D");
    public static final UUID UUID_IO_ControllerInterface = UUID
            .fromString("DEA00002-6C97-11D1-8271-00A02442DF7D");
    public static final UUID UUID_IO_SupervisorInterface = UUID
            .fromString("DEA00003-6C97-11D1-8271-00A02442DF7D");
    public static final UUID UUID_IO_ParameterServerInterface = UUID
            .fromString("DEA00004-6C97-11D1-8271-00A02442DF7D");

    // -- Operations --

    public static final short Connect = 0;
    public static final short Release = 1;
    public static final short Read = 2;
    public static final short Write = 3;
    public static final short Control = 4;
}
