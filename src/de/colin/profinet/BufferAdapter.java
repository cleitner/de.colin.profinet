/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.UUID;

/**
 * An adapter for {@code ByteBuffer}s to support structured data (e.g. NDR).
 * 
 * @see java.nio.ByteBuffer
 */
public class BufferAdapter {

    // Siemens loves Windows, and you'll always have to expect ISO8859-1 to
    // really be CP1252
    private static final Charset CHARSET = Charset.forName("windows-1252");

    private final ByteBuffer buf;

    public BufferAdapter(ByteBuffer buf) {
        if (buf == null) {
            throw new NullPointerException();
        }

        this.buf = buf;
    }

    public BufferAdapter(byte[] data) {
        this.buf = ByteBuffer.wrap(data);

        order(ByteOrder.BIG_ENDIAN);
    }

    public BufferAdapter(int capacity) {
        this.buf = ByteBuffer.allocate(capacity);

        order(ByteOrder.BIG_ENDIAN);
    }

    public ByteBuffer buffer() {
        return buf;
    }

    // -- ByteBuffer interface --

    public BufferAdapter order(ByteOrder order) {
        buf.order(order);
        return this;
    }

    public ByteOrder order() {
        return buf.order();
    }

    public boolean hasRemaining() {
        return buf.hasRemaining();
    }

    public int remaining() {
        return buf.remaining();
    }

    public int limit() {
        return buf.limit();
    }

    public BufferAdapter limit(int l) {
        buf.limit(l);
        return this;
    }

    public int position() {
        return buf.position();
    }

    public BufferAdapter position(int p) {
        buf.position(p);
        return this;
    }

    public BufferAdapter clear() {
        buf.clear();
        return this;
    }

    public BufferAdapter flip() {
        buf.flip();
        return this;
    }

    public BufferAdapter skip(int length) {
        buf.position(buf.position() + length);
        return this;
    }

    public BufferAdapter fill(int length) {
        return fill(length, (byte) 0);
    }

    public BufferAdapter fill(int length, byte value) {
        while (length > 0) {
            buf.put(value);
            length -= 1;
        }

        return this;
    }

    public byte get() {
        return buf.get();
    }

    public BufferAdapter put(byte b) {
        buf.put(b);
        return this;
    }

    public short getShort() {
        return buf.getShort();
    }

    public BufferAdapter putShort(short s) {
        buf.putShort(s);
        return this;
    }

    public int getUnsignedShort() {
        return buf.getShort() & 0xFFFF;
    }

    public BufferAdapter putUnsignedShort(int s) {
        if ((s < 0) || (s > 0xFFFF)) {
            throw new IllegalArgumentException();
        }
        buf.putShort((short) s);
        return this;
    }

    public int getInt() {
        return buf.getInt();
    }

    public BufferAdapter putInt(int i) {
        buf.putInt(i);
        return this;
    }

    public long getLong() {
        return buf.getLong();
    }

    public BufferAdapter putLong(long l) {
        buf.putLong(l);
        return this;
    }

    public BufferAdapter put(ByteBuffer src) {
        buf.put(src);
        return this;
    }

    public BufferAdapter put(byte[] src) {
        buf.put(src);
        return this;
    }

    public BufferAdapter get(byte[] dst) {
        buf.get(dst);
        return this;
    }

    // -- Alignment --

    /**
     * Aligns the buffers position to a multiple of {@code n}.
     * 
     * @param n
     *            the alignment
     */
    public BufferAdapter align(int n) {
        int p = buf.position() % n;

        // Needs alignment
        if (p != 0) {
            buf.position(buf.position() + n - p);
        }

        return this;
    }

    public short getAlignedShort() {
        align(2);
        return buf.getShort();
    }

    public BufferAdapter putAlignedShort(short s) {
        align(2);
        buf.putShort(s);
        return this;
    }

    public int getAlignedUnsignedShort() {
        align(2);
        return getUnsignedShort();
    }

    public BufferAdapter putAlignedUnsignedShort(int s) {
        align(2);
        return putUnsignedShort(s);
    }

    public int getAlignedInt() {
        align(4);
        return buf.getInt();
    }

    public BufferAdapter putAlignedInt(int i) {
        align(4);
        buf.putInt(i);
        return this;
    }

    public long getAlignedLong() {
        align(8);
        return buf.getLong();
    }

    public BufferAdapter putAlignedLong(long l) {
        align(8);
        buf.putLong(l);
        return this;
    }

    public UUID getAlignedUUID() {
        align(4);
        return getUUID();
    }

    public BufferAdapter putAlignedUUID(UUID uuid) {
        align(4);
        return putUUID(uuid);
    }

    // -- UUID --

    public BufferAdapter putUUID(UUID uuid) {

        long msb = uuid.getMostSignificantBits();
        long lsb = uuid.getLeastSignificantBits();

        putInt((int) (msb >> 32));
        putShort((short) (msb >> 16));
        putShort((short) msb);

        ByteOrder o = order();
        order(ByteOrder.BIG_ENDIAN);
        putLong(lsb);
        order(o);

        return this;
    }

    public UUID getUUID() {
        long msb, lsb;

        // time_low
        msb = ((long) getInt()) << 32l;
        // time_mid
        msb |= ((long) getShort() & 0xffff) << 16;
        // time_hi_and_version
        msb |= (long) getShort() & 0xffff;

        ByteOrder o = order();
        order(ByteOrder.BIG_ENDIAN);

        // clock_seq_hi_and_reserved
        // clock_seq_low
        // node
        lsb = getLong();

        order(o);

        return new UUID(msb, lsb);
    }

    // -- PN-IO Blocks --

    /**
     * Adds a new block header. The {@code length} argument contains the real
     * number of bytes in the block and will be adjusted to include the version
     * field.
     * 
     * Note that PN-IO blocks have to be written in big-endian order.
     * 
     * @param type
     * @param version
     * @param length
     *            the length of the block payload
     * 
     * @return {@code this}
     */
    public BufferAdapter putBlockHeader(short type, short version, int length) {
        if (length > 65533) {
            throw new IllegalArgumentException();
        }

        buf.putShort(type);
        buf.putShort((short) (length + 2));
        buf.putShort(version);

        return this;
    }

    // -- Strings --

    /**
     * Returns a string with a fixed length.
     * 
     * @return the converted string
     */
    public String getString(int length) {
        final int limit = buf.limit();

        buf.limit(buf.position() + length);

        String string;
        try {
            string = CHARSET.decode(buf.slice()).toString();
        } finally {
            buf.position(buf.limit());
            buf.limit(limit);
        }

        return string;
    }

    /**
     * Returns a string with a prefixed 16-Bit length. The Windows-1252 encoding
     * is used for any non-ASCII character.
     * 
     * @return the converted string
     */
    public String getLengthPrefixedString() {
        int length = buf.getShort() & 0xffff;

        return getString(length);
    }

    /**
     * Adds a length prefixed string to the buffer
     */
    public BufferAdapter putLengthPrefixedString(String str) {
        ByteBuffer chars = CHARSET.encode(str);

        // Don't use length directly, as str.length is not necessarily equal to
        // the number of bytes
        putUnsignedShort(chars.remaining());
        buf.put(chars);
        return this;
    }
}
