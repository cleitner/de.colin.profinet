/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class XmlConfigurationParser {

    private static class ConfigHandler implements ContentHandler {
        private static final String DEVICE_TAG = "device";
        private static final String MODULE_TAG = "module";
        private static final String PARAMS_TAG = "params";

        private short vendorId;
        private short deviceId;
        private List<ModuleConfiguration> modules = new ArrayList<ModuleConfiguration>();

        private int slot = 0;

        private int identNumber;
        private int inputLength;
        private int outputLength;

        private short index;
        private byte[] data;
        private List<ParameterRecordConfiguration> parameters = new ArrayList<ParameterRecordConfiguration>();

        private short getShortValue(Attributes atts, String name,
                short defaultValue) throws SAXException {
            String value = atts.getValue(name);

            if (value == null) {
                return defaultValue;
            } else {
                value = value.trim();

                try {
                    if (value.startsWith("0x") || value.startsWith("0X")) {
                        return Short.parseShort(value.substring(2), 16);
                    } else {
                        return Short.parseShort(value, 10);
                    }
                } catch (NumberFormatException nfe) {
                    throw new SAXException(nfe);
                }
            }
        }

        private int getIntValue(Attributes atts, String name, int defaultValue)
                throws SAXException {
            String value = atts.getValue(name);

            if (value == null) {
                return defaultValue;
            } else {
                value = value.trim();

                try {
                    if (value.startsWith("0x") || value.startsWith("0X")) {
                        return Integer.parseInt(value.substring(2), 16);
                    } else {
                        return Integer.parseInt(value, 10);
                    }
                } catch (NumberFormatException nfe) {
                    throw new SAXException(nfe);
                }
            }
        }
        
        private byte[] getDataValue(Attributes atts, String name, byte[] defaultValue)
                throws SAXException {
            String value = atts.getValue(name);

            if (value == null) {
                return Arrays.copyOf(defaultValue, defaultValue.length);
            } else {
                value = value.trim();

                String[] parts = value.split(",");
                List<Integer> values = new ArrayList<Integer>();
                
                for (String p : parts) {
                    int v;
                    try {
                        if (p.startsWith("0x") || p.startsWith("0X")) {
                            v = Integer.parseInt(p.substring(2), 16);
                        } else {
                            v = Integer.parseInt(p, 10);
                        }
                    } catch (NumberFormatException nfe) {
                        throw new SAXException(nfe);
                    }
                    
                    values.add(v);
                }
                
                byte[] bytes = new byte[values.size()];
                for (int n = 0; n < values.size(); n++) {
                    bytes[n] = values.get(n).byteValue();
                }
                
                return bytes;
            }
        }

        private DeviceConfiguration createDeviceConfig() {
            return new DeviceConfiguration(vendorId, deviceId, modules);
        }

        private ModuleConfiguration createModuleConfig() {
            List<SubmoduleConfiguration> submodules = new ArrayList<SubmoduleConfiguration>();

            submodules.add(new SubmoduleConfiguration(1, 0, inputLength,
                    outputLength, null, parameters));

            parameters.clear();

            return new ModuleConfiguration(slot, identNumber, submodules);
        }

        private ParameterRecordConfiguration createParameterConfig()
                throws SAXException {
            try {
                return new ParameterRecordConfiguration(index, data);
            } catch (IllegalArgumentException e) {
                throw new SAXException(e);
            }
        }

        @Override
        public void setDocumentLocator(Locator locator) {
            // Ignored
        }

        @Override
        public void startDocument() throws SAXException {
            // Ignored
        }

        @Override
        public void endDocument() throws SAXException {
            // Ignored
        }

        @Override
        public void startPrefixMapping(String prefix, String uri)
                throws SAXException {
            // Ignored
        }

        @Override
        public void endPrefixMapping(String prefix) throws SAXException {
            // Ignored
        }

        @Override
        public void startElement(String uri, String localName, String qName,
                Attributes atts) throws SAXException {
            if (localName.equals(DEVICE_TAG)) {
                vendorId = getShortValue(atts, "vendorId", (short) 0);
                deviceId = getShortValue(atts, "deviceId", (short) 0);

            } else if (localName.equals(MODULE_TAG)) {
                identNumber = getIntValue(atts, "ident", (short) 0);
                inputLength = getIntValue(atts, "input", (short) 0);
                outputLength = getIntValue(atts, "output", (short) 0);
            } else if (localName.equals(PARAMS_TAG)) {
                index = getShortValue(atts, "index", (short) 0);
                data = getDataValue(atts, "data", new byte[0]);
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName)
                throws SAXException {
            if (localName.equals(MODULE_TAG)) {
                modules.add(createModuleConfig());

                slot += 1;
            } else if (localName.equals(PARAMS_TAG)) {
                parameters.add(createParameterConfig());
            }
        }

        @Override
        public void characters(char[] ch, int start, int length)
                throws SAXException {
            // Ignored
        }

        @Override
        public void ignorableWhitespace(char[] ch, int start, int length)
                throws SAXException {
            // Ignored
        }

        @Override
        public void processingInstruction(String target, String data)
                throws SAXException {
            // Ignored
        }

        @Override
        public void skippedEntity(String name) throws SAXException {
            // Ignored
        }
    }

    public static DeviceConfiguration parse(InputSource source)
            throws IOException, SAXException {
        ConfigHandler handler = new ConfigHandler();

        XMLReader xmlReader = XMLReaderFactory.createXMLReader();
        xmlReader.setContentHandler(handler);
        xmlReader.parse(source);

        return handler.createDeviceConfig();
    }

}
