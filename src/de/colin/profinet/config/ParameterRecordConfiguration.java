/*
 *   Copyright 2013 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.config;

import java.util.Arrays;

public class ParameterRecordConfiguration {

    private final short index;
    private final byte[] data;

    public ParameterRecordConfiguration(short index, byte[] data) {
        this.index = index;
        this.data = Arrays.copyOf(data, data.length);
    }

    public short getIndex() {
        return index;
    }

    public byte[] getData() {
        return data;
    }

    public void accept(ConfigurationVisitor visitor) {
        visitor.visitParameterRecord(this);
    }
}
