/*
 *   Copyright 2014 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.config;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import de.colin.profisafe.CRC;

public class F_ParameterRecordConfiguration extends
        ParameterRecordConfiguration {

    private static byte[] createRecord(int prmFlag1, int prmFlag2,
            int hostAddress, int deviceAddress, int watchdogTimeout, int iParCRC) {

        boolean useiParCRC = ((prmFlag2 >> 3) & 0x07) == 1;

        int crc1 = CRC.calcCRC1(prmFlag1, prmFlag2, hostAddress, deviceAddress,
                watchdogTimeout, iParCRC);

        if (useiParCRC) {
            return new byte[] { (byte) (iParCRC >> 24), (byte) (iParCRC >> 16),
                    (byte) (iParCRC >> 8), (byte) (iParCRC), (byte) prmFlag1,
                    (byte) prmFlag2, (byte) (hostAddress >> 8),
                    (byte) (hostAddress), (byte) (deviceAddress >> 8),
                    (byte) (deviceAddress), (byte) (watchdogTimeout >> 8),
                    (byte) (watchdogTimeout), (byte) (crc1 >> 8), (byte) (crc1) };
        } else {
            return new byte[] { (byte) prmFlag1, (byte) prmFlag2,
                    (byte) (hostAddress >> 8), (byte) (hostAddress),
                    (byte) (deviceAddress >> 8), (byte) (deviceAddress),
                    (byte) (watchdogTimeout >> 8), (byte) (watchdogTimeout),
                    (byte) (crc1 >> 8), (byte) (crc1) };
        }
    }

    public F_ParameterRecordConfiguration(short index, int prmFlag1,
            int prmFlag2, int hostAddress, int deviceAddress,
            int watchdogTimeout) {
        super(index, createRecord(prmFlag1, prmFlag2, hostAddress,
                deviceAddress, watchdogTimeout, 0));
    }

    public F_ParameterRecordConfiguration(short index, int prmFlag1,
            int prmFlag2, int hostAddress, int deviceAddress,
            int watchdogTimeout, int iParCRC) {
        super(index, createRecord(prmFlag1, prmFlag2, hostAddress,
                deviceAddress, watchdogTimeout, iParCRC));

        if (!hasiParCRC() && iParCRC != 0) {
            throw new IllegalArgumentException(
                    "Set iParCRC to 0 if it's not used");
        }
    }

    public boolean hasiParCRC() {
        assert getData().length == 10 || getData().length == 14;
        return getData().length == 14;
    }

    public int getiParCRC() {
        if (!hasiParCRC()) {
            return 0;
        }

        ByteBuffer buf = ByteBuffer.wrap(getData());
        buf.order(ByteOrder.BIG_ENDIAN);

        return buf.getInt();
    }

    public int getPrmFlag1() {
        ByteBuffer buf = ByteBuffer.wrap(getData());
        buf.order(ByteOrder.BIG_ENDIAN);

        buf.position(0);
        buf.position(buf.position() + (hasiParCRC() ? 4 : 0));

        return buf.get() & 0xFF;
    }

    public int getPrmFlag2() {
        ByteBuffer buf = ByteBuffer.wrap(getData());
        buf.order(ByteOrder.BIG_ENDIAN);

        buf.position(1);
        buf.position(buf.position() + (hasiParCRC() ? 4 : 0));

        return buf.get() & 0xFF;
    }

    public int getHostAddress() {
        ByteBuffer buf = ByteBuffer.wrap(getData());
        buf.order(ByteOrder.BIG_ENDIAN);

        buf.position(2);
        buf.position(buf.position() + (hasiParCRC() ? 4 : 0));

        return buf.getShort() & 0xFFFF;
    }

    public int getDeviceAddress() {
        ByteBuffer buf = ByteBuffer.wrap(getData());
        buf.order(ByteOrder.BIG_ENDIAN);

        buf.position(4);
        buf.position(buf.position() + (hasiParCRC() ? 4 : 0));

        return buf.getShort() & 0xFFFF;
    }

    public int getWatchdogTimeout() {
        ByteBuffer buf = ByteBuffer.wrap(getData());
        buf.order(ByteOrder.BIG_ENDIAN);

        buf.position(6);
        buf.position(buf.position() + (hasiParCRC() ? 4 : 0));

        return buf.getShort() & 0xFFFF;
    }

    public int getCRC1() {
        ByteBuffer buf = ByteBuffer.wrap(getData());
        buf.order(ByteOrder.BIG_ENDIAN);

        buf.position(8);
        buf.position(buf.position() + (hasiParCRC() ? 4 : 0));

        return buf.getShort() & 0xFFFF;
    }
}
