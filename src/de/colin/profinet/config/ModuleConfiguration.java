/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ModuleConfiguration {

    private final int slot;
    private final int identNumber;

    private final ModuleState state;

    private final List<SubmoduleConfiguration> submodules;

    public ModuleConfiguration(int slot, int identNumber,
            List<SubmoduleConfiguration> submodules) {
        this(slot, identNumber, null, submodules);
    }

    public ModuleConfiguration(int slot, int identNumber, ModuleState state,
            List<SubmoduleConfiguration> submodules) {

        if ((slot < 0) || (slot > 65535)) {
            throw new IllegalArgumentException();
        }

        this.slot = slot;
        this.identNumber = identNumber;
        this.state = state;

        this.submodules = Collections
                .unmodifiableList(new ArrayList<SubmoduleConfiguration>(
                        submodules));
    }

    public void accept(ConfigurationVisitor visitor) {
        visitor.visitModule(this);

        for (SubmoduleConfiguration submodule : submodules) {
            submodule.accept(visitor);
        }
    }

    public int getSlot() {
        return slot;
    }

    public int getIdentNumber() {
        return identNumber;
    }

    public ModuleState getState() {
        return state;
    }

    public List<SubmoduleConfiguration> getSubmodules() {
        return submodules;
    }

    public SubmoduleConfiguration getSubmodule(int subslot) {
        // TODO: Linear search. Use this only in slow paths
        for (SubmoduleConfiguration s : submodules) {
            if (s.getSubslot() == subslot) {
                return s;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[slot=");
        builder.append(slot);
        builder.append(String.format(", identNumber=0x%08X, ", identNumber));
        if (state != null) {
            builder.append("state=");
            builder.append(state);
            builder.append(", ");
        }
        builder.append("submodules=");
        builder.append(submodules);
        builder.append("]");
        return builder.toString();
    }

}
