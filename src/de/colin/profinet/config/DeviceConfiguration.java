/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DeviceConfiguration {

    /**
     * Creates an instance of a device configuration with the given number of
     * dummy modules.
     * 
     * @return
     */
    public static DeviceConfiguration newEmptyConfiguration(int vendorId,
            int deviceId, int slots) {

        List<SubmoduleConfiguration> nullSubmodules = new ArrayList<SubmoduleConfiguration>();
        nullSubmodules.add(new SubmoduleConfiguration(0, 0));

        List<ModuleConfiguration> nullModules = new ArrayList<ModuleConfiguration>();
        for (int s = 0; s < slots; s++) {
            nullModules.add(new ModuleConfiguration(s, 0xFFFFFFFF,
                    nullSubmodules));
        }

        return new DeviceConfiguration(vendorId, deviceId, nullModules);
    }

    private final int vendorId;
    private final int deviceId;

    private final List<ModuleConfiguration> modules;

    public DeviceConfiguration(int vendorId, int deviceId,
            List<ModuleConfiguration> modules) {

        if ((vendorId < 0) || (vendorId > 0xFFFF)) {
            throw new IllegalArgumentException();
        }

        if ((deviceId < 0) || (deviceId > 0xFFFF)) {
            throw new IllegalArgumentException();
        }

        this.vendorId = vendorId;
        this.deviceId = deviceId;

        this.modules = Collections
                .unmodifiableList(new ArrayList<ModuleConfiguration>(modules));
    }

    public void accept(ConfigurationVisitor visitor) {
        visitor.visitDevice(this);

        for (ModuleConfiguration module : modules) {
            module.accept(visitor);
        }
    }

    @Override
    public String toString() {
        return String.format("[vendorId=0x%04X, deviceId=0x%04X, modules=%s]",
                vendorId, deviceId, modules);
    }

    public int getVendorId() {
        return vendorId;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public List<ModuleConfiguration> getModules() {
        return modules;
    }

    public ModuleConfiguration getModule(int slot) {
        // TODO: Linear search. Use this only in slow paths
        for (ModuleConfiguration m : modules) {
            if (m.getSlot() == slot) {
                return m;
            }
        }

        return null;
    }

}
