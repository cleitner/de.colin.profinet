/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SubmoduleConfiguration {

    private final int subslot;
    private final int identNumber;

    private final int inputLength;
    private final int outputLength;

    private final SubmoduleState state;

    private final List<ParameterRecordConfiguration> parameterRecords;

    public SubmoduleConfiguration(int subslot, int identNumber,
            int inputLength, int outputLength) {
        this(subslot, identNumber, inputLength, outputLength, null, null);
    }

    public SubmoduleConfiguration(int subslot, int identNumber,
            int inputLength, int outputLength, SubmoduleState state,
            List<ParameterRecordConfiguration> parameterRecords) {

        if ((subslot < 0) || (subslot > 65535)) {
            throw new IllegalArgumentException();
        }

        if (inputLength < 0) {
            throw new IllegalArgumentException();
        }

        if (outputLength < 0) {
            throw new IllegalArgumentException();
        }

        this.subslot = subslot;
        this.identNumber = identNumber;
        this.inputLength = inputLength;
        this.outputLength = outputLength;
        this.state = state;

        this.parameterRecords = Collections
                .unmodifiableList(new ArrayList<ParameterRecordConfiguration>(
                        parameterRecords));
    }

    public SubmoduleConfiguration(int inputLength, int outputLength) {
        this(1, 0, inputLength, outputLength);
    }

    public void accept(ConfigurationVisitor visitor) {
        visitor.visitSubmodule(this);
        
        for (ParameterRecordConfiguration record : parameterRecords) {
            record.accept(visitor);
        }
    }

    public int getSubslot() {
        return subslot;
    }

    public int getIdentNumber() {
        return identNumber;
    }

    public int getInputLength() {
        return inputLength;
    }

    public int getOutputLength() {
        return outputLength;
    }

    /**
     * Returns the submodule state of the device.
     * 
     * @return the submodule state or {@literal null} if not available
     */
    public SubmoduleState getState() {
        return state;
    }

    public List<ParameterRecordConfiguration> getParameterRecords() {
        return parameterRecords;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[subslot=");
        builder.append(subslot);
        builder.append(String.format(", identNumber=0x%08X", identNumber));
        builder.append(", inputLength=");
        builder.append(inputLength);
        builder.append(", outputLength=");
        builder.append(outputLength);
        builder.append(", ");
        if (state != null) {
            builder.append("state=");
            builder.append(state);
        }
        builder.append("]");
        return builder.toString();
    }

}
