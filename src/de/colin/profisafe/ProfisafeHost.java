/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profisafe;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.colin.timer.Clock;
import de.colin.timer.Timer;
import de.colin.timer.TimerListener;

/**
 * PROFIsafe host emulator for version 2 of the protocol.
 * 
 * <p>
 * For the love of god, this is <strong>NOT A SAFE IMPLEMENTATION AND CANNOT BE
 * USED IN A SAFETY APPLICATION.</strong>
 * 
 * <p>
 * This implementation lacks all <tt>iPar</tt> handling.
 * 
 * @see "IEC 61784-3-3 2007-12"
 * 
 * @author Colin Leitner
 */
public class ProfisafeHost {

    private static final Logger LOG = Logger.getLogger(ProfisafeHost.class
            .getName());

    private final Timer hostTimer;
    private final ProfisafeHostHandler handler;

    // Process data
    private final byte[] pvOutput;
    private final byte[] pvInput;

    // Data for safety PDUs
    private final byte[] output;
    private final byte[] input;

    // PROFIsafe PDUs
    private final byte[] psOutput;
    private final byte[] psInput;
    // Used to detect an acknowledgment from the device (new PDU)
    private final byte[] oldPsInput;

    // PROFIsafe F-parameters
    private int prmFlag1;
    private int prmFlag2;
    private int hostAddress = 1;
    private int deviceAddress = 2;
    private int watchdogTimeout = 100;
    private int iParCRC;

    // CRC1 of our PROFIsafe F-parameters
    private int crc1;

    /** Updates {@link #crc1} from the PROFIsafe F-parameters */
    private void updateCRC1() {
        crc1 = CRC.calcCRC1(prmFlag1, prmFlag2, hostAddress, deviceAddress,
                watchdogTimeout, iParCRC);
    }

    /**
     * Creates a new PROFIsafe host instance
     * 
     * @param clock
     *            the clock used for the watchdog timer
     * @param inputLength
     *            size of the safe input process image (excluding the PROFIsafe
     *            status byte and CRC)
     * @param outputLength
     *            size of the safe output process image (excluding the PROFIsafe
     *            control byte and CRC)
     * @param handler
     *            the handler for status updates
     */
    public ProfisafeHost(Clock clock, int inputLength, int outputLength,
            ProfisafeHostHandler handler) {
        if (handler == null) {
            throw new NullPointerException();
        }

        if (inputLength < 0 || inputLength > 123) {
            throw new IllegalArgumentException();
        }

        if (outputLength < 0 || outputLength > 123) {
            throw new IllegalArgumentException();
        }

        this.hostTimer = new Timer(clock, new TimerListener() {
            @Override
            public void onTimeout(Timer timer, int time) {
                ProfisafeHost.this.onTimeout();
            }
        });

        this.handler = handler;

        this.pvInput = new byte[inputLength];
        this.pvOutput = new byte[outputLength];
        this.input = new byte[inputLength];
        this.output = new byte[outputLength];

        if (input.length > 12) {
            this.psInput = new byte[input.length + 1 + 4];
        } else {
            this.psInput = new byte[input.length + 1 + 3];
        }
        this.oldPsInput = new byte[psInput.length];

        if (output.length > 12) {
            this.psOutput = new byte[output.length + 1 + 4];
        } else {
            this.psOutput = new byte[output.length + 1 + 3];
        }

        connect();
    }

    // We use the state diagram (IEC 61784-3-3, 7.2.2) as implementation guide.
    // We omit stored faults and the wait delay.

    private static enum State {
        /**
         * Equivalent to "3 Await Device Ack". After the host timeout or any we
         * enter <tt>RECONNECTING</tt>.
         * 
         * If Toggle_d = 1 and cons_nr_R = R_cons_nr = 0 we enter
         * <tt>CONNECTED</tt> or <tt>RECONNECTING</tt> (if we received a fault).
         */
        CONNECTING,
        /**
         * Equivalent to "6 Await Device Ack". Enters <tt>RECONNECTING</tt> if
         * the host timeout triggered or we received a fault.
         */
        CONNECTED,
        /**
         * Equivalent to "9 Await Device Ack". Enters <tt>CONNECTED</tt> if
         * Toggle_d = Toggle_h, cons_nr_R = R_cons_nr and OA is set.
         */
        RECONNECTING
    }

    private State state;

    private int x = 0xFFFFF0;
    private int old_x = 0xFFFFF0;
    private boolean OA_Req_S = false;
    private boolean OA_C_e = false;
    private boolean FV_activated_S = false;
    private boolean Host_CE_CRC = false;
    private boolean HostTimeout = false;
    private boolean activate_FV_C = false;
    private boolean OA_C = false;
    
    // Auto-reset OA latch. Simplifies the API consideraly, because it
    // auto-resets as soon as the next message has been sent
    private boolean operatorAcknowledgeLatch = false;

    // Control Byte (used by prepareMessage)
    private boolean Toggle_h = false;
    private boolean activate_FV = false;
    private boolean R_cons_nr = false;
    private boolean OA_Req = false;

    // Status byte (set by awaitDeviceAck)
    private boolean cons_nr_R = false;
    private boolean Toggle_d = false;
    private boolean FV_activated = false;
    private boolean WD_timeout = false;
    private boolean CE_CRC = false;
    private boolean Device_Fault = false;

    private void connect() {
        state = State.CONNECTING;

        restartHostTimer();

        x = 0xFFFFF0;
        old_x = 0xFFFFF0;
        OA_Req = false;
        OA_Req_S = false;
        OA_C_e = false;
        R_cons_nr = false;
        // iPar_OK_S = false

        // T1
        Toggle_h = true;
        useFV();

        // T2
        prepareMessage();
    }

    private void reconnect() {
        state = State.RECONNECTING;

        restartHostTimer();
        // storeFaults();
        resetVConsNr();
        useFV();

        // T15
        prepareMessage();
    }

    /**
     * Triggers state transitions after the host timeout triggered.
     */
    private void onHostTimeout() {
        switch (state) {
        case CONNECTING:
            // T10
            break;

        case CONNECTED:
            // T12/T13
            break;

        case RECONNECTING:
            // T20
            resetOA();
            break;

        default:
            throw new AssertionError();
        }

        reconnect();
    }

    /**
     * Handles a new PS PDU (an "Acknowledgment")
     */
    private void onDeviceAck() {
        int sb = psInput[input.length] & 0xFF;

        cons_nr_R = (sb & 0x40) != 0;
        Toggle_d = (sb & 0x20) != 0;
        FV_activated = (sb & 0x10) != 0;
        WD_timeout = (sb & 0x08) != 0;
        CE_CRC = (sb & 0x04) != 0;
        Device_Fault = (sb & 0x02) != 0;

        System.arraycopy(psInput, 0, input, 0, input.length);

        // Beware that the state diagram checks these conditions before CRC2
        // is checked!
        switch (state) {
        case CONNECTING:
            if (Toggle_d && (cons_nr_R == R_cons_nr)) {
                // T3
                restartHostTimer();
                checkDeviceAckNoToggle();
            }
            break;

        case CONNECTED:
            if (Toggle_d == Toggle_h) {
                // T6
                restartHostTimer();
                checkDeviceAckNoToggle();
            } else {
                // T7
                checkDeviceAckToggle();
            }
            break;

        case RECONNECTING:
            if ((Toggle_d == Toggle_h) && (cons_nr_R == R_cons_nr)) {
                // T16
                restartHostTimer();
                checkDeviceAckOA();
            }
            break;

        default:
            throw new AssertionError();
        }
    }

    /**
     * Handling of "4 Check Device Ack".
     */
    private void checkDeviceAckNoToggle() {
        checkHost_CE_CRC(false);

        if (Host_CE_CRC || WD_timeout || CE_CRC) {
            // T11
            reconnect();
        } else {
            boolean connected = state != State.CONNECTED;

            // T4
            state = State.CONNECTED;

            toggleVConsNr();
            updateIO();

            // T5
            prepareMessage();

            if (connected) {
                handler.onConnected(this);
            }
        }
    }

    /**
     * Handling of "7 Check Device Ack".
     */
    private void checkDeviceAckToggle() {
        if (state != State.CONNECTED) {
            throw new IllegalStateException();
        }

        checkHost_CE_CRC(true);

        if (Host_CE_CRC || WD_timeout || CE_CRC) {
            // T14
            reconnect();
        } else {
            // T8
            state = State.CONNECTED;

            updateIO();

            // T5
            prepareMessage();
        }
    }

    /**
     * Handling of "10 Check Device Ack".
     */
    private void checkDeviceAckOA() {
        if (state != State.RECONNECTING) {
            throw new IllegalStateException();
        }

        checkHost_CE_CRC(false);

        if (Host_CE_CRC || WD_timeout || CE_CRC) {
            // T18
            reconnect();
        } else {
            if (!operatorAcknowledgeLatch && (!OA_C || !OA_C_e)) {
                // T19

                boolean oaNecessary = !OA_Req_S;

                OA_Req_S = true;
                OA_Req = true;
                if (!OA_C) {
                    OA_C_e = true;
                }

                R_cons_nr = false;
                toggleVConsNr();
                useFV();

                // T15
                prepareMessage();

                if (oaNecessary) {
                    handler.onOANecessary(this);
                }

            } else {
                // T17
                state = State.CONNECTED;

                // resetStoredFaults();
                resetOA();
                R_cons_nr = false;
                toggleVConsNr();
                updateIO();

                // T5
                prepareMessage();

                handler.onConnected(this);
            }
        }
    }

    private void onInput() {
        // Ignore empty (all 0) PDUs
        boolean empty = true;
        for (byte i : psInput) {
            if (i != 0) {
                empty = false;
                break;
            }
        }
        if (empty) {
            return;
        }

        if (!Arrays.equals(oldPsInput, psInput)) {
            System.arraycopy(psInput, 0, oldPsInput, 0, psInput.length);

            onDeviceAck();
        } else {
            // No new PDU
        }
    }

    private void checkHost_CE_CRC(boolean includeOldX) {
        int sb = psInput[input.length] & 0xFF;

        int crc2;

        if ((psInput.length - input.length) == 5) {
            crc2 = ((psInput[input.length + 1] & 0xFF) << 24)
                    | ((psInput[input.length + 2] & 0xFF) << 16)
                    | ((psInput[input.length + 3] & 0xFF) << 8)
                    | (psInput[input.length + 4] & 0xFF);
        } else if ((psInput.length - input.length) == 4) {
            crc2 = ((psInput[input.length + 1] & 0xFF) << 16)
                    | ((psInput[input.length + 2] & 0xFF) << 8)
                    | (psInput[input.length + 3] & 0xFF);
        } else {
            throw new AssertionError();
        }

        int calculatedCRC2;

        Host_CE_CRC = false;

        if ((psInput.length - input.length) == 5) {
            calculatedCRC2 = CRC.calcCRC2_32(crc1, x, sb, psInput, 0,
                    input.length);
        } else if ((psInput.length - input.length) == 4) {
            calculatedCRC2 = CRC.calcCRC2_24(crc1, x, sb, psInput, 0,
                    input.length);
        } else {
            throw new AssertionError();
        }

        Host_CE_CRC = crc2 != calculatedCRC2;

        if (Host_CE_CRC && includeOldX) {
            if ((psInput.length - input.length) == 5) {
                calculatedCRC2 = CRC.calcCRC2_32(crc1, old_x, sb, psInput, 0,
                        input.length);
            } else if ((psInput.length - input.length) == 4) {
                calculatedCRC2 = CRC.calcCRC2_24(crc1, old_x, sb, psInput, 0,
                        input.length);
            } else {
                throw new AssertionError();
            }

            Host_CE_CRC = crc2 != calculatedCRC2;
        }

        if (Host_CE_CRC) {
            handler.onCRCError(this);
        }
    }

    private void restartHostTimer() {
        HostTimeout = false;
        hostTimer.start(watchdogTimeout);
    }

    private void resetVConsNr() {
        Toggle_h = !Toggle_h;
        R_cons_nr = true;
        x = 0;
    }

    private void toggleVConsNr() {
        Toggle_h = !Toggle_h;

        old_x = x;
        x = x + 1;
        if (x == 0x1000000) {
            x = 1;
        }
    }

    private void updateIO() {
        boolean fvActivated = false;

        if (FV_activated || activate_FV_C || Device_Fault) {

            fvActivated = !FV_activated_S;

            FV_activated_S = true;

            Arrays.fill(pvInput, (byte) 0);
        } else {
            FV_activated_S = false;

            System.arraycopy(input, 0, pvInput, 0, input.length);
        }

        if (activate_FV_C || Device_Fault) {
            activate_FV = true;

            Arrays.fill(output, (byte) 0);
        } else {
            activate_FV = false;

            System.arraycopy(pvOutput, 0, output, 0, output.length);
        }

        // iPar_OK_S = iPar_OK

        if (fvActivated) {
            handler.onFVActivated(this);
        }
    }

    private void useFV() {
        boolean fvActivated = !FV_activated_S;

        activate_FV = true;
        Arrays.fill(output, (byte) 0);

        // This is actually not mandated by the standard itself, but implemented
        // by the Siemens PLCs and the only sane thing to do. Without forcing
        // this value to true, module passivation would have to be implemented
        // manually in the PLC program itself.
        FV_activated_S = true;
        Arrays.fill(pvInput, (byte) 0);

        if (fvActivated) {
            handler.onFVActivated(this);
        }
    }

    private void resetOA() {
        operatorAcknowledgeLatch = false;
        OA_Req_S = false;
        OA_Req = false;
        OA_C_e = false;
    }

    private void prepareMessage() {

        if (crc1 == 0x0000) {
            Arrays.fill(pvOutput, (byte) 0);
            return;
        }

        int cb = 0;

        if (Toggle_h) {
            cb |= 0x20;
        }

        if (activate_FV) {
            cb |= 0x10;
        }

        if (R_cons_nr) {
            cb |= 0x04;
        }

        if (OA_Req) {
            cb |= 0x02;
        }

        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine(String.format("Preparing PS output: " + state
                    + " %04X 0x%06X\n", crc1, x));
        }

        System.arraycopy(output, 0, psOutput, 0, output.length);
        psOutput[output.length] = (byte) cb;

        if ((psOutput.length - output.length) == 5) {
            int crc2 = CRC.calcCRC2_32(crc1, x, cb, output);
            psOutput[output.length + 1] = (byte) (crc2 >> 24);
            psOutput[output.length + 2] = (byte) (crc2 >> 16);
            psOutput[output.length + 3] = (byte) (crc2 >> 8);
            psOutput[output.length + 4] = (byte) (crc2);
        } else if ((psOutput.length - output.length) == 4) {
            int crc2 = CRC.calcCRC2_24(crc1, x, cb, output);
            psOutput[output.length + 1] = (byte) (crc2 >> 16);
            psOutput[output.length + 2] = (byte) (crc2 >> 8);
            psOutput[output.length + 3] = (byte) (crc2);
        } else {
            throw new AssertionError();
        }
    }

    /**
     * Returns the current consecutive number.
     * 
     * @return the consecutive number
     */
    public int getConsecutiveNumber() {
        return x;
    }

    /**
     * Returns the F-parameter checksum.
     * 
     * @return CRC1
     */
    public int getCRC1() {
        return crc1;
    }

    // -- Control --

    /**
     * Enables the operator acknowledgment.
     * 
     * This flag is automatically set and reset during the PROFIsafe handshake.
     * 
     * @param oaRequest
     *            state of the OA request
     */
    public void setOA(boolean oaRequest) {
        this.OA_C = oaRequest;
    }

    /**
     * Latches the operator acknowledgment for one message. The OA resets automatically.
     */
    public void operatorAcknowledge() {
        this.operatorAcknowledgeLatch = true;
    }
    
    /**
     * Returns the indication to the user, that an operator acknowledgment is
     * necessary. The user has to call {@link #setOA(boolean)} to integrate the
     * device.
     */
    public boolean isOANecessary() {
        return OA_Req_S;
    }

    /**
     * Sets or disables the use of failsafe values.
     * 
     * @param activate
     *            if {@code true}, the process images will be set to 0
     */
    public void setActivateFV(boolean activate) {
        this.activate_FV_C = activate;
    }

    /**
     * Returns whether the device is currently using failsafe values.
     * 
     * @return {@code true} if the device uses failsafe values
     */
    public boolean isFVActivated() {
        return FV_activated_S;
    }

    /**
     * Returns whether the device reports an device fault.
     * 
     * @return {@code true} if the device has a pending device fault
     */
    public boolean isDeviceFault() {
        return Device_Fault;
    }

    /**
     * Returns whether the host or device detected a CRC error.
     * 
     * @return {@code true} if the host or device has a pending CRC error
     */
    public boolean isCRCError() {
        return Host_CE_CRC || CE_CRC;
    }

    /**
     * Returns whether the host or device encountered a watchdog timeout.
     * 
     * @return {@code true} if the host or device has a pending watchdog timeout
     */
    public boolean isWatchdogTimeout() {
        return HostTimeout || WD_timeout;
    }

    private void onTimeout() {
        if (!HostTimeout) {
            boolean connected = state == State.CONNECTED;

            HostTimeout = true;
            onHostTimeout();

            if (connected) {
                handler.onWatchdogTimeout(this);
            }
        }
    }

    /**
     * Returns whether the device is connected.
     * 
     * @return {@code true} if the host is connected to a device
     */
    public boolean isConnected() {
        return state == State.CONNECTED;
    }
    
    /**
     * Restarts the PROFIsafe connection.
     */
    public void restart() {
        connect();
    }

    /**
     * Stops the PROFIsafe timer.
     */
    public void stop() {
        hostTimer.stop();
    }

    // -- I/O --

    /**
     * Returns the length of the safe input image.
     */
    public int getProcessInputLength() {
        return input.length;
    }

    /**
     * Returns the length of the safe output image.
     */
    public int getProcessOutputLength() {
        return output.length;
    }

    /**
     * Sets the safe process output image to be transmitted to the device.
     * 
     * @param output
     *            the output image
     */
    public void setProcessOutput(byte[] output) {
        System.arraycopy(output, 0, pvOutput, 0, pvOutput.length);
    }

    /**
     * Returns the safe process input image.
     * 
     * @param input
     *            the array into which the input image is copied to
     */
    public void getProcessInput(byte[] input) {
        System.arraycopy(pvInput, 0, input, 0, pvInput.length);
    }

    /**
     * Writes the PROFIsafe output PDU
     * 
     * @param slice
     *            the output slice
     */
    public void writeProfisafeOutput(ByteBuffer slice) {
        slice.put(psOutput);
    }

    /**
     * Reads the PROFIsafe input PDU
     * 
     * @param slice
     *            the input slice
     */
    public void readProfisafeInput(ByteBuffer slice) {
        slice.get(psInput);

        onInput();
    }

    // -- F-parameters --

    public int getPrmFlag1() {
        return prmFlag1;
    }

    public void setPrmFlag1(int prmFlag1) {
        this.prmFlag1 = prmFlag1;
        updateCRC1();
    }

    public int getPrmFlag2() {
        return prmFlag2;
    }

    public void setPrmFlag2(int prmFlag2) {
        this.prmFlag2 = prmFlag2;
        updateCRC1();
    }

    public int getHostAddress() {
        return hostAddress;
    }

    public void setHostAddress(int hostAddress) {
        this.hostAddress = hostAddress;
        updateCRC1();
    }

    public int getDeviceAddress() {
        return deviceAddress;
    }

    public void setDeviceAddress(int deviceAddress) {
        this.deviceAddress = deviceAddress;
        updateCRC1();
    }

    public int getWatchdogTimeout() {
        return watchdogTimeout;
    }

    public void setWatchdogTimeout(int watchdogTimeout) {
        this.watchdogTimeout = watchdogTimeout;
        updateCRC1();
    }

    public int getiParCRC() {
        return iParCRC;
    }

    public void setiParCRC(int iParCRC) {
        this.iParCRC = iParCRC;
        updateCRC1();
    }

}
