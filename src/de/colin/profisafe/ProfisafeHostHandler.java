/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profisafe;

/**
 * Handler for the {@code ProfisafeHost} class.
 * 
 * @author Colin Leitner
 */
public interface ProfisafeHostHandler {

    /**
     * The host established a connection to the device.
     * 
     * @param host
     *            the host that connected to the device
     */
    void onConnected(ProfisafeHost host);

    /**
     * The host requires an operator acknowledgment to reintegrate the device.
     * 
     * @param host
     *            the host that needs the acknowledgment
     */
    void onOANecessary(ProfisafeHost host);

    /**
     * The host watchdog timer triggered.
     * 
     * @param host
     *            the host that detected the timeout
     */
    void onWatchdogTimeout(ProfisafeHost host);

    /**
     * The host detected a CRC error in a received message.
     * 
     * @param host
     *            the host that detected the error
     */
    void onCRCError(ProfisafeHost host);

    /**
     * The device requested to use failsafe values, or was ordered to do so.
     * 
     * @param host
     *            the host that detected the status
     */
    void onFVActivated(ProfisafeHost host);
}
