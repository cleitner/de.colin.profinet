/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profisafe;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * CRC methods for the various PROFIsafe checksums
 * 
 * @author Colin Leitner
 */
public class CRC {

    private CRC() {
    }

    /**
     * CRC16 lookup table. Polynomial <tt>0x4EAB</tt>.
     */
    public static int[] CRC16_TABLE = { 0x0000, 0x4EAB, 0x9D56, 0xD3FD, 0x7407,
            0x3AAC, 0xE951, 0xA7FA, 0xE80E, 0xA6A5, 0x7558, 0x3BF3, 0x9C09,
            0xD2A2, 0x015F, 0x4FF4, 0x9EB7, 0xD01C, 0x03E1, 0x4D4A, 0xEAB0,
            0xA41B, 0x77E6, 0x394D, 0x76B9, 0x3812, 0xEBEF, 0xA544, 0x02BE,
            0x4C15, 0x9FE8, 0xD143, 0x73C5, 0x3D6E, 0xEE93, 0xA038, 0x07C2,
            0x4969, 0x9A94, 0xD43F, 0x9BCB, 0xD560, 0x069D, 0x4836, 0xEFCC,
            0xA167, 0x729A, 0x3C31, 0xED72, 0xA3D9, 0x7024, 0x3E8F, 0x9975,
            0xD7DE, 0x0423, 0x4A88, 0x057C, 0x4BD7, 0x982A, 0xD681, 0x717B,
            0x3FD0, 0xEC2D, 0xA286, 0xE78A, 0xA921, 0x7ADC, 0x3477, 0x938D,
            0xDD26, 0x0EDB, 0x4070, 0x0F84, 0x412F, 0x92D2, 0xDC79, 0x7B83,
            0x3528, 0xE6D5, 0xA87E, 0x793D, 0x3796, 0xE46B, 0xAAC0, 0x0D3A,
            0x4391, 0x906C, 0xDEC7, 0x9133, 0xDF98, 0x0C65, 0x42CE, 0xE534,
            0xAB9F, 0x7862, 0x36C9, 0x944F, 0xDAE4, 0x0919, 0x47B2, 0xE048,
            0xAEE3, 0x7D1E, 0x33B5, 0x7C41, 0x32EA, 0xE117, 0xAFBC, 0x0846,
            0x46ED, 0x9510, 0xDBBB, 0x0AF8, 0x4453, 0x97AE, 0xD905, 0x7EFF,
            0x3054, 0xE3A9, 0xAD02, 0xE2F6, 0xAC5D, 0x7FA0, 0x310B, 0x96F1,
            0xD85A, 0x0BA7, 0x450C, 0x81BF, 0xCF14, 0x1CE9, 0x5242, 0xF5B8,
            0xBB13, 0x68EE, 0x2645, 0x69B1, 0x271A, 0xF4E7, 0xBA4C, 0x1DB6,
            0x531D, 0x80E0, 0xCE4B, 0x1F08, 0x51A3, 0x825E, 0xCCF5, 0x6B0F,
            0x25A4, 0xF659, 0xB8F2, 0xF706, 0xB9AD, 0x6A50, 0x24FB, 0x8301,
            0xCDAA, 0x1E57, 0x50FC, 0xF27A, 0xBCD1, 0x6F2C, 0x2187, 0x867D,
            0xC8D6, 0x1B2B, 0x5580, 0x1A74, 0x54DF, 0x8722, 0xC989, 0x6E73,
            0x20D8, 0xF325, 0xBD8E, 0x6CCD, 0x2266, 0xF19B, 0xBF30, 0x18CA,
            0x5661, 0x859C, 0xCB37, 0x84C3, 0xCA68, 0x1995, 0x573E, 0xF0C4,
            0xBE6F, 0x6D92, 0x2339, 0x6635, 0x289E, 0xFB63, 0xB5C8, 0x1232,
            0x5C99, 0x8F64, 0xC1CF, 0x8E3B, 0xC090, 0x136D, 0x5DC6, 0xFA3C,
            0xB497, 0x676A, 0x29C1, 0xF882, 0xB629, 0x65D4, 0x2B7F, 0x8C85,
            0xC22E, 0x11D3, 0x5F78, 0x108C, 0x5E27, 0x8DDA, 0xC371, 0x648B,
            0x2A20, 0xF9DD, 0xB776, 0x15F0, 0x5B5B, 0x88A6, 0xC60D, 0x61F7,
            0x2F5C, 0xFCA1, 0xB20A, 0xFDFE, 0xB355, 0x60A8, 0x2E03, 0x89F9,
            0xC752, 0x14AF, 0x5A04, 0x8B47, 0xC5EC, 0x1611, 0x58BA, 0xFF40,
            0xB1EB, 0x6216, 0x2CBD, 0x6349, 0x2DE2, 0xFE1F, 0xB0B4, 0x174E,
            0x59E5, 0x8A18, 0xC4B3 };

    /**
     * CRC32 lookup table. Polynomial <tt>0x5D6DCB</tt>.
     */
    public static final int CRC24_TABLE[] = new int[] { 0x000000, 0x5D6DCB,
            0xBADB96, 0xE7B65D, 0x28DAE7, 0x75B72C, 0x920171, 0xCF6CBA,
            0x51B5CE, 0x0CD805, 0xEB6E58, 0xB60393, 0x796F29, 0x2402E2,
            0xC3B4BF, 0x9ED974, 0xA36B9C, 0xFE0657, 0x19B00A, 0x44DDC1,
            0x8BB17B, 0xD6DCB0, 0x316AED, 0x6C0726, 0xF2DE52, 0xAFB399,
            0x4805C4, 0x15680F, 0xDA04B5, 0x87697E, 0x60DF23, 0x3DB2E8,
            0x1BBAF3, 0x46D738, 0xA16165, 0xFC0CAE, 0x336014, 0x6E0DDF,
            0x89BB82, 0xD4D649, 0x4A0F3D, 0x1762F6, 0xF0D4AB, 0xADB960,
            0x62D5DA, 0x3FB811, 0xD80E4C, 0x856387, 0xB8D16F, 0xE5BCA4,
            0x020AF9, 0x5F6732, 0x900B88, 0xCD6643, 0x2AD01E, 0x77BDD5,
            0xE964A1, 0xB4096A, 0x53BF37, 0x0ED2FC, 0xC1BE46, 0x9CD38D,
            0x7B65D0, 0x26081B, 0x3775E6, 0x6A182D, 0x8DAE70, 0xD0C3BB,
            0x1FAF01, 0x42C2CA, 0xA57497, 0xF8195C, 0x66C028, 0x3BADE3,
            0xDC1BBE, 0x817675, 0x4E1ACF, 0x137704, 0xF4C159, 0xA9AC92,
            0x941E7A, 0xC973B1, 0x2EC5EC, 0x73A827, 0xBCC49D, 0xE1A956,
            0x061F0B, 0x5B72C0, 0xC5ABB4, 0x98C67F, 0x7F7022, 0x221DE9,
            0xED7153, 0xB01C98, 0x57AAC5, 0x0AC70E, 0x2CCF15, 0x71A2DE,
            0x961483, 0xCB7948, 0x0415F2, 0x597839, 0xBECE64, 0xE3A3AF,
            0x7D7ADB, 0x201710, 0xC7A14D, 0x9ACC86, 0x55A03C, 0x08CDF7,
            0xEF7BAA, 0xB21661, 0x8FA489, 0xD2C942, 0x357F1F, 0x6812D4,
            0xA77E6E, 0xFA13A5, 0x1DA5F8, 0x40C833, 0xDE1147, 0x837C8C,
            0x64CAD1, 0x39A71A, 0xF6CBA0, 0xABA66B, 0x4C1036, 0x117DFD,
            0x6EEBCC, 0x338607, 0xD4305A, 0x895D91, 0x46312B, 0x1B5CE0,
            0xFCEABD, 0xA18776, 0x3F5E02, 0x6233C9, 0x858594, 0xD8E85F,
            0x1784E5, 0x4AE92E, 0xAD5F73, 0xF032B8, 0xCD8050, 0x90ED9B,
            0x775BC6, 0x2A360D, 0xE55AB7, 0xB8377C, 0x5F8121, 0x02ECEA,
            0x9C359E, 0xC15855, 0x26EE08, 0x7B83C3, 0xB4EF79, 0xE982B2,
            0x0E34EF, 0x535924, 0x75513F, 0x283CF4, 0xCF8AA9, 0x92E762,
            0x5D8BD8, 0x00E613, 0xE7504E, 0xBA3D85, 0x24E4F1, 0x79893A,
            0x9E3F67, 0xC352AC, 0x0C3E16, 0x5153DD, 0xB6E580, 0xEB884B,
            0xD63AA3, 0x8B5768, 0x6CE135, 0x318CFE, 0xFEE044, 0xA38D8F,
            0x443BD2, 0x195619, 0x878F6D, 0xDAE2A6, 0x3D54FB, 0x603930,
            0xAF558A, 0xF23841, 0x158E1C, 0x48E3D7, 0x599E2A, 0x04F3E1,
            0xE345BC, 0xBE2877, 0x7144CD, 0x2C2906, 0xCB9F5B, 0x96F290,
            0x082BE4, 0x55462F, 0xB2F072, 0xEF9DB9, 0x20F103, 0x7D9CC8,
            0x9A2A95, 0xC7475E, 0xFAF5B6, 0xA7987D, 0x402E20, 0x1D43EB,
            0xD22F51, 0x8F429A, 0x68F4C7, 0x35990C, 0xAB4078, 0xF62DB3,
            0x119BEE, 0x4CF625, 0x839A9F, 0xDEF754, 0x394109, 0x642CC2,
            0x4224D9, 0x1F4912, 0xF8FF4F, 0xA59284, 0x6AFE3E, 0x3793F5,
            0xD025A8, 0x8D4863, 0x139117, 0x4EFCDC, 0xA94A81, 0xF4274A,
            0x3B4BF0, 0x66263B, 0x819066, 0xDCFDAD, 0xE14F45, 0xBC228E,
            0x5B94D3, 0x06F918, 0xC995A2, 0x94F869, 0x734E34, 0x2E23FF,
            0xB0FA8B, 0xED9740, 0x0A211D, 0x574CD6, 0x98206C, 0xC54DA7,
            0x22FBFA, 0x7F9631 };

    /**
     * CRC32 lookup table. Polynomial <tt>0xF4ACFB13</tt>.
     */
    public static final int CRC32_TABLE[] = new int[] { 0x00000000, 0xF4ACFB13,
            0x1DF50D35, 0xE959F626, 0x3BEA1A6A, 0xCF46E179, 0x261F175F,
            0xD2B3EC4C, 0x77D434D4, 0x8378CFC7, 0x6A2139E1, 0x9E8DC2F2,
            0x4C3E2EBE, 0xB892D5AD, 0x51CB238B, 0xA567D898, 0xEFA869A8,
            0x1B0492BB, 0xF25D649D, 0x06F19F8E, 0xD44273C2, 0x20EE88D1,
            0xC9B77EF7, 0x3D1B85E4, 0x987C5D7C, 0x6CD0A66F, 0x85895049,
            0x7125AB5A, 0xA3964716, 0x573ABC05, 0xBE634A23, 0x4ACFB130,
            0x2BFC2843, 0xDF50D350, 0x36092576, 0xC2A5DE65, 0x10163229,
            0xE4BAC93A, 0x0DE33F1C, 0xF94FC40F, 0x5C281C97, 0xA884E784,
            0x41DD11A2, 0xB571EAB1, 0x67C206FD, 0x936EFDEE, 0x7A370BC8,
            0x8E9BF0DB, 0xC45441EB, 0x30F8BAF8, 0xD9A14CDE, 0x2D0DB7CD,
            0xFFBE5B81, 0x0B12A092, 0xE24B56B4, 0x16E7ADA7, 0xB380753F,
            0x472C8E2C, 0xAE75780A, 0x5AD98319, 0x886A6F55, 0x7CC69446,
            0x959F6260, 0x61339973, 0x57F85086, 0xA354AB95, 0x4A0D5DB3,
            0xBEA1A6A0, 0x6C124AEC, 0x98BEB1FF, 0x71E747D9, 0x854BBCCA,
            0x202C6452, 0xD4809F41, 0x3DD96967, 0xC9759274, 0x1BC67E38,
            0xEF6A852B, 0x0633730D, 0xF29F881E, 0xB850392E, 0x4CFCC23D,
            0xA5A5341B, 0x5109CF08, 0x83BA2344, 0x7716D857, 0x9E4F2E71,
            0x6AE3D562, 0xCF840DFA, 0x3B28F6E9, 0xD27100CF, 0x26DDFBDC,
            0xF46E1790, 0x00C2EC83, 0xE99B1AA5, 0x1D37E1B6, 0x7C0478C5,
            0x88A883D6, 0x61F175F0, 0x955D8EE3, 0x47EE62AF, 0xB34299BC,
            0x5A1B6F9A, 0xAEB79489, 0x0BD04C11, 0xFF7CB702, 0x16254124,
            0xE289BA37, 0x303A567B, 0xC496AD68, 0x2DCF5B4E, 0xD963A05D,
            0x93AC116D, 0x6700EA7E, 0x8E591C58, 0x7AF5E74B, 0xA8460B07,
            0x5CEAF014, 0xB5B30632, 0x411FFD21, 0xE47825B9, 0x10D4DEAA,
            0xF98D288C, 0x0D21D39F, 0xDF923FD3, 0x2B3EC4C0, 0xC26732E6,
            0x36CBC9F5, 0xAFF0A10C, 0x5B5C5A1F, 0xB205AC39, 0x46A9572A,
            0x941ABB66, 0x60B64075, 0x89EFB653, 0x7D434D40, 0xD82495D8,
            0x2C886ECB, 0xC5D198ED, 0x317D63FE, 0xE3CE8FB2, 0x176274A1,
            0xFE3B8287, 0x0A977994, 0x4058C8A4, 0xB4F433B7, 0x5DADC591,
            0xA9013E82, 0x7BB2D2CE, 0x8F1E29DD, 0x6647DFFB, 0x92EB24E8,
            0x378CFC70, 0xC3200763, 0x2A79F145, 0xDED50A56, 0x0C66E61A,
            0xF8CA1D09, 0x1193EB2F, 0xE53F103C, 0x840C894F, 0x70A0725C,
            0x99F9847A, 0x6D557F69, 0xBFE69325, 0x4B4A6836, 0xA2139E10,
            0x56BF6503, 0xF3D8BD9B, 0x07744688, 0xEE2DB0AE, 0x1A814BBD,
            0xC832A7F1, 0x3C9E5CE2, 0xD5C7AAC4, 0x216B51D7, 0x6BA4E0E7,
            0x9F081BF4, 0x7651EDD2, 0x82FD16C1, 0x504EFA8D, 0xA4E2019E,
            0x4DBBF7B8, 0xB9170CAB, 0x1C70D433, 0xE8DC2F20, 0x0185D906,
            0xF5292215, 0x279ACE59, 0xD336354A, 0x3A6FC36C, 0xCEC3387F,
            0xF808F18A, 0x0CA40A99, 0xE5FDFCBF, 0x115107AC, 0xC3E2EBE0,
            0x374E10F3, 0xDE17E6D5, 0x2ABB1DC6, 0x8FDCC55E, 0x7B703E4D,
            0x9229C86B, 0x66853378, 0xB436DF34, 0x409A2427, 0xA9C3D201,
            0x5D6F2912, 0x17A09822, 0xE30C6331, 0x0A559517, 0xFEF96E04,
            0x2C4A8248, 0xD8E6795B, 0x31BF8F7D, 0xC513746E, 0x6074ACF6,
            0x94D857E5, 0x7D81A1C3, 0x892D5AD0, 0x5B9EB69C, 0xAF324D8F,
            0x466BBBA9, 0xB2C740BA, 0xD3F4D9C9, 0x275822DA, 0xCE01D4FC,
            0x3AAD2FEF, 0xE81EC3A3, 0x1CB238B0, 0xF5EBCE96, 0x01473585,
            0xA420ED1D, 0x508C160E, 0xB9D5E028, 0x4D791B3B, 0x9FCAF777,
            0x6B660C64, 0x823FFA42, 0x76930151, 0x3C5CB061, 0xC8F04B72,
            0x21A9BD54, 0xD5054647, 0x07B6AA0B, 0xF31A5118, 0x1A43A73E,
            0xEEEF5C2D, 0x4B8884B5, 0xBF247FA6, 0x567D8980, 0xA2D17293,
            0x70629EDF, 0x84CE65CC, 0x6D9793EA, 0x993B68F9 };

    /**
     * Calculates the CRC value using the 16-Bit polynomial <tt>0x4EAB</tt>.
     * 
     * @param crc
     *            initial value
     * @param buf
     *            buffer containing the data
     * @param offset
     *            the offset of the data in the buffer
     * @param len
     *            length of the data
     * @return result CRC
     */
    public static int crc16(int crc, byte[] buf, int offset, int len) {
        while (len > 0) {
            crc = (CRC16_TABLE[((crc >>> 8) ^ buf[offset]) & 0xFF] ^ (crc << 8)) & 0xFFFF;

            offset += 1;
            len -= 1;
        }

        return crc;
    }

    /**
     * Calculates the CRC value using the 24-Bit polynomial <tt>0x5D6DCB</tt>.
     * 
     * @param crc
     *            initial value
     * @param buf
     *            buffer containing the data
     * @param offset
     *            the offset of the data in the buffer
     * @param len
     *            length of the data
     * @return result CRC
     */
    public static int crc24(int crc, byte[] buf, int offset, int len) {
        while (len > 0) {
            crc = (CRC24_TABLE[((crc >>> 16) ^ buf[offset]) & 0xFF] ^ (crc << 8)) & 0xFFFFFF;

            offset += 1;
            len -= 1;
        }

        return crc;
    }

    /**
     * Calculates the CRC value using the 24-Bit polynomial <tt>0x5D6DCB</tt>
     * over the data in reverse order.
     * 
     * @param crc
     *            initial value
     * @param buf
     *            buffer containing the data
     * @param offset
     *            the offset of the data in the buffer
     * @param len
     *            length of the data
     * @return result CRC
     */
    public static int crc24Reverse(int crc, byte[] buf, int offset, int len) {
        offset += len;

        while (len > 0) {
            crc = (CRC24_TABLE[((crc >>> 16) ^ buf[offset - 1]) & 0xFF] ^ (crc << 8)) & 0xFFFFFF;

            offset -= 1;
            len -= 1;
        }

        return crc;
    }

    /**
     * Calculates the CRC value using the 32-Bit polynomial <tt>0xF4ACFB13</tt>.
     * 
     * @param crc
     *            initial value
     * @param buf
     *            buffer containing the data
     * @param offset
     *            the offset of the data in the buffer
     * @param len
     *            length of the data
     * @return result CRC
     */
    public static int crc32(int crc, byte[] buf, int offset, int len) {
        while (len > 0) {
            crc = CRC32_TABLE[((crc >>> 24) ^ buf[offset]) & 0xFF] ^ (crc << 8);

            offset += 1;
            len -= 1;
        }

        return crc;
    }

    /**
     * Calculates the CRC value using the 32-Bit polynomial <tt>0xF4ACFB13</tt>
     * over the data in reverse order.
     * 
     * @param crc
     *            initial value
     * @param buf
     *            buffer containing the data
     * @param offset
     *            the offset of the data in the buffer
     * @param len
     *            length of the data
     * @return result CRC
     */
    public static int crc32Reverse(int crc, byte[] buf, int offset, int len) {
        offset += len;

        while (len > 0) {
            crc = CRC32_TABLE[((crc >>> 24) ^ buf[offset - 1]) & 0xFF]
                    ^ (crc << 8);

            offset -= 1;
            len -= 1;
        }

        return crc;
    }

    /**
     * Calculates CRC1 for the PROFIsafe parameters
     */
    public static int calcCRC1(int F_Prm_Flag1, int F_Prm_Flag2,
            int F_Source_Add, int F_Dest_Add, int F_WD_Time, int F_iPar_CRC) {

        byte[] block = new byte[12];

        boolean useIPar = ((F_Prm_Flag2 >> 3) & 0x07) == 1;

        block[0] = (byte) (F_iPar_CRC >> 24);
        block[1] = (byte) (F_iPar_CRC >> 16);
        block[2] = (byte) (F_iPar_CRC >> 8);
        block[3] = (byte) (F_iPar_CRC);

        block[4] = (byte) (F_Prm_Flag1);
        block[5] = (byte) (F_Prm_Flag2);

        block[6] = (byte) (F_Source_Add >> 8);
        block[7] = (byte) (F_Source_Add);

        block[8] = (byte) (F_Dest_Add >> 8);
        block[9] = (byte) (F_Dest_Add);

        block[10] = (byte) (F_WD_Time >> 8);
        block[11] = (byte) (F_WD_Time);

        int crc1 = crc16(0, block, useIPar ? 0 : 4, useIPar ? block.length
                : block.length - 4);

        if (crc1 == 0) {
            crc1 = 1;
        }

        return crc1;
    }

    /**
     * Calculates CRC2 for a PROFIsafe PDU using the 24-Bit polynomial.
     * 
     * @param crc1
     *            CRC1 value as calculated from the PROFIsafe parameters
     * @param vconsnr
     *            the consecutive number
     * @param csb
     *            control or status byte
     * @param data
     *            the F data
     * @return CRC2
     */
    public static int calcCRC2_24(int crc1, int vconsnr, int csb, byte[] data) {
        return calcCRC2_24(crc1, vconsnr, csb, data, 0, data.length);
    }

    public static int calcCRC2_24(int crc1, int vconsnr, int csb, byte[] data,
            int offset, int len) {

        if (crc1 < 0 || crc1 > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (vconsnr < 0 || vconsnr > 0xFFFFFF) {
            throw new IllegalArgumentException();
        }

        if (csb < 0 || csb > 0xFF) {
            throw new IllegalArgumentException();
        }

        // See IEC 61784-3-3, 7.1.5

        byte[] vconsnrBuf = new byte[4];
        vconsnrBuf[0] = 0;
        vconsnrBuf[1] = (byte) (vconsnr >> 16);
        vconsnrBuf[2] = (byte) (vconsnr >> 8);
        vconsnrBuf[3] = (byte) (vconsnr);

        // CRC1 ist the start value
        int crc2 = crc1;

        crc2 = crc24Reverse(crc2, new byte[] { (byte) csb }, 0, 1);
        crc2 = crc24Reverse(crc2, data, offset, len);
        crc2 = crc24Reverse(crc2, vconsnrBuf, 0, vconsnrBuf.length);

        if (crc2 == 0) {
            crc2 = 1;
        }

        return crc2;
    }

    /**
     * Calculates CRC2 for a PROFIsafe PDU using the 32-Bit polynomial.
     * 
     * @param crc1
     *            CRC1 value as calculated from the PROFIsafe parameters
     * @param vconsnr
     *            the consecutive number
     * @param csb
     *            control or status byte
     * @param data
     *            the F data
     * @return CRC2
     */
    public static int calcCRC2_32(int crc1, int vconsnr, int csb, byte[] data) {
        return calcCRC2_32(crc1, vconsnr, csb, data, 0, data.length);
    }

    public static int calcCRC2_32(int crc1, int vconsnr, int csb, byte[] data,
            int offset, int len) {

        if (crc1 < 0 || crc1 > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (vconsnr < 0 || vconsnr > 0xFFFFFF) {
            throw new IllegalArgumentException();
        }

        if (csb < 0 || csb > 0xFF) {
            throw new IllegalArgumentException();
        }

        // See IEC 61784-3-3, 7.1.5

        byte[] vconsnrBuf = new byte[4];
        vconsnrBuf[0] = 0;
        vconsnrBuf[1] = (byte) (vconsnr >> 16);
        vconsnrBuf[2] = (byte) (vconsnr >> 8);
        vconsnrBuf[3] = (byte) (vconsnr);

        // CRC1 ist the start value
        int crc2 = crc1;

        crc2 = crc32Reverse(crc2, new byte[] { (byte) csb }, 0, 1);
        crc2 = crc32Reverse(crc2, data, offset, len);
        crc2 = crc32Reverse(crc2, vconsnrBuf, 0, vconsnrBuf.length);

        if (crc2 == 0) {
            crc2 = 1;
        }

        return crc2;
    }

    /**
     * Calculates CRC7 as used by configuration tools to validate the I/O
     * configuration.
     * 
     * @param version
     *            version of the I/O configuration. Version 1 returns a 16-Bit
     *            CRC and doesn't support Int32 and U8+U8 data items.
     * 
     * @return CRC7. If version is 1, the return value is 16-Bit wide, 32
     *         otherwise
     */
    public static int calcCRC7(int version, int inputTotalBytes,
            int inputCompositeBytes, int inputU8U8Bytes, int inputBoolChannels,
            int inputBoolBytes, int inputInt16Bytes, int inputInt32Bytes,
            int inputFloatBytes, int outputTotalBytes,
            int outputCompositeBytes, int outputU8U8Bytes,
            int outputBoolChannels, int outputBoolBytes, int outputInt16Bytes,
            int outputInt32Bytes, int outputFloatBytes) {

        // TODO: The standard is not very clear on this calculation. Needs some
        // validation

        int crc7 = 0;

        if (version < 1 || version > 2) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            crc7 = 0;
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) version }, 0, 1);
        }

        if (inputTotalBytes < 0 || inputTotalBytes > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            crc7 = crc16(crc7, new byte[] { (byte) (inputTotalBytes >> 8),
                    (byte) inputTotalBytes }, 0, 2);
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (inputTotalBytes >> 8),
                    (byte) inputTotalBytes }, 0, 2);
        }

        if (inputCompositeBytes < 0 || inputCompositeBytes > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            crc7 = crc16(crc7, new byte[] { (byte) (inputCompositeBytes >> 8),
                    (byte) inputCompositeBytes }, 0, 2);
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (inputCompositeBytes >> 8),
                    (byte) inputCompositeBytes }, 0, 2);
        }

        if (inputU8U8Bytes < 0 || inputU8U8Bytes > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            if (inputU8U8Bytes != 0) {
                throw new IllegalArgumentException();
            }
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (inputU8U8Bytes >> 8),
                    (byte) inputU8U8Bytes }, 0, 2);
        }

        if (inputBoolChannels < 0 || inputBoolChannels > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            crc7 = crc16(crc7, new byte[] { (byte) (inputBoolChannels >> 8),
                    (byte) inputBoolChannels }, 0, 2);
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (inputBoolChannels >> 8),
                    (byte) inputBoolChannels }, 0, 2);
        }

        if (inputBoolBytes < 0 || inputBoolBytes > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            crc7 = crc16(crc7, new byte[] { (byte) (inputBoolBytes >> 8),
                    (byte) inputBoolBytes }, 0, 2);
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (inputBoolBytes >> 8),
                    (byte) inputBoolBytes }, 0, 2);
        }

        if (inputInt16Bytes < 0 || inputInt16Bytes > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            crc7 = crc16(crc7, new byte[] { (byte) (inputInt16Bytes >> 8),
                    (byte) inputInt16Bytes }, 0, 2);
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (inputInt16Bytes >> 8),
                    (byte) inputInt16Bytes }, 0, 2);
        }

        if (inputInt32Bytes < 0 || inputInt32Bytes > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            if (inputInt32Bytes != 0) {
                throw new IllegalArgumentException();
            }
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (inputInt32Bytes >> 8),
                    (byte) inputInt32Bytes }, 0, 2);
        }

        if (inputFloatBytes < 0 || inputFloatBytes > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            crc7 = crc16(crc7, new byte[] { (byte) (inputFloatBytes >> 8),
                    (byte) inputFloatBytes }, 0, 2);
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (inputFloatBytes >> 8),
                    (byte) inputFloatBytes }, 0, 2);
        }

        if (outputTotalBytes < 0 || outputTotalBytes > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            crc7 = crc16(crc7, new byte[] { (byte) (outputTotalBytes >> 8),
                    (byte) outputTotalBytes }, 0, 2);
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (outputTotalBytes >> 8),
                    (byte) outputTotalBytes }, 0, 2);
        }

        if (outputCompositeBytes < 0 || outputCompositeBytes > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            crc7 = crc16(crc7, new byte[] { (byte) (outputCompositeBytes >> 8),
                    (byte) outputCompositeBytes }, 0, 2);
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (outputCompositeBytes >> 8),
                    (byte) outputCompositeBytes }, 0, 2);
        }

        if (outputU8U8Bytes < 0 || outputU8U8Bytes > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            if (outputU8U8Bytes != 0) {
                throw new IllegalArgumentException();
            }
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (outputU8U8Bytes >> 8),
                    (byte) outputU8U8Bytes }, 0, 2);
        }

        if (outputBoolChannels < 0 || outputBoolChannels > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            crc7 = crc16(crc7, new byte[] { (byte) (outputBoolChannels >> 8),
                    (byte) outputBoolChannels }, 0, 2);
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (outputBoolChannels >> 8),
                    (byte) outputBoolChannels }, 0, 2);
        }

        if (outputBoolBytes < 0 || outputBoolBytes > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            crc7 = crc16(crc7, new byte[] { (byte) (outputBoolBytes >> 8),
                    (byte) outputBoolBytes }, 0, 2);
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (outputBoolBytes >> 8),
                    (byte) outputBoolBytes }, 0, 2);
        }

        if (outputInt16Bytes < 0 || outputInt16Bytes > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            crc7 = crc16(crc7, new byte[] { (byte) (outputInt16Bytes >> 8),
                    (byte) outputInt16Bytes }, 0, 2);
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (outputInt16Bytes >> 8),
                    (byte) outputInt16Bytes }, 0, 2);
        }

        if (outputInt32Bytes < 0 || outputInt32Bytes > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            if (outputInt32Bytes != 0) {
                throw new IllegalArgumentException();
            }
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (outputInt32Bytes >> 8),
                    (byte) outputInt32Bytes }, 0, 2);
        }

        if (outputFloatBytes < 0 || outputFloatBytes > 0xFFFF) {
            throw new IllegalArgumentException();
        }

        if (version == 1) {
            crc7 = crc16(crc7, new byte[] { (byte) (outputFloatBytes >> 8),
                    (byte) outputFloatBytes }, 0, 2);
        } else {
            crc7 = crc32(crc7, new byte[] { (byte) (outputFloatBytes >> 8),
                    (byte) outputFloatBytes }, 0, 2);
        }

        return crc7;
    }

    /**
     * Calculates the virtual consecutive number by brute-forcing all possible
     * numbers.
     * 
     * @param crc1
     *            CRC1 used to generate the PDU
     * @return the consecutive number or -1 if the message contains no valid
     *         PROFIsafe PDU
     */
    public static int findConsecutiveNumber(int crc1, byte[] pdu) {
        if (pdu.length < 4) {
            throw new IllegalArgumentException();
        }

        int crc2 = ((pdu[pdu.length - 3] & 0xFF) << 16)
                | ((pdu[pdu.length - 2] & 0xFF) << 8)
                | (pdu[pdu.length - 1] & 0xFF);

        int csb = pdu[pdu.length - 4] & 0xFF;

        return findConsecutiveNumber(crc1, csb, crc2, pdu, 0, pdu.length - 4);
    }

    /**
     * Calculates the virtual consecutive number by brute-forcing all possible
     * numbers.
     * 
     * @param crc1
     *            CRC1
     * @param csb
     *            control or status byte of original message
     * @param crc2
     *            CRC2 of original message
     * @param data
     *            buffer of safe process data of original message
     * @param offset
     *            offset in the buffer
     * @param len
     *            length of the process data
     * @return the consecutive number or -1 if it couldn't be found
     */
    public static int findConsecutiveNumber(int crc1, int csb, int crc2,
            byte[] data, int offset, int len) {

        int vconsNr = 0xFFFFF0;
        do {

            int currentCRC2 = calcCRC2_24(crc1, vconsNr, csb, data, offset, len);

            if (currentCRC2 == crc2) {
                return vconsNr;
            }

            // We include 0, because R_cons_nr sets x to 0 (regular VConsNr
            // counting skips 0)
            vconsNr += 1;
            vconsNr &= 0xFFFFFF;
        } while (vconsNr != 0xFFFFF0);

        return -1;
    }

    public static void main(String[] args) throws Exception {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;

        int crc1;

        System.out.print("CRC1: ");
        line = in.readLine().trim();
        if (line.startsWith("0x") || line.startsWith("0X")) {
            line = line.substring(2);
        }
        crc1 = Integer.parseInt(line, 16);
        System.out.printf("CRC1: 0x%04x\n", crc1);

        while (true) {

            int len = 0;
            byte[] msg = new byte[16];

            System.out.printf("Message: ");
            if ((line = in.readLine()) == null) {
                break;
            }

            StringTokenizer tokenizer = new StringTokenizer(line);
            while ((len < 16) && tokenizer.hasMoreTokens()) {
                String token = tokenizer.nextToken("\t :");

                msg[len] = (byte) Integer.parseInt(token, 16);
                len += 1;
            }

            if (len == 0) {
                break;
            }

            if (len < 4) {
                System.out.printf("Message is too short\n");
                continue;
            }

            byte csb = msg[len - 4];
            int crc2 = ((msg[len - 3] << 16) & 0xff0000)
                    | ((msg[len - 2] << 8) & 0xff00) | ((msg[len - 1]) & 0xff);

            System.out.printf("CB/SB: 0x%02X\n", csb);
            System.out.printf("CRC2:  0x%06X\n", crc2);

            int vconsnr = findConsecutiveNumber(crc1, csb, crc2, msg, 0,
                    len - 4);

            if (vconsnr != -1) {
                System.out.printf("Consecutive Number: 0x%06X\n", vconsnr);
            } else {
                System.out.printf("Couldn't find consecutive number\n");
            }
        }
    }

}
