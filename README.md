Java library for CPF3 and FSCP 3/1 protocols
============================================

Introduction
------------

This library allows you to run a PN-RT transport on a PC or mobile phone.

Additionally it comes with an PN-IO capable controller, GSDML parser and a
FSCP 3/1 compatible host implementation.

It has been tested to work with Festo CPX valve terminals. 

FSCP 3/1
--------

This library contains a FSCP 3/1 (also known as PROFIsafe) host implementation.

It hasn't been developed by the necessary development process to qualify as
safe and hasn't been certified by the PNO.

Therefore **YOU CANNOT USE THE `ProfisafeHost` CLASS IN PRODUCTION CODE!**

It is purely meant to be used as a development aid.

Supported platforms
-------------------

The library requires a native utility to provide access to raw Ethernet.

The transport helper has been compiled and successfully tested on

  * Win32
  * Linux/i686+amd64
  * Android/armeabi
  * MacOS X

You'll need the [WinPcap](http://www.winpcap.org/) development kit and MinGW or
cygwin to compile the transport helper on Windows (`make -f Makefile.win32`)
and the libpcap development library of your choice on Linux.

Running
-------

Compile the transport helper and add its location to `$PATH`. Under Linux
you'll also have to `setuid root` on the helper, or your application won't find
any network interfaces.

If you wan't to specify the path to the helper, call your application with a
special property:

    java -Dde.colin.profinet.pnrt.transport-helper=/path/to/helper example.Program

License
-------

The code is licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).

Disclaimer
----------

PROFINET and PROFIsafe are trademarks of the [PROFIBUS Nutzerorganisation e.V.
(PNO)](http://www.profibus.com/). The library has not been certified to conform
to either standard.

